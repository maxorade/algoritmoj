
import Herramientas.BusquedaProteinas as bp
from tqdm import tqdm
import json
import itertools


############## busqueda de proteinas individuales
# with open('Documentos/ParesProteinasFInal/listado_final_posibles_pares.json', 'r') as file:
#     listado_pares = json.load(file) 

# listado_proteinas =  []

# for par in listado_pares:
#     prot1_pdb  = par[0]
#     prot2_pdb  = par[1]
#     listado_proteinas.append(prot1_pdb)
#     listado_proteinas.append(prot2_pdb)

# print("total proteinas",len(listado_proteinas))

# listado_proteinas = list(set(listado_proteinas))

# print("total proteinas",len(listado_proteinas))

# with open("Documentos/ParesProteinasFInal/total_proteinas.json", "w") as write_file:
#     json.dump(listado_proteinas, write_file)

#################busqueda de proteinas en uniprot
# with open('Documentos/ParesProteinasFInal/total_proteinas.json', 'r') as file:
#     listado_proteinas = json.load(file) 



# listado_proteinas_uniprot = {}

# for i in tqdm(range(len(listado_proteinas))):
#     index =  i 
#     prot = listado_proteinas[i]
#     prot_uniprot = bp.MapeoPDBUniprot(pdb_id=prot)
#     listado_proteinas_uniprot[prot] = prot_uniprot

# with open("Documentos/ParesProteinasFInal/total_proteinas_uniprot.json", "w") as write_file:
#     json.dump(listado_proteinas_uniprot, write_file)


################ uniprot a biogrid
# with open('Documentos/ParesProteinasFInal/listado_final_posibles_pares.json', 'r') as file:
#     listado_pares = json.load(file) 

# with open('Documentos/ParesProteinasFInal/total_proteinas_uniprot.json', 'r') as file:
#     listado_proteinas_uniprot = json.load(file) 


# total_proteina_complex  = []

# biogrid_encontrados = []

# for i in tqdm(range(len(listado_pares))):
#     index =  i 
#     par = listado_pares[i]
#     prot1_pdb  = par[0]
#     prot2_pdb  = par[1]

#     prot1_uniprot = listado_proteinas_uniprot[prot1_pdb]
#     prot2_uniprot = listado_proteinas_uniprot[prot2_pdb]


#     if prot1_uniprot != None and prot2_uniprot != None:

#         pares_uniprot = [(prot1["UniprotId"],prot2["UniprotId"] ) for prot1 in prot1_uniprot for prot2 in prot2_uniprot]

#         proteina_complex = [prot for prot in pares_uniprot if prot[0] == prot[1] ]

#         if len(proteina_complex) > 0:
#             total_proteina_complex.append(par)
#             # print(proteina_complex)
#         else: 
#             for i in pares_uniprot:
#                 uniprot_id1 = i[0]
#                 uniprot_id2 = i[1]
#                 # print(uniprot_id1)
#                 # print(uniprot_id2)

#                 chain1 = [i["Chain"] for i in prot1_uniprot if uniprot_id1  == i["UniprotId"] ][0]
#                 chain2 = [i["Chain"] for i in prot2_uniprot if uniprot_id2  == i["UniprotId"] ][0]
#                 # print("chain1",chain1)
#                 # print("##########")
#                 # print("chain2",chain2)

#                 busqueda_biogrid = bp.BusquedaBiogridParesUniprot(uniprot_id1=uniprot_id1,
#                                                     uniprot_id2=uniprot_id2)

#                 if len(busqueda_biogrid) > 0:
#                     # print(busqueda_biogrid)
#                     busqueda_biogrid = '|'.join([i["BiogridId"] for i in busqueda_biogrid])
#                     datos_integrados = {
#                             "PdbId1": prot1_pdb,
#                             "UniprotId1": uniprot_id1,
#                             "Chain1":chain1,
#                             "PdbId2": prot2_pdb,
#                             "UniprotId2": uniprot_id2,
#                             "Chain2":chain2,
#                             "BiogridIds":busqueda_biogrid
#                         }
#                     biogrid_encontrados.append(datos_integrados)


# print(len(total_proteina_complex))
# print(len(biogrid_encontrados))
#     # print(pares_uniprot)
#     # print(proteina_complex)

# # 116
# # 83


# with open("Documentos/ParesProteinasFInal/proteinas_forman_parte_otra.json", "w") as write_file:
#     json.dump(total_proteina_complex, write_file)

# with open("Documentos/ParesProteinasFInal/pares_encontrados_biogrid.json", "w") as write_file:
#     json.dump(biogrid_encontrados, write_file)


################# csv analisis 

# with open('Documentos/ParesProteinasFInal/pares_encontrados_biogrid.json', 'r') as file:
#     biogrid_encontrados = json.load(file) 


# listado_validar_docking =  [{
#         "NombreProyecto":  i["PdbId1"]+"_"+ i["PdbId2"] ,
#         "PdbId1": i["PdbId1"],
#         "UniprotId1": i["UniprotId1"],
#         "Chain1": i["Chain1"],
#         "PdbId2": i["PdbId2"],
#         "UniprotId2": i["UniprotId2"],
#         "Chain2": i["Chain2"],
#         "BiogridIds":i["BiogridIds"],
#         "ValidadoPydockWeb":"no"
#     }
#     for i in  biogrid_encontrados ]
# import pandas as pd

# df = pd.DataFrame(listado_validar_docking)

# # Write the DataFrame to a CSV file
# df.to_csv('Documentos/ParesProteinasFInal/listado_validado.csv', index=False)


################ descargar proteinas pdb

# import os
# with open('Documentos/ParesProteinasFInal/total_proteinas.json', 'r') as file:
#     listado_proteinas = json.load(file) 

# ruta_registro = "Documentos/ParesProteinasFInal/ProteinasDocking"

# for i in tqdm(range(len(listado_proteinas))):
#     index =  i 
#     pdb_id = listado_proteinas[i]

#     ruta_proteina = f"{ruta_registro}/{pdb_id}.pdb"
#     if os.path.exists(ruta_proteina) == False:
#         bp.GuardarProteina(pdb_id, ruta_registro)



################ descargar proteinas fasta

# import os
# with open('Documentos/ParesProteinasFInal/total_proteinas.json', 'r') as file:
#     listado_proteinas = json.load(file) 

# ruta_registro = "Documentos/ParesProteinasFInal/ProteinasDockingFasta"

# for i in tqdm(range(len(listado_proteinas))):
#     index =  i 
#     pdb_id = listado_proteinas[i]

#     ruta_proteina = f"{ruta_registro}/{pdb_id}.fasta"
#     if os.path.exists(ruta_proteina) == False:
#         bp.GuardarProteinaFasta(pdb_id, ruta_registro)

####################### proteinas docking faltantes
import os
 
with open('Documentos/ParesProteinasFInal/proteinas_forman_parte_otra.json', 'r') as file:
    total_proteina_complex = json.load(file) 

with open('Documentos/ParesProteinasFInal/pares_encontrados_biogrid.json', 'r') as file:
    biogrid_encontrados = json.load(file) 

with open('Documentos/ParesProteinasFInal/listado_final_posibles_pares.json', 'r') as file:
    listado_pares = json.load(file) 


listado_biogrid_encontradas = [[i["PdbId1"], i["PdbId2"]] for i in biogrid_encontrados]

listado_total_descarte = total_proteina_complex + listado_biogrid_encontradas

listado_total =    [par  for par in listado_pares  if par not in  listado_total_descarte]

print(len(listado_total_descarte))
print(len(listado_pares))
print(len(listado_total))

# 199
# 490
# 300

listado_validar_docking =  [{
        "NombreProyecto":  i[0]+"_"+ i[1] ,
        "PdbId1": i[0],
        "PdbId2": i[1],
        "Gramm":""
    }
    for i in  listado_total ]
import pandas as pd

df = pd.DataFrame(listado_validar_docking)

# Write the DataFrame to a CSV file
df.to_csv('Documentos/ParesProteinasFInal/listado_faltante.csv', index=False)




# from pydock import docker
# output = docker.run("hello-world")
# print(output)