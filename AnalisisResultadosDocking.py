import Herramientas.BusquedaProteinas as bp
from tqdm import tqdm
import os 
import pandas as pd


ruta_base = "Documentos/Articulos/PDB"
carpeta_pmcids = os.listdir(ruta_base)

listado_rmsd =  []

for i in tqdm(range(len(carpeta_pmcids))):
    if ".txt" in carpeta_pmcids[i] or ".csv"  in carpeta_pmcids[i] :
        continue
    else:
        ruta_carpeta  = f"{ruta_base}/{carpeta_pmcids[i]}"
        carpeta_articulo = os.listdir(ruta_carpeta)

        validar_gramm = False
        validar_pydock = False
        validar_cluspro = False

        rmsd_gramm = 0 
        rmsd_pydock = 0 
        rmsd_cluspro  = 0 

        

        # analisis GRAMM

        ruta = f"{ruta_carpeta}/GRAMM/GRAMMresults"

        if os.path.exists(ruta) == True:

            validar_gramm = True
            model1 =  f'{ruta}/model_1.pdb'
            model2 = f'{ruta}/model_2.pdb'

            rmsd_gramm =  bp.CalcularRMSD(model1, model2)
            rmsd_gramm = round(rmsd_gramm, 2)


        # analisis  pyDockWeb

        ruta = f"{ruta_carpeta}/pyDockWeb"

        carpeta_pydock = os.listdir(ruta)

        if len(carpeta_pydock) > 1:
            validar_pydock = True
            pydock_modelos = [i for i in carpeta_pydock if ".ene" in i ][0]
            nombre_proyecto = pydock_modelos.replace(".ene", "")

            archivo = open(f'{ruta}/{pydock_modelos}', 'r')
            contenido = archivo.read()
            archivo.close()

            model1 = contenido.split("\n")[2].split(" ")
            model1 = [x for x in model1 if x != ""][0]
            model1 = f"{ruta}/{nombre_proyecto}_{model1}.pdb"


            model2 = contenido.split("\n")[3].split(" ")
            model2 = [x for x in model2 if x != ""][0]
            model2 = f"{ruta}/{nombre_proyecto}_{model2}.pdb"

            rmsd_pydock =  bp.CalcularRMSD(model1, model2)
            rmsd_pydock = round(rmsd_pydock, 2)


        # analisis  ClusPro

        ruta = f"{ruta_carpeta}/ClusPro"

        carpeta_clus = os.listdir(ruta)

        if len(carpeta_clus) != 0:

            validar_cluspro = True
            carpeta_clus = [j for j in carpeta_clus if ".tar.bz2" not in j ][0]
            ruta = f"{ruta}/{carpeta_clus}"

            model1 =  f'{ruta}/model.000.00.pdb'
            model2 = f'{ruta}/model.000.01.pdb'

            rmsd_cluspro =  bp.CalcularRMSD(model1, model2)
            rmsd_cluspro = round(rmsd_cluspro, 2)



        if validar_gramm == True or validar_pydock == True or validar_cluspro == True:
            datos =  {
                "PMCId": carpeta_pmcids[i],
                "GRAMMRMSD": rmsd_gramm,
                "GRAMMLogLR": bp.CalcularLogLR(rmsd_gramm),
                "pyDockWebRMSD": rmsd_pydock,
                "pyDockWebLogLR":  bp.CalcularLogLR(rmsd_pydock),
                "ClusProRMSD": rmsd_cluspro,
                "ClusProLogLR":  bp.CalcularLogLR(rmsd_cluspro)
            }
            listado_rmsd.append(datos)
        



df = pd.DataFrame(listado_rmsd)

df.to_csv(f"Documentos/Articulos/ResultadosDockingRMSDLogLR.csv", index=False)          


