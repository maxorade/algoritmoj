import Herramientas.BusquedaProteinas as bp
from tqdm import tqdm
import os 
import pandas as pd


ruta_base = "Docking/ResultadosGRAMM"

listado_rmsd =  []

tipo =  "Validado"

path = f"{ruta_base}/{tipo}"
files = os.listdir(path)

for file in files:
    path_res = f"{path}/{file}/GRAMMresults"
    # print(file)
    # print(path_res)

    model1 =  f'{path_res}/model_1.pdb'
    model2 = f'{path_res}/model_2.pdb'

    rmsd_gramm =  bp.CalcularRMSD(model1, model2)
    rmsd_gramm = round(rmsd_gramm, 2)


    datos =  {
        "Proyecto": file,
        "Tipo":tipo,
        "GRAMMRMSD": rmsd_gramm,
        "GRAMMLogLR": bp.CalcularLogLR(rmsd_gramm),
    }
    listado_rmsd.append(datos)


############### 
tipo =  "Faltante"

path = f"{ruta_base}/{tipo}"
files = os.listdir(path)

for file in files:
    path_res = f"{path}/{file}/GRAMMresults"
    # print(file)
    # print(path_res)

    model1 =  f'{path_res}/model_1.pdb'
    model2 = f'{path_res}/model_2.pdb'

    rmsd_gramm =  bp.CalcularRMSD(model1, model2)
    rmsd_gramm = round(rmsd_gramm, 2)


    datos =  {
        "Proyecto": file,
        "Tipo":tipo,
        "GRAMMRMSD": rmsd_gramm,
        "GRAMMLogLR": bp.CalcularLogLR(rmsd_gramm),
    }
    listado_rmsd.append(datos)

df = pd.DataFrame(listado_rmsd)

df.to_csv(f"{ruta_base}/ResultadosDockingGrammRMSDLogLR.csv", index=False)          
