import json 
import spacy
import os
import sys
import pandas as pd
import Herramientas.FuncionesLexicon as fl
import warnings
warnings.filterwarnings('ignore')


if __name__ == "__main__":

    print("Inicio Proceso")
    opcion = sys.argv[1]

    print(opcion)

    if opcion == "RecopilacionResultadosManualModelos":

        with open("Documentos/Articulos/ArticulosProteinasAnalisis_1.json", "r") as archivo:
            papers_docking = json.load(archivo)

        with open("Documentos/Articulos/ResultadosClasificadores/ResultadosParrafosClasificados.json", "r") as archivo:
            resultados_modelos = json.load(archivo)

        with open('Documentos/ListadoProteinNucleicAcidComplexes.json', 'r') as file:
            listadoscomplejospdb = json.load(file)

        with open('Documentos/BiogridPDB/ParesProteinasBiogrid.json', 'r') as file:
            listadoparesbiogrid = json.load(file)


        carpeta_asociar = os.listdir("Documentos/BiogridPDB/AsociarPDB")


        columns_to_read = ['Index', 'PdbId', 'UniprotId1', 'Chain1', 'UniprotId2', 'Chain2', 'BiogridIds' ]

        listado_asociado = pd.DataFrame(columns=columns_to_read)

        for nombre_file in carpeta_asociar:
            nombre_file =  f"Documentos/BiogridPDB/AsociarPDB/{nombre_file}"
            df = pd.read_csv(nombre_file, header=None, names=columns_to_read)
            listado_asociado = pd.concat([listado_asociado, df], axis=0)


        print("Total interacciones entre proteinas pdb-biogrid", len(listado_asociado["PdbId"]))

        listado_pdb_asociado = set(listado_asociado["PdbId"])

        print("Total proteinas asociadas pdb", len(listado_pdb_asociado))

        jnlpba = spacy.load('en_ner_jnlpba_md')

        total_parrafos = []

        listado_proteinas = []
        listado_proteinas_complejos = []
        listado_pares = []

        for i in papers_docking:
            for parrafo in i["Parrafos"]:
                
                pares_parrafo = []
                proteina_search_complex = []
                for proteina in parrafo["Proteinas"]:
                    listado_proteinas.append(proteina)
                    if proteina in listadoscomplejospdb:
                        listado_proteinas_complejos.append(proteina)
                        proteina_search_complex.append(proteina)
                for par in parrafo["Pares"]:
                    if par[1] not in proteina_search_complex and par[0] not in proteina_search_complex:
                        listado_pares.append(par)
                        pares_parrafo.append(par)

                if len(pares_parrafo) > 0:
                    parrafo_utf8 = parrafo["Parrafo"]

                    palabras_claves =  list(set(parrafo["PalabrasClaves"]))

                    parrafo_info = parrafo_utf8
                    for proteina  in parrafo["Proteinas"]:
                        parrafo_info = parrafo_info.replace(proteina, f"<Proteina>{proteina}</Proteina>")
                    
                    for palabra  in palabras_claves:
                        parrafo_info = parrafo_info.replace(palabra, f"<PalabraClave>{palabra}</PalabraClave>")

                    info_parrafo = {
                        "Parrafo":parrafo_utf8,
                        "ParrafoEtiquetado":parrafo_info,
                        "Proteinas":parrafo["Proteinas"],
                        "ComplejosPDBMencionados": proteina_search_complex,
                        "ParesProteinas":pares_parrafo,
                        "PalabrasClaves":palabras_claves,
                        "TipoFiltro":"Manual",
                        "Valido":"no"
                    }
                    if info_parrafo not in total_parrafos:
                        total_parrafos.append(info_parrafo)



        print("Total de proteinas", len(listado_proteinas))
        print("Total de proteinas en listado de complejos",len(listado_proteinas_complejos))
        print("Numero de pares",len(listado_pares))


        listado_proteinas = []
        listado_proteinas_complejos = []
        listado_pares = []

        for i in resultados_modelos:
            pares_parrafo = []
            proteina_search_complex = []
            for proteina in i["Proteinas"]:
                listado_proteinas.append(proteina)
                if proteina in listadoscomplejospdb:
                    listado_proteinas_complejos.append(proteina)
                    proteina_search_complex.append(proteina)
            for par in i["Pares"]:
                if par[1] not in proteina_search_complex and par[0] not in proteina_search_complex:
                    listado_pares.append(par)
                    pares_parrafo.append(par)
            if len(pares_parrafo) > 0:
                parrafo_utf8 = str(str(i["Parrafo"]).encode('ascii', 'ignore').decode('ascii'))

                doc = jnlpba(parrafo_utf8)
                palabras_claves =  list(set(fl.ValidarPalabrasClaves(doc)[1]))

                parrafo_info = parrafo_utf8
                for proteina  in i["Proteinas"]:
                    parrafo_info = parrafo_info.replace(proteina, f"<Proteina>{proteina}</Proteina>")
                
                for palabra  in palabras_claves:
                    parrafo_info = parrafo_info.replace(palabra, f"<PalabraClave>{palabra}</PalabraClave>")

                info_parrafo = {
                        "Parrafo":parrafo_utf8,
                        "ParrafoEtiquetado":parrafo_info,
                        "Proteinas":i["Proteinas"],
                        "ComplejosPDBMencionados": proteina_search_complex,
                        "ParesProteinas":pares_parrafo,
                        "PalabrasClaves":palabras_claves,
                        "TipoFiltro":"Modelos",
                        "Valido":"no"
                    }
                
                if info_parrafo not in total_parrafos:
                    total_parrafos.append(info_parrafo)



        print("Total de proteinas",len(listado_proteinas))
        print("Total de proteinas en listado de complejos",len(listado_proteinas_complejos))
        print("Numero de pares",len(listado_pares))


        print("Total Parrafos ",len(total_parrafos))
        with open(f"Documentos/AnalisisParesProteinasManualModelos.json", 'w', encoding='utf-8') as f:
            json.dump(total_parrafos, f)



        # Total interacciones entre proteinas pdb-biogrid 55523
        # Total proteinas asociadas pdb 581
        # Total de proteinas 569
        # Total de proteinas en listado de complejos 28
        # Numero de pares 956
        # Total de proteinas 250
        # Total de proteinas en listado de complejos 14
        # Numero de pares 236
        # Total Parrafos  220

    if opcion == "AnalisisPares":

        with open("Documentos/AnalisisParesProteinasManualModelos.json", "r") as archivo:
            textos_analisis = json.load(archivo)
        
        textos_validos = []
        cantidad_pares = 0
        for i in textos_analisis:
            if i["Valido"] == "si":
                cantidad_pares = cantidad_pares + len(i["ParesProteinas"])
                textos_validos.append(i)
        
        print("Total textos ",len(textos_analisis))
        print("Textos filtrados ",len(textos_validos))
        print("Cantidad pares ",cantidad_pares)

        with open(f"Documentos/AnalisisFinalPares.json", 'w', encoding='utf-8') as f:
            json.dump(textos_validos, f)

        # Inicio Proceso
        # AnalisisPares
        # Total textos  220
        # Textos filtrados  119
        # Cantidad pares  849
