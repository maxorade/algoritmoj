import requests
import sys
import os
import Herramientas.DescargarAbstracts as da
import json
import xml.etree.ElementTree as ET
import pandas as pd
from tqdm import tqdm
import numpy as np
import joblib
import spacy
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import itertools
from Bio.PDB import PDBList


# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    opcion = sys.argv[1]

    if opcion == "GenerarListadoPmcId":

        query = "protein-protein interactions"

        listados_ids = []

        for i in range(10):
            inicio = i * 1000
            cantidad_docs = (i + 1) * 1000

            listados_ids = listados_ids + da.ListadoIdsArticulosAbstractsQuerys(
                query=query, index_inicio=inicio, cantidad_maxima_articulos=cantidad_docs)

        # listados_ids =  da.ListadoIdsArticulosAbstractsQuerys(query="protein-protein interactions")

        print(len(listados_ids))

        with open(f"Documentos/Articulos/ListadoPMCIds_{query}.json", "w") as archivo:
            json.dump(listados_ids, archivo)

    if opcion == "FiltrarListadoPmcIdRepetidos":

        query = "protein-protein interactions"

        ruta_ids = f"Documentos/Articulos/ListadoPMCIds_{query}.json"

        with open(ruta_ids, "r") as archivo:
            listados_ids = json.load(archivo)

        print(len(listados_ids))

        listado_filtrado = []
        for i in listados_ids:
            if i not in listado_filtrado:
                listado_filtrado.append(i)

        print(len(listado_filtrado))

        with open(f"Documentos/Articulos/ListadoPMCIds_{query}Filtrado.json", "w") as archivo:
            json.dump(listado_filtrado, archivo)

    if opcion == "DescargarArticulos":

        query = "protein-protein interactions"

        ruta_ids = f"Documentos/Articulos/ListadoPMCIds_{query}Filtrado.json"

        with open(ruta_ids, "r") as archivo:
            listados_ids = json.load(archivo)

        print(len(listados_ids))

        listado_doc_not = []

        for i in tqdm(range(len(listados_ids[0:10000]))):
            articulo = listados_ids[i]
            ruta_articulo_carpeta = f"Documentos/Articulos/PMC/{articulo}"
            ruta_articulo = f"Documentos/Articulos/PMC/{articulo}/{articulo}.xml"

            if os.path.exists(ruta_articulo_carpeta) == False:
                os.mkdir(ruta_articulo_carpeta)

            if os.path.exists(ruta_articulo) == False:
                try:
                    da.DescargarXMLPMC(articulo, ruta_articulo_carpeta)
                except:
                    listado_doc_not.append(articulo)

        with open(f"Documentos/Articulos/ListadoPMCIds_{query}NoDescargados.json", "w") as archivo:
            json.dump(listado_doc_not, archivo)

        # if os.path.exists(ruta_papers) == False:
        #     os.mkdir(ruta_papers)

    if opcion == "ExtraccionParrafos":

        carpeta_abs = os.listdir("Documentos/Articulos/PMC")

        for i in tqdm(range(len(carpeta_abs))):
            carpeta = carpeta_abs[i]
            ruta_carpeta_xml = f"Documentos/Articulos/PMC/{carpeta}"
            xmls = os.listdir(ruta_carpeta_xml)

            for xml in xmls:
                if os.path.exists(ruta_carpeta_xml + f"/{carpeta}.json") == False:
                    ruta_xml = f"Documentos/Articulos/PMC/{carpeta}/{xml}"
                    parrafos = da.ExtraccionParrafosXML(ruta_xml)

                    with open(ruta_carpeta_xml + f"/{carpeta}.json", "w") as archivo:
                        json.dump(parrafos, archivo)

        print(len(carpeta_abs))

    if opcion == "AnalisisParrafosBusquedaProteinas":

        pdblist = PDBList()
        all_pdb_ids = pdblist.get_all_entries()

        carpeta_abs = os.listdir("Documentos/Articulos/PMC")

        listado_articulos_proteinas = []
        listado_textos_proteinas = []

        for i in tqdm(range(len(carpeta_abs))):
            carpeta = carpeta_abs[i]
            ruta_carpeta_xml = f"Documentos/Articulos/PMC/{carpeta}"
            archivos = os.listdir(ruta_carpeta_xml)
            for ar in archivos:
                if os.path.exists(ruta_carpeta_xml + f"/{carpeta}.json") == True:
                    with open(ruta_carpeta_xml + f"/{carpeta}.json", "r") as archivo:
                        parrafos = json.load(archivo)
                    listado_total_proteinas = []
                    paper_valido = False
                    for index_p, p in enumerate(parrafos):
                        matches = bp.IdentificarPDBId(texto=p)
                        proteinas = []
                        for proteina in matches:
                            proteina = proteina.upper()
                            try:
                                numero = float(proteina)
                            except ValueError:
                                if proteina in all_pdb_ids:  # check if the protein ID is in the list
                                    proteinas.append(proteina)

                        if len(proteinas) != 0:
                            paper_valido = True
                            datos = {
                                "PMCId": carpeta,
                                "Parrafo": p,
                                "NumeroParrafo": index_p,
                                "Proteinas": (', '.join(proteinas))
                            }

                            listado_total_proteinas = listado_total_proteinas + proteinas

                            listado_textos_proteinas.append(datos)
                    if paper_valido == True:
                        frecuencias = {}

                        # Cuenta la cantidad de veces que se repite cada elemento en la lista
                        for elemento in set(listado_total_proteinas):
                            frecuencias[elemento] = listado_total_proteinas.count(
                                elemento)

                        datos_articulo = {
                            "PMCId": carpeta,
                            "Proteinas": frecuencias
                        }

                        if datos_articulo not in listado_articulos_proteinas:
                            listado_articulos_proteinas.append(datos_articulo)

        print("cantidad textos :", len(listado_articulos_proteinas))

        import pandas as pd

        df = pd.DataFrame(listado_textos_proteinas)

        df.to_csv(f"Documentos/Articulos/ParrafosProteinas.csv", index=False)

        with open("Documentos/Articulos/ArticulosProteinas.json", "w") as archivo:
            json.dump(listado_articulos_proteinas, archivo)

    if opcion == "AnalisisParesProteinas":

        with open("Documentos/Articulos/ArticulosProteinas.json", "r") as archivo:
            listado_articulos_proteinas = json.load(archivo)

        print("cantidad textos :", len(listado_articulos_proteinas))

        listado_filtrado = []
        listados_pmc_ids = []
        listado_filtrado_parrafos = []

        for i in listado_articulos_proteinas:
            if len(i["Proteinas"]) != 1:
                pair_order_list = itertools.permutations(
                    list(i["Proteinas"]), 2)

                listado_par_filtrado = []
                for par in list(pair_order_list):
                    par_prueba = (par[1], par[0])
                    if (par[1] not in par[0]) and (par[0] not in par[1]):
                        if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                            listado_par_filtrado.append(par)

                datos = {
                    "PMCId": i["PMCId"],
                    "Proteinas":  i["Proteinas"],
                    "PosiblesPares": listado_par_filtrado
                }

                listados_pmc_ids.append(i["PMCId"])
                listado_filtrado.append(datos)

        print("listado_filtrado", len(listado_filtrado))
        print("listados_pmc_ids", len(listados_pmc_ids))

        listado_parrafos = pd.read_csv(
            f"Documentos/Articulos/ParrafosProteinas.csv")
        listado_parrafos = listado_parrafos.values.tolist()

        print("listado_parrafos", len(listado_parrafos))

        for i in listado_parrafos:
            pmc_id = str(i[0])

            if pmc_id in listados_pmc_ids:
                datos = {
                    "PMCId": pmc_id,
                    "Parrafo": i[1],
                    "NumeroParrafo": i[2],
                    "Proteinas": i[3]
                }
                # listado_filtrado_parrafos.append(datos)

                if datos not in listado_filtrado_parrafos:
                    listado_filtrado_parrafos.append(datos)

        print("listado_filtrado_parrafos", len(listado_filtrado_parrafos))

        df = pd.DataFrame(listado_filtrado_parrafos)

        df.to_csv(f"Documentos/Articulos/ParrafosProteinasFiltro.csv", index=False)

        with open("Documentos/Articulos/ArticulosProteinasFiltro.json", "w") as archivo:
            json.dump(listado_filtrado, archivo)

        # 10008190

        # cantidad de textos inicial 10.000
        # cantidad textos : 2.629
        # listado_filtrado 1.034
        # listados_pmc_ids 1.034
        # listado_parrafos 12.088
        # listado_filtrado_parrafos 3.432

    if opcion == "FiltrarPruebasDocking":

        jnlpba = spacy.load('en_ner_jnlpba_md')

        with open("Documentos/Articulos/ArticulosProteinasFiltro.json", "r") as archivo:
            listado_articulos_proteinas = json.load(archivo)

        print("listado_articulos_proteinas", len(listado_articulos_proteinas))

        listado_pruebas_docking = [
            i for i in listado_articulos_proteinas if len(i["Proteinas"]) <= 3]

        print("listado_articulos_proteinas 3 a 2 proteinas",
              len(listado_pruebas_docking))

        listado_parrafos = pd.read_csv(
            f"Documentos/Articulos/ParrafosProteinasFiltro.csv")
        listado_parrafos = listado_parrafos.values.tolist()

        listado_parrafos_docking = []

        print("__________________")

        papers_docking = []

        for articulo in listado_pruebas_docking:
            # print("PMCId",  articulo["PMCId"] )
            # print("Proteinas",  articulo["Proteinas"] )
            validar = False
            list_parrafos = []
            for i in listado_parrafos:
                pmc_id = str(i[0])
                parrafo = i[1]
                numero_parrafo = i[2]
                proteinas = i[3]
                if articulo["PMCId"] == pmc_id:
                    proteinas = proteinas.split(",")
                    proteinas = [i.strip() for i in proteinas]
                    unique_list = list(set(proteinas))
                    if len(unique_list) > 1:
                        if "complex" in parrafo:
                            validar = True
                            doc = jnlpba(parrafo)
                            datos_parrafos = {
                                "NumeroParrafo": numero_parrafo,
                                "Proteinas": unique_list,
                                "Parrafo": str(str(parrafo).encode('ascii', 'ignore').decode('utf-8')),
                                "PalabrasClaves": fl.ValidarPalabrasClaves(doc)[1]
                            }
                            list_parrafos.append(datos_parrafos)
                            listado_parrafos_docking.append(i)

            if validar == True:
                datos = {
                    "PMCId": articulo["PMCId"],
                    "Parrafos": list_parrafos
                }
                papers_docking.append(datos)

            # print("__________________")

        articulos = [i[0] for i in listado_parrafos_docking]
        print("listado_parrafos_docking", len(listado_parrafos_docking))

        unique_list = list(set(articulos))
        print("listado articulos filtrado docking", len(unique_list))

        print("papers_docking: ", len(papers_docking))

        with open("Documentos/Articulos/ArticulosProteinasFiltro2_3Proteinas.json", 'w', encoding='utf-8') as f:
            json.dump(papers_docking, f)

        # listado_articulos_proteinas 1034
        # listado_articulos_proteinas 3 a 2 proteinas 769
        # __________________
        # listado_parrafos_docking 60
        # listado articulos filtrado docking 57

    if opcion == "UsoModelo":

        listado_parrafos = pd.read_csv(
            f"Documentos/Articulos/ParrafosProteinasFiltro.csv")

        X = np.asarray(listado_parrafos["Parrafo"])
        print(len(X))

        modelo = "AImed_NB_LEXICON_Model"
        info_modelo = modelo.split("_")
        ruta_resultados = f"Resultados/{info_modelo[1]}/Modelo"
        ruta_modelo = f"{ruta_resultados}/{modelo}.joblib"

        modelo = joblib.load(ruta_modelo)
        y_pred = modelo.predict(X)

        with open("Documentos/Articulos/ResultadosParrafosFiltrados5.json", 'w') as f:
            json.dump(y_pred.tolist(), f)
            print("Clasificación Terminada...")

    if opcion == "AnalisisResultadosClasificacion":

        with open("Documentos/Articulos/ResultadosParrafosFiltrados5.json", "r") as archivo:
            resultados = json.load(archivo)

        with open("Documentos/Articulos/ArticulosProteinasFiltro2_3Proteinas.json", "r") as archivo:
            papers_docking = json.load(archivo)

        with open("Documentos/Articulos/ArticulosProteinasFiltro.json", "r") as archivo:
            listado_articulos_proteinas = json.load(archivo)

        resultados_positivos = [index for index,
                                i in enumerate(resultados) if i == 1]

        listado_parrafos = pd.read_csv(
            f"Documentos/Articulos/ParrafosProteinasFiltro.csv")
        listado_parrafos = listado_parrafos.values.tolist()

        listado_parrafos_positivos = []

        for i in resultados_positivos:
            listado_parrafos_positivos.append(listado_parrafos[i])

        listado_filtro_manual = [i["PMCId"] for i in papers_docking]

        print("papers filtrados manualmente", len(listado_filtro_manual))

        listado_pmc_id = []
        for i in listado_parrafos_positivos:
            pmc_id = str(i[0])
            parrafo = i[1]
            proteinas = i[3]
            # print(parrafo)
            # print(proteinas)
            # articulo = [articulo for articulo in listado_articulos_proteinas if articulo["PMCId"] == str(pmc_id)]
            # print(articulo)
            # print("________________")
            if pmc_id not in listado_pmc_id and pmc_id in listado_filtro_manual:
                listado_pmc_id.append(pmc_id)

        print("total articulos : ", len(listado_pmc_id))

        print(listado_pmc_id)

    if opcion == "DescargarProteinasPdb":
        with open("Documentos/Articulos/ArticulosProteinasFiltro2_3Proteinas.json", "r") as archivo:
            papers_docking = json.load(archivo)

        for i in papers_docking:
            ruta_folder = "Documentos/Articulos/PDB"
            if os.path.exists(f"{ruta_folder}/{i['PMCId']}") == False:
                os.mkdir(f"{ruta_folder}/{i['PMCId']}")

            ruta_pmcid = f"{ruta_folder}/{i['PMCId']}"

            if os.path.exists(f"{ruta_pmcid}/GRAMM") == False:
                os.mkdir(f"{ruta_pmcid}/GRAMM")

            if os.path.exists(f"{ruta_pmcid}/pyDockWeb") == False:
                os.mkdir(f"{ruta_pmcid}/pyDockWeb")

            if os.path.exists(f"{ruta_pmcid}/ClusPro") == False:
                os.mkdir(f"{ruta_pmcid}/ClusPro")

            if os.path.exists(f"{ruta_pmcid}/HEX") == False:
                os.mkdir(f"{ruta_pmcid}/HEX")

            if os.path.exists(f"{ruta_pmcid}/InterEvDock") == False:
                os.mkdir(f"{ruta_pmcid}/InterEvDock")

            if os.path.exists(f"{ruta_pmcid}/ZDOCK") == False:
                os.mkdir(f"{ruta_pmcid}/ZDOCK")

            if os.path.exists(f"{ruta_pmcid}/RosettaDock") == False:
                os.mkdir(f"{ruta_pmcid}/RosettaDock")

            for parrafos in i["Parrafos"]:
                for proteina in parrafos["Proteinas"]:
                    if os.path.exists(f"{ruta_pmcid}/{proteina}.pdb") == False:
                        bp.GuardarProteina(proteina, f"{ruta_pmcid}")

                        # os.mkdir(f"{ruta_folder}/{i['PMCId']}")

        datos_docking = []

        carpeta_papers_proteinas = os.listdir("Documentos/Articulos/PDB")
        carpeta_papers_proteinas = [
            i for i in carpeta_papers_proteinas if ".csv" not in i]

        for i in carpeta_papers_proteinas:
            carpeta_proteinas = os.listdir(f"Documentos/Articulos/PDB/{i}")
            carpeta_proteinas = [j.replace(".pdb", "")
                                 for j in carpeta_proteinas if ".pdb" in j]

            pair_order_list = itertools.permutations(carpeta_proteinas, 2)
            listado_par_filtrado = []
            for par in list(pair_order_list):
                par_prueba = (par[1], par[0])
                if (par[1] not in par[0]) and (par[0] not in par[1]):
                    if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                        listado_par_filtrado.append(par)

            for par in listado_par_filtrado:
                datos = {
                    "Analisis": "No",
                    "PMCId": i,
                    "CodigoProyecto": f"{par[0]}_{par[1]}",
                    "Protena1": f"{par[0]}",
                    "Protena2": f"{par[1]}",
                    "GRAMM": "No",
                    "GRAMMProblemas": "No",
                    "pyDockWeb": "No",
                    "pyDockWebProblemas": "No",
                    "ClusPro": "No",
                    "ClusProProblemas": "No",
                    "HEX": "No",
                    "HEXProblemas": "No",
                    "InterEvDock": "No",
                    "InterEvDockProblemas": "No",
                    "ZDOCK": "No",
                    "ZDOCKProblemas": "No",
                    "RosettaDock": "No",
                    "RosettaDockProblemas": "No",
                }

                datos_docking.append(datos)

        df = pd.DataFrame(datos_docking)

        df.to_csv(f"Documentos/Articulos/PDB/AnalisisDocking.csv", index=False)

    if opcion == "AnalisisFaltante":

        # python AnalisisTextosComplejosDocking.py AnalisisFaltante
        with open("Documentos/Articulos/ArticulosProteinasFiltro2_3Proteinas.json", "r") as archivo:
            papers_docking = json.load(archivo)

        print(len(papers_docking))
        
        jnlpba = spacy.load('en_ner_jnlpba_md')

        with open("Documentos/Articulos/ArticulosProteinasFiltro.json", "r") as archivo:
            listado_articulos_proteinas = json.load(archivo)

        print("listado_articulos_proteinas", len(listado_articulos_proteinas))

        filtrado = []
        for i in listado_articulos_proteinas:
            for j in papers_docking:
                if i["PMCId"] != j["PMCId"]:
                    # print(i["PMCId"], j["PMCId"])
                    if i not in filtrado :
                        filtrado.append(i)
        
        print("filtrado", len(filtrado))

        # listado_pruebas_docking = [
        #     len(i["Proteinas"]) for i in listado_articulos_proteinas if len(i["Proteinas"]) <= ]

        # print("listado_articulos_proteinas 3 a 2 proteinas",
        #       len(listado_pruebas_docking))

        listado_parrafos = pd.read_csv(
            f"Documentos/Articulos/ParrafosProteinasFiltro.csv")
        listado_parrafos = listado_parrafos.values.tolist()

        listado_parrafos_docking = []

        print("__________________")

        papers_docking = []
        cantidad_pares = 0 

        for articulo in filtrado:
            # print("PMCId",  articulo["PMCId"] )
            # print("Proteinas",  articulo["Proteinas"] )
            validar = False
            list_parrafos = []
            for i in listado_parrafos:
                pmc_id = str(i[0])
                parrafo = i[1]
                numero_parrafo = i[2]
                proteinas = i[3]
                if articulo["PMCId"] == pmc_id:
                    proteinas = proteinas.split(",")
                    proteinas = [i.strip() for i in proteinas]
                    unique_list = list(set(proteinas))
                    if len(unique_list) > 1:
                        if "complex" in parrafo or "protein-protein" in parrafo or "interaction" in parrafo:
                            validar = True
                            doc = jnlpba(parrafo)

                            pair_order_list = itertools.permutations(unique_list, 2)
                            listado_par_filtrado = []
                            for par in list(pair_order_list):
                                par_prueba = (par[1], par[0])
                                if (par[1] not in par[0]) and (par[0] not in par[1]):
                                    if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                                        listado_par_filtrado.append(par)

                            cantidad_pares = cantidad_pares + len(listado_par_filtrado)
                            datos_parrafos = {
                                "NumeroParrafo": numero_parrafo,
                                "Proteinas": unique_list,
                                "Pares": listado_par_filtrado,
                                "Parrafo": str(str(parrafo).encode('ascii', 'ignore').decode('utf-8')),
                                "PalabrasClaves": fl.ValidarPalabrasClaves(doc)[1]
                            }
                            list_parrafos.append(datos_parrafos)
                            listado_parrafos_docking.append(i)

            if validar == True:
                datos = {
                    "PMCId": articulo["PMCId"],
                    "Parrafos": list_parrafos
                }
                papers_docking.append(datos)

            # print("__________________")


        print("cantidad pares", cantidad_pares)
        articulos = [i[0] for i in listado_parrafos_docking]
        print("listado_parrafos_docking", len(listado_parrafos_docking))

        unique_list = list(set(articulos))
        print("listado articulos filtrado docking", len(unique_list))

        with open("Documentos/Articulos/ArticulosProteinasAnalisis_1.json", 'w', encoding='utf-8') as f:
            json.dump(papers_docking, f)

        # Inicio Proceso
        # 57
        # listado_articulos_proteinas 1034
        # filtrado 1034
        # __________________
        # cantidad pares 1006
        # listado_parrafos_docking 192
        # listado articulos filtrado docking 159
