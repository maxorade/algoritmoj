import requests
import sys
import os
import Herramientas.DescargarAbstracts as da
import json
import xml.etree.ElementTree as ET
import pandas as pd
from tqdm import tqdm
import numpy as np
import joblib
import spacy
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import itertools
from Bio.PDB import PDBList

def ValidarPar(pares_biogrid , listado_pares ):
    for par in listado_pares:
        par = list(par)
        par_inverso = [par[1],par[0]]
        if par in pares_biogrid or  par_inverso in pares_biogrid:
            print(par)



# Mainpi
if __name__ == "__main__":

    print("Inicio Proceso")
    opcion = sys.argv[0]

#clasificacion de texto a partir de los mejores clasificadores 

    cantidad_maxima_proteinas_doc = 10

    modelos =  ["AImed_NB_LEXICONspacy_Model",
    "AImed_SVM_TFIDF_LEXICONspacy_Model",
    "Biocreative3_RF_TFIDF_LEXICONninguno_Model",
    "AImed_KNN_MULTIPLE_TFIDFspacy_Model"]

    ruta_pares_biogrid_pdb  = "Documentos/BiogridPDB/ParesProteinasBiogrid.json"

    ruta_articulos = "Documentos/Articulos/PMC"

    ruta_parrafos = f"Documentos/Articulos/ParrafosProteinas.csv"

    with open(ruta_pares_biogrid_pdb, "r") as archivo:
        pares_biogrid = json.load(archivo)
    

    # Extracción de parrafos
    # # carpeta_abs = os.listdir(ruta_articulos)

    # # listado_parrafos = []

    # # for i in tqdm(range(len(carpeta_abs))):
    # #     carpeta = carpeta_abs[i]
    # #     ruta_carpeta_xml = f"Documentos/Articulos/PMC/{carpeta}"
    # #     archivos = os.listdir(ruta_carpeta_xml)
    # #     for ar in archivos:
    # #         if os.path.exists(ruta_carpeta_xml + f"/{carpeta}.json") == True:
    # #             with open(ruta_carpeta_xml + f"/{carpeta}.json", "r") as archivo:
    # #                 parrafos = json.load(archivo)
    # #             listado_parrafos =  listado_parrafos + parrafos

    # # print("cantidad articulos: ",len(carpeta_abs) )
    # # print("cantidad parrafos: ",len(listado_parrafos) )

    # # with open("Documentos/Articulos/ParrafosPMC10000.json", 'w') as f:
    # #         json.dump(listado_parrafos, f)

    # cantidad articulos:  10000
    # cantidad parrafos:  836578

    # Uso de modelos
    df_parrafos = pd.read_csv(ruta_parrafos)
    listado_parrafos = df_parrafos.values.tolist()

    print("cantidad de parrafos", len(listado_parrafos))

    # # for modelo in modelos:
    # #     info_modelo = modelo.split("_")
    # #     ruta_resultados = f"Resultados/{info_modelo[1]}/Modelo"
    # #     ruta_modelo = f"{ruta_resultados}/{modelo}.joblib"
    # #     print(ruta_modelo)

    # #     X = np.asarray(df_parrafos["Parrafo"])
        
    # #     modelo_clasificador = joblib.load(ruta_modelo)
    # #     y_pred = modelo_clasificador.predict(X)

    # #     with open(f"Documentos/Articulos/ResultadosClasificadores/Resultados{modelo}", 'w') as f:
    # #         json.dump(y_pred.tolist(), f)
    # #         print("Clasificación Terminada...")

    resultados_obtenidos = []
    cantidad_repetidos = []

    jnlpba = spacy.load('en_ner_jnlpba_md')

    for modelo in modelos:
        ruta_resultados_modelos  = f"Documentos/Articulos/ResultadosClasificadores/Resultados{modelo}"
        ruta_clasificadores = f"Documentos/Articulos/ResultadosClasificadores/"

        with open(ruta_resultados_modelos, "r") as archivo:
            resultados = json.load(archivo)

        resultados_positivos = [index for index,
                                    i in enumerate(resultados) if i == 1]
        
        for index, value in enumerate(resultados):
            if value == 1:
                pmc_id = df_parrafos["PMCId"][index]

                proteinas = str(df_parrafos["Proteinas"][index])
                proteinas =  [prot.strip() for prot in proteinas.split(",")]
                proteinas = list(set(proteinas))

                parrafo = df_parrafos["Parrafo"][index]

                doc = jnlpba(parrafo)
                palabras_claves = fl.ValidarPalabrasClaves(doc)[1]

                validar_cantidad_proteinas = False
                validar_palabras_claves = False
                if len(proteinas) <= cantidad_maxima_proteinas_doc and  len(proteinas) >=2:
                    validar_cantidad_proteinas = True

                # if "complex" in parrafo or "protein-protein" in parrafo or "interaction" in parrafo:
                #     validar_cantidad_proteinas = True
                    
                if len(palabras_claves) >= 1:  
                    validar_palabras_claves = True

                if validar_cantidad_proteinas == True and validar_palabras_claves == True:  
                    pair_order_list = itertools.permutations(list(proteinas), 2)
                    listado_par_filtrado = []
                    for par in list(pair_order_list):
                        par_prueba = (par[1], par[0])
                        if (par[1] not in par[0]) and (par[0] not in par[1]):
                            if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                                listado_par_filtrado.append(par)

                    # print("proteinas",proteinas)
                    # print("listado_par_filtrado",list(listado_par_filtrado[0]))

#####################################333
                    # ValidarPar(pares_biogrid , listado_par_filtrado )
                      
                    if index not in resultados_obtenidos:
                        datos = {
                            # "PMCId":pmc_id,
                            "Parrafo":parrafo,
                            "Proteinas":proteinas,
                            "Pares": listado_par_filtrado,
                            # "PalabrasClaves":palabras_claves

                        }
                        resultados_obtenidos.append(datos)


   

        
        # for valor in resultados_positivos:
        #     if valor not in  resultados_obtenidos:
        #         resultados_obtenidos.append(valor)
        #     else: 
        #         cantidad_repetidos.append(valor)
        
        # print(len(resultados_positivos))

    # Cantidad Valores Repetidos:  2
    print("cantidad textos clasificados: ",len(resultados_obtenidos) )
    with open(f"Documentos/Articulos/ResultadosClasificadores/ResultadosParrafosClasificados.json", 'w', encoding='utf-8') as f:
        json.dump(resultados_obtenidos, f)
