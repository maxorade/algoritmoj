import Herramientas.BusquedaProteinas as bp
from tqdm import tqdm
import json
import itertools


rango_inicial = 10561 
rango_final = rango_inicial + 11486

with open('Documentos/ListadoProteinNucleicAcidComplexes.json', 'r') as file:
    listadoscomplejospdb = json.load(file)

with open(f"Documentos/BiogridPDB/AsociarPDB/ASOCIACION_BIOGRID_PDB_{rango_inicial}_{rango_final}.txt", 'w') as f:

    for i in tqdm(range(len(listadoscomplejospdb[rango_inicial:rango_final]))):
        index =  i +  rango_inicial
        pdb_id = listadoscomplejospdb[index]
        try: 
            # print(pdb_id)
            mapeo = bp.MapeoPDBUniprot(pdb_id=pdb_id)
            # print(mapeo)
            if mapeo != None:
                # print(mapeo)
                uniprot_ids = [i["UniprotId"] for i in mapeo]
                # print(uniprot_ids)

                pair_order_list = itertools.permutations(uniprot_ids, 2)
                listado_pares_uniprot = []
                for par in list(pair_order_list):
                    par_prueba = (par[1], par[0])
                    if (par[1] not in par[0]) and (par[0] not in par[1]):
                        if (par_prueba in listado_pares_uniprot) == False and (par in listado_pares_uniprot) == False:
                            listado_pares_uniprot.append(par)

                # print(listado_pares_uniprot)

                for i in listado_pares_uniprot:
                    uniprot_id1 = i[0]
                    uniprot_id2 = i[1]

                    chain1 = [i["Chain"] for i in mapeo if uniprot_id1  == i["UniprotId"] ][0]
                    chain2 = [i["Chain"] for i in mapeo if uniprot_id2  == i["UniprotId"] ][0]

                    busqueda_biogrid = bp.BusquedaBiogridParesUniprot(uniprot_id1=uniprot_id1,
                                                        uniprot_id2=uniprot_id2)

                    if len(busqueda_biogrid) > 0:
                        busqueda_biogrid = '|'.join([i["BiogridId"] for i in busqueda_biogrid])  

                        datos_integrados = {
                            "PdbId": pdb_id,
                            "UniprotId1": uniprot_id1,
                            "Chain1":chain1,
                            "UniprotId2": uniprot_id2,
                            "Chain2":chain2,
                            "BiogridIds":busqueda_biogrid
                        }

                        informacion_write = f"{index},{pdb_id},{uniprot_id1},{chain1},{uniprot_id2},{chain2},{busqueda_biogrid}" + "\n"
                        f.write(informacion_write)
                        f.flush()
                        # print(datos_integrados)
        except:
            print(index)