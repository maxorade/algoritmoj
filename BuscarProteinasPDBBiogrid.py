from tqdm import tqdm
import pandas as pd

# file_path = "Documentos/BIOGRID-ALL-4.4.219.tab3.txt"

# df = pd.read_csv(file_path, sep="\t", low_memory=False)


# # print(listado_valores[0])
# columnas = ['#BioGRID Interaction ID',
#  'Entrez Gene Interactor A',
#  'Entrez Gene Interactor B',
#  'BioGRID ID Interactor A',
#  'BioGRID ID Interactor B',
#  'Systematic Name Interactor A',
#  'Systematic Name Interactor B',
#  'Official Symbol Interactor A',
#  'Official Symbol Interactor B',
#  'Synonyms Interactor A',
#  'Synonyms Interactor B',
#  'Experimental System',
#  'Experimental System Type',
#  'Author',
#  'Publication Source',
#  'Organism ID Interactor A',
#  'Organism ID Interactor B',
#  'Throughput',
#  'Score',
#  'Modification',
#  'Qualifications',
#  'Tags',
#  'Source Database',
#  'SWISS-PROT Accessions Interactor A',
#  'TREMBL Accessions Interactor A',
#  'REFSEQ Accessions Interactor A',
#  'SWISS-PROT Accessions Interactor B',
#  'TREMBL Accessions Interactor B',
#  'REFSEQ Accessions Interactor B',
#  'Ontology Term IDs',
#  'Ontology Term Names',
#  'Ontology Term Categories',
#  'Ontology Term Qualifier IDs',
#  'Ontology Term Qualifier Names',
#  'Ontology Term Types',
#  'Organism Name Interactor A',
#  'Organism Name Interactor B']


# datos_biogrid = []

# for index, row in df.iterrows():
#     datos = {
#         "BiogridId":row['#BioGRID Interaction ID'],
#         "SwissProtIdA":row['SWISS-PROT Accessions Interactor A'],
#         "SwissProtIdB":row['SWISS-PROT Accessions Interactor B']
#     }
#     datos_biogrid.append(datos)


# df = pd.DataFrame(datos_biogrid)
# df.to_csv(f"Documentos/BIOGRID_SWISSPROT.csv", index=False)

##################################

import requests


def GetCodigoPDBUniprot(uniprot_id):
    # Send a GET request to the UniProt API to retrieve the entry information
    url = f"https://www.uniprot.org/uniprot/{uniprot_id}.xml"
    response = requests.get(url)

    # Parse the XML response and extract the PDB code
    xml = response.content.decode("utf-8")
    pdb_code = ""
    for line in xml.split("\n"):
        if "<dbReference type=\"PDB\"" in line:
            start = line.find("id=") + 4
            end = line.find("\"", start)
            pdb_code = line[start:end]
            break

    return pdb_code


def GetCodigoPDBUniprotJSON(uniprot_id:str):
    # Send a GET request to the UniProt API to retrieve the entry information
    url = f"https://www.uniprot.org/uniprot/{uniprot_id}.json"
    response = requests.get(url)

    pdb_code = ""
    
    data_json = response.json()

    if "uniProtKBCrossReferences" in data_json:
        data_json = data_json["uniProtKBCrossReferences"]
        for i in data_json:
            if i["database"] == "PDB":
                pdb_code = i["id"]
                break
 
    return pdb_code

# print(GetCodigoPDBUniprotJSON(uniprot_id = "P45985") )
# ##################################

rango_inicial = 55000
rango_final =  rango_inicial + 5000

print("rango_inicial",rango_inicial)
print("rango_final",rango_final)


file_path = f"Documentos/BIOGRID_SWISSPROT.csv"

df = pd.read_csv(file_path, low_memory=False)


listado_pares = df.values.tolist()
print(len(df["BiogridId"]))

datos_biogrid = []

with open(f"Documentos/BIOGRID_SWISSPROT_PDB_{rango_inicial}_{rango_final}.txt", 'w') as f:

# f = open(f"Documentos/BIOGRID_SWISSPROT_PDB_{rango_inicial}_{rango_final}.txt", 'w') 

    for i in tqdm(range(len(listado_pares[rango_inicial:rango_final]))):
        index =  i +  rango_inicial
        biogridid = listado_pares[index][0]
        uniprotA = listado_pares[index][1]
        uniprotB = listado_pares[index][2]
        uniprotA_list = []
        uniprotB_list = []
        pdbA = ""
        pdbB = ""

        if uniprotA == "-" or uniprotB == "-":
            continue

        if "|" in uniprotA:
            uniprotA_list = str(uniprotA).split("|")

        if "|" in uniprotB:
            uniprotB_list = str(uniprotB).split("|")

        # PDB A

        if len(uniprotA_list) > 0:
            pdbA_list = []
            for uniprot_id in uniprotA_list:
                pdb_id = GetCodigoPDBUniprotJSON(uniprot_id)
                # if len(pdb_id) > 0: 
                pdbA_list.append(pdb_id)

            # if len(pdbA_list) > 0:
            pdbA = '|'.join(pdbA_list)
        else:
            pdbA = GetCodigoPDBUniprotJSON(uniprotA)

        # PDB B

        if len(uniprotB_list) > 0:
            pdbB_list = []
            for uniprot_id in uniprotA_list:
                pdb_id = GetCodigoPDBUniprotJSON(uniprot_id)
                # if len(pdb_id) > 0:
                pdbB_list.append(pdb_id)

            # if len(pdbB_list) > 0:
            pdbB = '|'.join(pdbB_list)
        else:
            pdbB = GetCodigoPDBUniprotJSON(uniprotB)

        if len(pdbA) > 0 and len(pdbB) > 0:
            datos = {
                "BiogridId": biogridid,
                "SwissProtIdA": uniprotA,
                "SwissProtIdB": uniprotB,
                "PdbA": pdbA,
                "PdbB": pdbB
            }

            datos_biogrid.append(datos)
            informacion_write = f"{index},{biogridid},{uniprotA},{uniprotB},{pdbA},{pdbB}" + "\n"
            # print(informacion_write)
            f.write(informacion_write)
            f.flush()


print("Total pares de proteinas", len(datos_biogrid))
# f.close()

df = pd.DataFrame(datos_biogrid)
df.to_csv(f"Documentos/BIOGRID_SWISSPROT_PDB_{rango_inicial}_{rango_final}.csv", index=False)
