import requests
import sys
import os
import Herramientas.DescargarAbstracts as da
import json
import xml.etree.ElementTree as ET
import pandas as pd
from tqdm import tqdm
import re
import numpy as np
import joblib
import spacy
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl




# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    opcion = sys.argv[1]

    ruta = "Documentos/DatosProfesora/"


    if opcion == "DescargarComplejosUniprotId":
        id = "P04637"
        ruta = f"Documentos/Uniprot"

        ruta_registro = f"{ruta}/Abstracts/{id}"

        if os.path.exists(ruta_registro) == False:
            os.mkdir(ruta_registro)


        ruta_detalle = f"Documentos/Uniprot/DetalleCodigosComplejosQuery{id}.csv"

        listado_complejos = pd.read_csv(ruta_detalle)
        listado_complejos = listado_complejos.values.tolist()


        for i in tqdm(range(len(listado_complejos))):
            uniprot_id = listado_complejos[i][0]
            referencias = listado_complejos[i][4]

            if len(referencias) > 1:

                referencias = str(referencias).split(",")

                ruta_abs = f"{ruta_registro}/{uniprot_id}"

                if os.path.exists(ruta_abs) == False:
                    os.mkdir(ruta_abs)

                for pubmed_id in referencias:
                    if os.path.exists(f"{ruta_abs}/{pubmed_id}.xml") == False:
                        da.DescargarXMLPubmed(pubmed_id, ruta_abs )

        # pubmed_id = "11124955"
      

        # print(f"{ruta_registro}/{pubmed_id}" )

        # da.DescargarXMLPubmed(pubmed_id, ruta_registro )

        # print("prueba")

    if opcion == "BuscarInfoComplejosUniprotId":
        id = "P04637"
        ruta = f"Documentos/Uniprot"
        ruta_complejos  = f"{ruta}/CodigosComplejosQuery{id}.json"
        # bp.BuscarComplejosUniprot(uniprot_id=uniprot_id,ruta=ruta)

        with open(ruta_complejos, 'r') as f:
            listado_complejos = json.load(f)

        print(len(listado_complejos))
        print()

        listado_complejos[1]
        listado_prot = []
        listado_nombres = []

        for i in tqdm(range(len(listado_complejos))):
            proteina = listado_complejos[i]

            valido, uniprot_id, nombres_proteina, pdb_ids, referencias_listado = bp.BuscarInformacionUniprot(
                query=proteina)
            
            if valido == True:

                listado_nombres = listado_nombres + nombres_proteina

                nombres_proteina = (', '.join(nombres_proteina))
                pdb_ids = (', '.join(pdb_ids))
                referencias_listado = (', '.join(referencias_listado))

                data = {
                    "Proteina": proteina,
                    "UniprotId": uniprot_id,
                    "Nombres": nombres_proteina,
                    "PDBIds": pdb_ids,
                    "Referencias": referencias_listado
                }

                listado_prot.append(data)

        df = pd.DataFrame(listado_prot)

        df.to_csv(f"{ruta}/DetalleCodigosComplejosQuery{id}.csv", index=False)
        with open(f"{ruta}/NombreProteinasCodigosComplejosQuery{id}.json", 'w') as f:
            json.dump(listado_nombres, f)
    

    if opcion == "BuscarInfoUniprotComplejosPositivos":

        # ComplejosTransient - ComplejosObligate

        tipo = "ComplejosObligate"

        listado_complejos = pd.read_csv(ruta + f"{tipo}.csv")
        listado_complejos = listado_complejos.values.tolist()

        listado_prot = []
        listado_nombres = []

        for i in tqdm(range(len(listado_complejos))):
            proteina = listado_complejos[i][1]

            valido, uniprot_id, nombres_proteina, pdb_ids, referencias_listado = bp.InformacionUniprotPdbId(
                pdb_id=proteina)
            
            if valido == True:

                listado_nombres = listado_nombres + nombres_proteina

                nombres_proteina = (', '.join(nombres_proteina))
                pdb_ids = (', '.join(pdb_ids))
                referencias_listado = (', '.join(referencias_listado))

                data = {
                    "Proteina": proteina,
                    "UniprotId": uniprot_id,
                    "Nombres": nombres_proteina,
                    "PDBIds": pdb_ids,
                    "Referencias": referencias_listado
                }

                listado_prot.append(data)

        df = pd.DataFrame(listado_prot)

        df.to_csv(f"{ruta}Uniprot/{tipo}Uniprot.csv", index=False)

        with open(f"{ruta}Uniprot/{tipo}NombresProteinas.json", 'w') as f:
            json.dump(listado_nombres, f)


    if opcion == "DescargarArticulosComplejos":

        # ComplejosTransient - ComplejosObligate
        listado_complejos = pd.read_csv(ruta + "ComplejosTransient.csv")
        listado_complejos = listado_complejos.values.tolist()

        listado_doc_not = []

        for i in tqdm(range(len(listado_complejos))):
            id = listado_complejos[i][0]
            pdb_id = listado_complejos[i][1]
            nombre = listado_complejos[i][4]
            ruta_papers = f"{ruta}/Articulos/{id}"

            if os.path.exists(ruta_papers) == False:
                os.mkdir(ruta_papers)

            listado_ids_papers = da.ListadoPMCIds(pdb_id)
            listado_ids_papers = [j for j in da.ListadoPMCIds(
                nombre) if j not in listado_ids_papers]

            for id_doc in listado_ids_papers:
                if os.path.exists(f"{ruta_papers}/id_doc") == False:
                    try:
                        da.DescargarXMLPMC(id_doc, ruta_papers)
                    except:
                        listado_doc_not.append(id_doc)

        with open(f"{ruta}/Articulos/ArticulosSinDescargar.json", 'w') as f:
            json.dump(listado_doc_not, f)

    if opcion == "ExtraccionParrafosArticulos":
        ruta_articulos = f"{ruta}Articulos/"
        carpeta_abs = os.listdir(ruta_articulos)

        listado_articulos = []
        for i in carpeta_abs:
            if i != "ArticulosSinDescargar.json":
                carpeta_complejo = os.listdir(f"{ruta_articulos}{i}")
                listado_articulos = listado_articulos + \
                    [f"{ruta_articulos}{i}/{j}" for j in carpeta_complejo if j not in listado_articulos]

        parrafos = []

        for i in tqdm(range(len(listado_articulos))):
            ruta_articulo = listado_articulos[i]
            parrafos = parrafos + da.FormatoParrafoJSON(ruta_articulo)

        df = pd.DataFrame(data=parrafos)
        df.to_csv(
            f"{ruta}ParrafosArticulosComplejos.csv", index=False)

        # ruta_articulo = listado_articulos[18]
        # print(ruta_articulo)
        # da.FormatoParrafoJSON(ruta_articulo)

    if opcion == "ClasificacionModelo":
        ruta = "Documentos/DatosProfesora"
        ruta_corpus = f"{ruta}/ParrafosArticulosComplejos.csv"
        corpus = pd.read_csv(ruta_corpus)
        # print(corpus)

        rango1 = 50000
        rango2 = 100000
        X = np.asarray(corpus["Texto"][rango1:rango2])
        X = X.ravel()
        print(len(X))

        modelo = "AImed_NB_LEXICON_Model"
        info_modelo = modelo.split("_")
        ruta_resultados = f"Resultados/{info_modelo[1]}/Modelo"
        ruta_modelo = f"{ruta_resultados}/{modelo}.joblib"

        modelo = joblib.load(ruta_modelo)
        y_pred = modelo.predict(X)

        with open(f"{ruta}/ResultadoClasificacionTexto{rango1}_{rango2}.json", 'w') as f:
            json.dump(y_pred.tolist(), f)
            print("Clasificación Terminada...")

    if opcion == "BusquedaProteinas":
        ruta = "Documentos/DatosProfesora"
        ruta_corpus = f"{ruta}/ParrafosArticulosComplejos.csv"
        ruta_resultados = f"{ruta}/ResultadoClasificacionTexto.json"

        # Complementos

        jnlpba = spacy.load('en_ner_jnlpba_md')
        with open('Documentos/PatronProteinasSwissProt.txt') as f:
            patron_swiss = f.readlines()

        patron = patron_swiss[0]

        ####

        corpus = pd.read_csv(ruta_corpus)
        corpus = corpus.values.tolist()

        with open(ruta_resultados, 'r') as f:
            # Load the JSON data from the file
            resultados = json.load(f)

        resultados_index = [index for index,
                            res in enumerate(resultados) if res == 1]
        
        extraccion_info = []

        with open(f"{ruta}/BusquedaProteinas.txt", 'w') as f:

            for i in tqdm(range(len(resultados_index[0:20]))):
                index = resultados_index[i]
                info_corpus = corpus[index]
                abstract = info_corpus[3]

                complejo = info_corpus[0]
                complejo_id = str(complejo.split("_")[0]).upper()
                id_pmc = info_corpus[1]
                f.writelines("_________________\n")

                f.writelines(f"complejo :{complejo}\n")
                f.writelines(f"complejo id :{complejo_id}\n")
                f.writelines(f"id_pmc :{id_pmc}\n")
                f.writelines(f"abstract :{abstract}\n")

                doc = jnlpba(abstract)

                proteinas_docs = []

                proteinas_spacy = bp.BusquedaProteinasSpacy(abstract, doc)
                proteinas_patron = bp.BusquedaProteinasPatrones(
                    abstract, patron)
                proteinas_corpus = bp.BuscarProteinasCorpus(abstract)

                proteinas_complex =  bp.BuscarComplejosCorpus(abstract)

                for j in proteinas_spacy:
                    if j not in proteinas_docs:
                        if j in proteinas_patron or j in proteinas_corpus:
                            proteinas_docs.append(j)
                for j in proteinas_patron:
                    if j not in proteinas_docs:
                        if j in proteinas_spacy or j in proteinas_corpus:
                            proteinas_docs.append(j)
                for j in proteinas_corpus:
                    if j not in proteinas_docs:
                        if j in proteinas_spacy or j in proteinas_patron:
                            proteinas_docs.append(j)

                f.writelines(f"largo texto :{len(abstract)}\n")
                f.writelines("____\n")
                f.writelines(f"proteinas_spacy :{proteinas_spacy}\n",)
                f.writelines(f"proteinas_patron :{proteinas_patron}\n")
                f.writelines(f"proteinas_corpus :{proteinas_corpus}\n")
                f.writelines(f"proteinas_complex :{proteinas_complex}\n")

                f.writelines("____\n")
                f.writelines(f"proteinas validadas :{proteinas_docs}\n")

                proteinas = []
                for  j in proteinas_docs:
                    indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
                    proteinas_aparicion = [{"Proteina": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
                    info_proteina = {
                        "Proteina": j,
                        "ListadoIndex": proteinas_aparicion
                    }
                    proteinas.append(info_proteina)
                # print("proteina :", proteinas)

                validar = False
                for j in proteinas_docs:
                    pdb_id = bp.GetPDBId(nombre=j)
                    if pdb_id != None:
                        f.writelines(f"PDB :{pdb_id}\n")
                        if complejo_id == pdb_id:
                            validar == True

                f.writelines(f"complejos encontrados :{validar}\n")

                f.writelines("____\n")
                palabras_docs = fl.ValidarPalabrasClaves(doc)[1]
                f.writelines(f"palabras positivas :{palabras_docs}\n")

                palabras_claves = []
                for  j in palabras_docs:
                    indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
                    palabra = [{"Palabra": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
                    info_palabra = {
                        "Palabra": j,
                        "ListadoIndex": palabra
                    }
                    palabras_claves.append(info_palabra)

                f.writelines("_________________\n")

                if len(proteinas) >=2:
                    data = {
                        "Complejo": complejo,
                        "PDBId":  complejo_id,
                        "PMCId": id_pmc,
                        "Abstract": abstract,
                        "ProteinasSpacy":proteinas_spacy,
                        "ProteinasPatron":proteinas_patron,
                        "ProteinasCorpus":proteinas_corpus,
                        "ProteinasComplejos":proteinas_complex,
                        "ProteinasValidadas":proteinas_docs,
                        "ProteinasUbicacion": proteinas,
                        "ComplejosEncontrados":validar,
                        "PalabrasPositivas":palabras_docs,
                        "PalabrasUbicacion":palabras_claves
                    }
                    
                    extraccion_info.append(data)
        with open(f"{ruta}/BusquedaProteinas.json", 'w') as f:
            json.dump(extraccion_info, f)


    if opcion == "AnalisisInfo":
        ruta = "Documentos/DatosProfesora"

        with open(f"{ruta}/BusquedaProteinas.json") as f:
            data = json.load(f)
    
        abstract = data[0]["Abstract"]
        abstract_etiquetas = abstract

        proteinas = []

        for proteina in data[0]["ProteinasUbicacion"]:
            listado_indexs = proteina["ListadoIndex"]
            prot_pattern = proteina["Proteina"]
            abstract_etiquetas = str(abstract_etiquetas).replace(f"{prot_pattern}", f"<proteina>{prot_pattern}</proteina>")

            for index in listado_indexs:
                proteinas.append(index)

        ubicaciones = []

        for prot in proteinas:
            for prot2 in proteinas:
                if prot2["Proteina"].lower()  ==  prot["Proteina"].lower() and  prot2["Inicio"]  ==  prot["Inicio"] and  prot2["Proteina"].lower()  in  prot["Proteina"].lower() :
                    continue
                else:
                    if prot["Inicio"] <  prot2["Inicio"]:
                        ubicaciones.append((prot["Inicio"], prot2["Termino"]))
                    elif prot["Inicio"] >  prot2["Inicio"] :
                        # print("__")
                        # print(prot)
                        # print(prot2)
                        # print("__")
                        ubicaciones.append((prot2["Inicio"], prot["Termino"]))
                    # prot["Termino"]  
                    # print(prot)
        

        oracion_pares = []
        for index in ubicaciones:
            oracion_pares = abstract[index[0]:index[1] ]


        print(len(ubicaciones))
        print(abstract_etiquetas)

    if opcion == "DescargarAbstractsPubmedPDB":
        
        ruta = "Documentos/DatosProfesora/"

        listado_complejos = pd.read_csv(ruta + "ComplejosTransient.csv")
        listado_complejos = listado_complejos.values.tolist()

        listado_complejos = listado_complejos + pd.read_csv(ruta + "ComplejosObligate.csv").values.tolist()


        listado_doc_not = []

        for i in tqdm(range(len(listado_complejos))):
            id = listado_complejos[i][0]
            pdb_id = listado_complejos[i][1]
            nombre = listado_complejos[i][4]
            ruta_papers = f"{ruta}/PubmedAbstracts/{id}"

            if os.path.exists(ruta_papers) == False:
                os.mkdir(ruta_papers)

            listado_ids_papers = da.BusquedaCitasPubmedPDBId(pdb_id)

            for id_doc in listado_ids_papers:
                if os.path.exists(f"{ruta_papers}/{id_doc}") == False:
                    try:
                        da.DescargarXMLPubmed(id_doc, ruta_papers)
                    except:
                        listado_doc_not.append(id_doc)

        with open(f"{ruta}/PubmedAbstracts/ArticulosSinDescargar.json", 'w') as f:
            json.dump(listado_doc_not, f)

    if opcion == "ExtraccionAbstracts":
        ruta = "Documentos/DatosProfesora/"

        ruta_articulos = ruta + f"PubmedAbstracts/"
        carpeta_abs = os.listdir(ruta_articulos)

        listado_articulos = []
        for i in carpeta_abs:
            if i != "ArticulosSinDescargar.json":
                carpeta_complejo = os.listdir(f"{ruta_articulos}{i}")
                listado_articulos = listado_articulos + \
                    [f"{ruta_articulos}{i}/{j}" for j in carpeta_complejo if j not in listado_articulos]

        parrafos = []

        for i in tqdm(range(len(listado_articulos))):
            ruta_articulo = listado_articulos[i]
            abstract = da.ExtraccionAbstractsXML(ruta_articulo)

            id_complex, id_pubmed = str(ruta_articulo).replace(ruta_articulos,"").split("/")
            data = {
                "IdComplex": id_complex,
                "IdPubmed": id_pubmed.replace(".xml", ""),
                "Texto": abstract
            }

            parrafos.append(data)

        df = pd.DataFrame(data=parrafos)
        df.to_csv(
            f"{ruta}AbstractsArticulosComplejos.csv", index=False)





     


# def OrdenInicio(listado):






# termino = "CRYSTAL STRUCTURE OF A HETERODIMERIC COMPLEX OF RAR AND RXR LIGAND-BINDING DOMAINS"
# print(ListadoPMCIds(termino))
# termino = "1dkf"
# print(ListadoPMCIds(termino))
