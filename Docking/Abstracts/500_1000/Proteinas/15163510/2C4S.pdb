HEADER    TEXTURE OF CONNECTIVE TISSUE            23-MAY-78   2C4S              
TITLE     CALCIUM CHONDROITIN 4-SULFATE. MOLECULAR CONFORMATION AND ORGANIZATION
TITLE    2 OF POLYSACCHARIDE CHAINS IN A PROTEOGLYCAN                           
CAVEAT     2C4S    GCU A 1 HAS WRONG CHIRALITY AT ATOM C1 GCU A 3 HAS WRONG     
CAVEAT   2 2C4S    CHIRALITY AT ATOM C1                                         
COMPND    MOL_ID: 1;                                                            
COMPND   2 MOLECULE: 2-ACETAMIDO-2-DEOXY-4-O-SULFO-BETA-D-GALACTOPYRANOSE-(1-4)-
COMPND   3 ALPHA-D-GLUCOPYRANURONIC ACID-(1-3)-2-ACETAMIDO-2-DEOXY-4-O-SULFO-   
COMPND   4 BETA-D-GALACTOPYRANOSE-(1-4)-ALPHA-D-GLUCOPYRANURONIC ACID;          
COMPND   5 CHAIN: A;                                                            
COMPND   6 ENGINEERED: YES                                                      
SOURCE    MOL_ID: 1                                                             
KEYWDS    TEXTURE OF CONNECTIVE TISSUE                                          
EXPDTA    FIBER DIFFRACTION                                                     
AUTHOR    S.ARNOTT                                                              
REVDAT   7   29-JUL-20 2C4S    1       CAVEAT COMPND REMARK HETNAM              
REVDAT   7 2                   1       LINK   SITE   ATOM                       
REVDAT   6   24-FEB-09 2C4S    1       VERSN                                    
REVDAT   5   01-APR-03 2C4S    1       JRNL                                     
REVDAT   4   15-JUL-92 2C4S    1       FORMUL                                   
REVDAT   3   30-SEP-83 2C4S    1       REVDAT                                   
REVDAT   2   01-OCT-80 2C4S    1       REMARK                                   
REVDAT   1   28-MAR-80 2C4S    0                                                
JRNL        AUTH   J.J.CAEL,W.T.WINTER,S.ARNOTT                                 
JRNL        TITL   CALCIUM CHONDROITIN 4-SULFATE: MOLECULAR CONFORMATION AND    
JRNL        TITL 2 ORGANIZATION OF POLYSACCHARIDE CHAINS IN A PROTEOGLYCAN.     
JRNL        REF    J.MOL.BIOL.                   V. 125    21 1978              
JRNL        REFN                   ISSN 0022-2836                               
JRNL        PMID   712856                                                       
JRNL        DOI    10.1016/0022-2836(78)90252-8                                 
REMARK   1                                                                      
REMARK   1 REFERENCE 1                                                          
REMARK   1  AUTH   P.J.C.SMITH,S.ARNOTT                                         
REMARK   1  TITL   LALS, A LINKED-ATOM LEAST-SQUARES RECIPROCAL-SPACE           
REMARK   1  TITL 2 REFINEMENT SYSTEM INCORPORATING STEREOCHEMICAL RESTRAINTS TO 
REMARK   1  TITL 3 SUPPLEMENT SPARSE DIFFRACTION DATA                           
REMARK   1  REF    ACTA CRYSTALLOGR.,SECT.A      V.  34     3 1978              
REMARK   1  REFN                   ISSN 0108-7673                               
REMARK   2                                                                      
REMARK   2 RESOLUTION.    3.00 ANGSTROMS.                                       
REMARK   3                                                                      
REMARK   3 REFINEMENT.                                                          
REMARK   3   PROGRAM     : LINKED-ATOM LEAST-SQUARES MODEL-BUILDING PROCEDURE   
REMARK   3   AUTHORS     : NULL                                                 
REMARK   3                                                                      
REMARK   3  DATA USED IN REFINEMENT.                                            
REMARK   3   RESOLUTION RANGE HIGH (ANGSTROMS) : 3.00                           
REMARK   3   RESOLUTION RANGE LOW  (ANGSTROMS) : NULL                           
REMARK   3   DATA CUTOFF            (SIGMA(F)) : NULL                           
REMARK   3   DATA CUTOFF HIGH         (ABS(F)) : NULL                           
REMARK   3   DATA CUTOFF LOW          (ABS(F)) : NULL                           
REMARK   3   COMPLETENESS (WORKING+TEST)   (%) : NULL                           
REMARK   3   NUMBER OF REFLECTIONS             : NULL                           
REMARK   3                                                                      
REMARK   3  FIT TO DATA USED IN REFINEMENT.                                     
REMARK   3   CROSS-VALIDATION METHOD          : NULL                            
REMARK   3   FREE R VALUE TEST SET SELECTION  : NULL                            
REMARK   3   R VALUE            (WORKING SET) : NULL                            
REMARK   3   FREE R VALUE                     : NULL                            
REMARK   3   FREE R VALUE TEST SET SIZE   (%) : NULL                            
REMARK   3   FREE R VALUE TEST SET COUNT      : NULL                            
REMARK   3   ESTIMATED ERROR OF FREE R VALUE  : NULL                            
REMARK   3                                                                      
REMARK   3  FIT IN THE HIGHEST RESOLUTION BIN.                                  
REMARK   3   TOTAL NUMBER OF BINS USED           : NULL                         
REMARK   3   BIN RESOLUTION RANGE HIGH       (A) : NULL                         
REMARK   3   BIN RESOLUTION RANGE LOW        (A) : NULL                         
REMARK   3   BIN COMPLETENESS (WORKING+TEST) (%) : NULL                         
REMARK   3   REFLECTIONS IN BIN    (WORKING SET) : NULL                         
REMARK   3   BIN R VALUE           (WORKING SET) : NULL                         
REMARK   3   BIN FREE R VALUE                    : NULL                         
REMARK   3   BIN FREE R VALUE TEST SET SIZE  (%) : NULL                         
REMARK   3   BIN FREE R VALUE TEST SET COUNT     : NULL                         
REMARK   3   ESTIMATED ERROR OF BIN FREE R VALUE : NULL                         
REMARK   3                                                                      
REMARK   3  NUMBER OF NON-HYDROGEN ATOMS USED IN REFINEMENT.                    
REMARK   3   PROTEIN ATOMS            : 0                                       
REMARK   3   NUCLEIC ACID ATOMS       : 0                                       
REMARK   3   HETEROGEN ATOMS          : 62                                      
REMARK   3   SOLVENT ATOMS            : 14                                      
REMARK   3                                                                      
REMARK   3  B VALUES.                                                           
REMARK   3   FROM WILSON PLOT           (A**2) : NULL                           
REMARK   3   MEAN B VALUE      (OVERALL, A**2) : NULL                           
REMARK   3   OVERALL ANISOTROPIC B VALUE.                                       
REMARK   3    B11 (A**2) : NULL                                                 
REMARK   3    B22 (A**2) : NULL                                                 
REMARK   3    B33 (A**2) : NULL                                                 
REMARK   3    B12 (A**2) : NULL                                                 
REMARK   3    B13 (A**2) : NULL                                                 
REMARK   3    B23 (A**2) : NULL                                                 
REMARK   3                                                                      
REMARK   3  ESTIMATED COORDINATE ERROR.                                         
REMARK   3   ESD FROM LUZZATI PLOT        (A) : NULL                            
REMARK   3   ESD FROM SIGMAA              (A) : NULL                            
REMARK   3   LOW RESOLUTION CUTOFF        (A) : NULL                            
REMARK   3                                                                      
REMARK   3  CROSS-VALIDATED ESTIMATED COORDINATE ERROR.                         
REMARK   3   ESD FROM C-V LUZZATI PLOT    (A) : NULL                            
REMARK   3   ESD FROM C-V SIGMAA          (A) : NULL                            
REMARK   3                                                                      
REMARK   3  RMS DEVIATIONS FROM IDEAL VALUES.                                   
REMARK   3   BOND LENGTHS                 (A) : NULL                            
REMARK   3   BOND ANGLES            (DEGREES) : NULL                            
REMARK   3   DIHEDRAL ANGLES        (DEGREES) : NULL                            
REMARK   3   IMPROPER ANGLES        (DEGREES) : NULL                            
REMARK   3                                                                      
REMARK   3  ISOTROPIC THERMAL MODEL : NULL                                      
REMARK   3                                                                      
REMARK   3  ISOTROPIC THERMAL FACTOR RESTRAINTS.    RMS    SIGMA                
REMARK   3   MAIN-CHAIN BOND              (A**2) : NULL  ; NULL                 
REMARK   3   MAIN-CHAIN ANGLE             (A**2) : NULL  ; NULL                 
REMARK   3   SIDE-CHAIN BOND              (A**2) : NULL  ; NULL                 
REMARK   3   SIDE-CHAIN ANGLE             (A**2) : NULL  ; NULL                 
REMARK   3                                                                      
REMARK   3  NCS MODEL : NULL                                                    
REMARK   3                                                                      
REMARK   3  NCS RESTRAINTS.                         RMS   SIGMA/WEIGHT          
REMARK   3   GROUP  1  POSITIONAL            (A) : NULL  ; NULL                 
REMARK   3   GROUP  1  B-FACTOR           (A**2) : NULL  ; NULL                 
REMARK   3                                                                      
REMARK   3  PARAMETER FILE  1  : NULL                                           
REMARK   3  TOPOLOGY FILE  1   : NULL                                           
REMARK   3                                                                      
REMARK   3  OTHER REFINEMENT REMARKS:                                           
REMARK   3  THE SPACE GROUP CHOSEN FOR THIS STUDY WAS P 2 21 21 WHICH           
REMARK   3  IS A NON-STANDARD REPRESENTATION OF P 21 21 2.  IN THIS             
REMARK   3  FORMER GROUP THE EQUIPOINTS ARE X,Y,Z  X,-Y,-Z  -X,1/2-Y,           
REMARK   3  1/2+Z AND -X,1/2+Y,1/2-Z.                                           
REMARK   4                                                                      
REMARK   4 2C4S COMPLIES WITH FORMAT V. 3.30, 13-JUL-11                         
REMARK 100                                                                      
REMARK 100 THIS ENTRY HAS BEEN PROCESSED BY BNL.                                
REMARK 100 THE DEPOSITION ID IS D_1000177883.                                   
REMARK 200                                                                      
REMARK 200 EXPERIMENTAL DETAILS                                                 
REMARK 200  EXPERIMENT TYPE                : FIBER DIFFRACTION                  
REMARK 200  DATE OF DATA COLLECTION        : NULL                               
REMARK 200  TEMPERATURE           (KELVIN) : NULL                               
REMARK 200  PH                             : NULL                               
REMARK 200  NUMBER OF CRYSTALS USED        : NULL                               
REMARK 200                                                                      
REMARK 200  SYNCHROTRON              (Y/N) : NULL                               
REMARK 200  RADIATION SOURCE               : NULL                               
REMARK 200  BEAMLINE                       : NULL                               
REMARK 200  X-RAY GENERATOR MODEL          : NULL                               
REMARK 200  MONOCHROMATIC OR LAUE    (M/L) : NULL                               
REMARK 200  WAVELENGTH OR RANGE        (A) : NULL                               
REMARK 200  MONOCHROMATOR                  : NULL                               
REMARK 200  OPTICS                         : NULL                               
REMARK 200                                                                      
REMARK 200  DETECTOR TYPE                  : NULL                               
REMARK 200  DETECTOR MANUFACTURER          : NULL                               
REMARK 200  INTENSITY-INTEGRATION SOFTWARE : NULL                               
REMARK 200  DATA SCALING SOFTWARE          : NULL                               
REMARK 200                                                                      
REMARK 200  NUMBER OF UNIQUE REFLECTIONS   : NULL                               
REMARK 200  RESOLUTION RANGE HIGH      (A) : NULL                               
REMARK 200  RESOLUTION RANGE LOW       (A) : NULL                               
REMARK 200  REJECTION CRITERIA  (SIGMA(I)) : NULL                               
REMARK 200                                                                      
REMARK 200 OVERALL.                                                             
REMARK 200  COMPLETENESS FOR RANGE     (%) : NULL                               
REMARK 200  DATA REDUNDANCY                : NULL                               
REMARK 200  R MERGE                    (I) : NULL                               
REMARK 200  R SYM                      (I) : NULL                               
REMARK 200  <I/SIGMA(I)> FOR THE DATA SET  : NULL                               
REMARK 200                                                                      
REMARK 200 IN THE HIGHEST RESOLUTION SHELL.                                     
REMARK 200  HIGHEST RESOLUTION SHELL, RANGE HIGH (A) : NULL                     
REMARK 200  HIGHEST RESOLUTION SHELL, RANGE LOW  (A) : NULL                     
REMARK 200  COMPLETENESS FOR SHELL     (%) : NULL                               
REMARK 200  DATA REDUNDANCY IN SHELL       : NULL                               
REMARK 200  R MERGE FOR SHELL          (I) : NULL                               
REMARK 200  R SYM FOR SHELL            (I) : NULL                               
REMARK 200  <I/SIGMA(I)> FOR SHELL         : NULL                               
REMARK 200                                                                      
REMARK 200 DIFFRACTION PROTOCOL: NULL                                           
REMARK 200 METHOD USED TO DETERMINE THE STRUCTURE: NULL                         
REMARK 200 SOFTWARE USED: NULL                                                  
REMARK 200 STARTING MODEL: NULL                                                 
REMARK 200                                                                      
REMARK 200 REMARK: NULL                                                         
REMARK 205                                                                      
REMARK 205 FIBER DIFFRACTION                                                    
REMARK 205 THE COORDINATES IN THIS ENTRY WERE GENERATED FROM FIBER              
REMARK 205 DIFFRACTION DATA.  PROTEIN DATA BANK CONVENTIONS REQUIRE             
REMARK 205 THAT CRYST1 AND SCALE RECORDS BE INCLUDED, BUT THE                   
REMARK 205 VALUES ON THESE RECORDS ARE MEANINGLESS.                             
REMARK 300                                                                      
REMARK 300 BIOMOLECULE: 1                                                       
REMARK 300 SEE REMARK 350 FOR THE AUTHOR PROVIDED AND/OR PROGRAM                
REMARK 300 GENERATED ASSEMBLY INFORMATION FOR THE STRUCTURE IN                  
REMARK 300 THIS ENTRY. THE REMARK MAY ALSO PROVIDE INFORMATION ON               
REMARK 300 BURIED SURFACE AREA.                                                 
REMARK 300 REMARK: THE FOUR-RESIDUE CHAIN SEGMENT GIVEN HERE WAS OBTAINED FROM  
REMARK 300 THE PUBLISHED COORDINATES FOR A TWO-RESIDUE FRAGMENT BY THE          
REMARK 300 ACTION OF A 21 SCREW AXIS AS DEFINED IN THE PAPER CITED IN           
REMARK 300 THE JRNL RECORDS ABOVE.                                              
REMARK 350                                                                      
REMARK 350 COORDINATES FOR A COMPLETE MULTIMER REPRESENTING THE KNOWN           
REMARK 350 BIOLOGICALLY SIGNIFICANT OLIGOMERIZATION STATE OF THE                
REMARK 350 MOLECULE CAN BE GENERATED BY APPLYING BIOMT TRANSFORMATIONS          
REMARK 350 GIVEN BELOW.  BOTH NON-CRYSTALLOGRAPHIC AND                          
REMARK 350 CRYSTALLOGRAPHIC OPERATIONS ARE GIVEN.                               
REMARK 350                                                                      
REMARK 350 BIOMOLECULE: 1                                                       
REMARK 350 AUTHOR DETERMINED BIOLOGICAL UNIT: MONOMERIC                         
REMARK 350 APPLY THE FOLLOWING TO CHAINS: A                                     
REMARK 350   BIOMT1   1  1.000000  0.000000  0.000000        0.00000            
REMARK 350   BIOMT2   1  0.000000  1.000000  0.000000        0.00000            
REMARK 350   BIOMT3   1  0.000000  0.000000  1.000000        0.00000            
REMARK 610                                                                      
REMARK 610 MISSING HETEROATOM                                                   
REMARK 610 THE FOLLOWING RESIDUES HAVE MISSING ATOMS (M=MODEL NUMBER;           
REMARK 610 RES=RESIDUE NAME; C=CHAIN IDENTIFIER; SSEQ=SEQUENCE NUMBER;          
REMARK 610 I=INSERTION CODE):                                                   
REMARK 610   M RES C SSEQI                                                      
REMARK 610     ASG A    4                                                       
REMARK 620                                                                      
REMARK 620 METAL COORDINATION                                                   
REMARK 620 (M=MODEL NUMBER; RES=RESIDUE NAME; C=CHAIN IDENTIFIER;               
REMARK 620 SSEQ=SEQUENCE NUMBER; I=INSERTION CODE):                             
REMARK 620                                                                      
REMARK 620 COORDINATION ANGLES FOR:  M RES CSSEQI METAL                         
REMARK 620                              CA A   5  CA                            
REMARK 620 N RES CSSEQI ATOM                                                    
REMARK 620 1 GCU A   1   O6B                                                    
REMARK 620 2 HOH A  10   O   102.6                                              
REMARK 620 3 HOH A  11   O   118.4 118.5                                        
REMARK 620 4 HOH A  12   O    88.2  65.0 149.1                                  
REMARK 620 5 HOH A  14   O    58.8 149.9  63.2 130.9                            
REMARK 620 N                    1     2     3     4                             
REMARK 620                                                                      
REMARK 620 COORDINATION ANGLES FOR:  M RES CSSEQI METAL                         
REMARK 620                              CA A   6  CA                            
REMARK 620 N RES CSSEQI ATOM                                                    
REMARK 620 1 ASG A   2   OSB                                                    
REMARK 620 2 GCU A   3   O6B  71.0                                              
REMARK 620 3 HOH A  17   O    90.2 102.6                                        
REMARK 620 4 HOH A  18   O    65.6 118.4 118.5                                  
REMARK 620 5 HOH A  19   O   143.6  88.2  65.0 149.1                            
REMARK 620 6 HOH A  21   O    62.4  58.8 149.9  63.2 130.9                      
REMARK 620 N                    1     2     3     4     5                       
HET    GCU  A   1      18                                                       
HET    ASG  A   2      29                                                       
HET    GCU  A   3      17                                                       
HET    ASG  A   4      28                                                       
HET     CA  A   5       1                                                       
HET     CA  A   6       1                                                       
HETNAM     GCU ALPHA-D-GLUCOPYRANURONIC ACID                                    
HETNAM     ASG 2-ACETAMIDO-2-DEOXY-4-O-SULFO-BETA-D-GALACTOPYRANOSE             
HETNAM      CA CALCIUM ION                                                      
FORMUL   1  GCU    2(C6 H10 O7)                                                 
FORMUL   1  ASG    2(C8 H15 N O9 S)                                             
FORMUL   2   CA    2(CA 2+)                                                     
FORMUL   4  HOH   *14(H2 O)                                                     
LINK         O4  GCU A   1                 C1  ASG A   2     1555   1555  1.39  
LINK         O3  ASG A   2                 C1  GCU A   3     1555   1555  1.39  
LINK         O4  GCU A   3                 C1  ASG A   4     1555   1555  1.39  
LINK         O6B GCU A   1                CA    CA A   5     1555   1555  2.64  
LINK         OSB ASG A   2                CA    CA A   6     1555   1555  2.55  
LINK         O6B GCU A   3                CA    CA A   6     1555   1555  2.64  
LINK        CA    CA A   5                 O   HOH A  10     1555   1555  2.48  
LINK        CA    CA A   5                 O   HOH A  11     1555   1555  2.50  
LINK        CA    CA A   5                 O   HOH A  12     1555   1555  2.48  
LINK        CA    CA A   5                 O   HOH A  14     1555   1555  2.61  
LINK        CA    CA A   6                 O   HOH A  17     1555   1555  2.48  
LINK        CA    CA A   6                 O   HOH A  18     1555   1555  2.50  
LINK        CA    CA A   6                 O   HOH A  19     1555   1555  2.48  
LINK        CA    CA A   6                 O   HOH A  21     1555   1555  2.61  
CRYST1    7.450   17.810   19.640  90.00  90.00  90.00 P 2 21 21     4          
ORIGX1      0.100000  0.000000  0.000000        0.00000                         
ORIGX2      0.000000  0.100000  0.000000        0.00000                         
ORIGX3      0.000000  0.000000  0.100000        0.00000                         
SCALE1      0.134228  0.000000  0.000000        0.00000                         
SCALE2      0.000000  0.056148  0.000000        0.00000                         
SCALE3      0.000000  0.000000  0.050916        0.00000                         
HETATM    1  C1  GCU A   1       0.336  -0.126   1.685  1.00  0.00           C  
HETATM    2  C2  GCU A   1       0.720  -1.262   2.623  1.00  0.00           C  
HETATM    3  C3  GCU A   1      -0.068  -1.173   3.920  1.00  0.00           C  
HETATM    4  C4  GCU A   1       0.082   0.212   4.536  1.00  0.00           C  
HETATM    5  C5  GCU A   1      -0.256   1.285   3.506  1.00  0.00           C  
HETATM    6  C6  GCU A   1      -0.016   2.687   4.024  1.00  0.00           C  
HETATM    7  O1  GCU A   1       1.171  -0.171   0.576  1.00  0.00           O  
HETATM    8  O2  GCU A   1       0.488  -2.507   1.974  1.00  0.00           O  
HETATM    9  O3  GCU A   1       0.404  -2.162   4.837  1.00  0.00           O  
HETATM   10  O4  GCU A   1      -0.795   0.358   5.651  1.00  0.00           O  
HETATM   11  O5  GCU A   1       0.563   1.125   2.338  1.00  0.00           O  
HETATM   12  O6A GCU A   1      -0.986   3.205   4.616  1.00  0.00           O  
HETATM   13  O6B GCU A   1       1.123   3.148   3.791  1.00  0.00           O  
HETATM   14  H1  GCU A   1      -0.728  -0.214   1.419  1.00  0.00           H  
HETATM   15  H2  GCU A   1       1.797  -1.208   2.843  1.00  0.00           H  
HETATM   16  H3  GCU A   1      -1.131  -1.371   3.718  1.00  0.00           H  
HETATM   17  H4  GCU A   1       1.117   0.349   4.883  1.00  0.00           H  
HETATM   18  H5  GCU A   1      -1.317   1.203   3.226  1.00  0.00           H  
HETATM   19  C1  ASG A   2      -0.540  -0.451   6.750  1.00  0.00           C  
HETATM   20  C2  ASG A   2      -0.861   0.338   8.013  1.00  0.00           C  
HETATM   21  C3  ASG A   2      -0.736  -0.548   9.242  1.00  0.00           C  
HETATM   22  C4  ASG A   2      -1.577  -1.807   9.074  1.00  0.00           C  
HETATM   23  C5  ASG A   2      -1.239  -2.492   7.754  1.00  0.00           C  
HETATM   24  C6  ASG A   2      -2.123  -3.688   7.474  1.00  0.00           C  
HETATM   25  C7  ASG A   2      -0.426   2.682   8.636  1.00  0.00           C  
HETATM   26  C8  ASG A   2       0.610   3.780   8.672  1.00  0.00           C  
HETATM   27  N2  ASG A   2       0.052   1.500   8.108  1.00  0.00           N  
HETATM   28  O3  ASG A   2      -1.171   0.171  10.398  1.00  0.00           O  
HETATM   29  O4  ASG A   2      -2.965  -1.482   9.088  1.00  0.00           O  
HETATM   30  O5  ASG A   2      -1.416  -1.577   6.662  1.00  0.00           O  
HETATM   31  O6  ASG A   2      -1.422  -4.712   6.770  1.00  0.00           O  
HETATM   32  O7  ASG A   2      -1.570   2.828   9.034  1.00  0.00           O  
HETATM   33  OSA ASG A   2      -4.764  -1.220  10.792  1.00  0.00           O  
HETATM   34  OSB ASG A   2      -2.962  -2.677  11.275  1.00  0.00           O  
HETATM   35  OSC ASG A   2      -4.581  -3.292   9.661  1.00  0.00           O  
HETATM   36  S   ASG A   2      -3.838  -2.184  10.230  1.00  0.00           S  
HETATM   37  H1  ASG A   2       0.502  -0.800   6.789  1.00  0.00           H  
HETATM   38  H2  ASG A   2      -1.885   0.735   7.949  1.00  0.00           H  
HETATM   39  H3  ASG A   2       0.318  -0.828   9.388  1.00  0.00           H  
HETATM   40  H4  ASG A   2      -1.370  -2.500   9.902  1.00  0.00           H  
HETATM   41  H5  ASG A   2      -0.196  -2.840   7.777  1.00  0.00           H  
HETATM   42  H61 ASG A   2      -2.485  -4.108   8.424  1.00  0.00           H  
HETATM   43  H62 ASG A   2      -2.982  -3.375   6.862  1.00  0.00           H  
HETATM   44  H81 ASG A   2       1.494   3.432   9.226  1.00  0.00           H  
HETATM   45  H82 ASG A   2       0.903   4.042   7.644  1.00  0.00           H  
HETATM   46  H83 ASG A   2       0.190   4.665   9.171  1.00  0.00           H  
HETATM   47  HN2 ASG A   2       0.950   1.424   7.808  1.00  0.00           H  
HETATM   48  C1  GCU A   3      -0.336   0.126  11.505  1.00  0.00           C  
HETATM   49  C2  GCU A   3      -0.720   1.262  12.443  1.00  0.00           C  
HETATM   50  C3  GCU A   3       0.068   1.173  13.740  1.00  0.00           C  
HETATM   51  C4  GCU A   3      -0.082  -0.212  14.356  1.00  0.00           C  
HETATM   52  C5  GCU A   3       0.256  -1.285  13.326  1.00  0.00           C  
HETATM   53  C6  GCU A   3       0.016  -2.687  13.844  1.00  0.00           C  
HETATM   54  O2  GCU A   3      -0.488   2.507  11.794  1.00  0.00           O  
HETATM   55  O3  GCU A   3      -0.404   2.162  14.657  1.00  0.00           O  
HETATM   56  O4  GCU A   3       0.795  -0.358  15.471  1.00  0.00           O  
HETATM   57  O5  GCU A   3      -0.563  -1.125  12.158  1.00  0.00           O  
HETATM   58  O6A GCU A   3       0.986  -3.205  14.436  1.00  0.00           O  
HETATM   59  O6B GCU A   3      -1.123  -3.148  13.611  1.00  0.00           O  
HETATM   60  H1  GCU A   3       0.728   0.214  11.239  1.00  0.00           H  
HETATM   61  H2  GCU A   3      -1.797   1.208  12.663  1.00  0.00           H  
HETATM   62  H3  GCU A   3       1.131   1.371  13.538  1.00  0.00           H  
HETATM   63  H4  GCU A   3      -1.117  -0.349  14.703  1.00  0.00           H  
HETATM   64  H5  GCU A   3       1.317  -1.203  13.046  1.00  0.00           H  
HETATM   65  C1  ASG A   4       0.540   0.451  16.570  1.00  0.00           C  
HETATM   66  C2  ASG A   4       0.861  -0.338  17.833  1.00  0.00           C  
HETATM   67  C3  ASG A   4       0.736   0.548  19.062  1.00  0.00           C  
HETATM   68  C4  ASG A   4       1.577   1.807  18.894  1.00  0.00           C  
HETATM   69  C5  ASG A   4       1.239   2.492  17.574  1.00  0.00           C  
HETATM   70  C6  ASG A   4       2.123   3.688  17.294  1.00  0.00           C  
HETATM   71  C7  ASG A   4       0.426  -2.682  18.456  1.00  0.00           C  
HETATM   72  C8  ASG A   4      -0.610  -3.780  18.492  1.00  0.00           C  
HETATM   73  N2  ASG A   4      -0.052  -1.500  17.928  1.00  0.00           N  
HETATM   74  O4  ASG A   4       2.965   1.482  18.908  1.00  0.00           O  
HETATM   75  O5  ASG A   4       1.416   1.577  16.482  1.00  0.00           O  
HETATM   76  O6  ASG A   4       1.422   4.712  16.590  1.00  0.00           O  
HETATM   77  O7  ASG A   4       1.570  -2.828  18.854  1.00  0.00           O  
HETATM   78  OSA ASG A   4       4.764   1.220  20.612  1.00  0.00           O  
HETATM   79  OSB ASG A   4       2.962   2.677  21.095  1.00  0.00           O  
HETATM   80  OSC ASG A   4       4.581   3.292  19.481  1.00  0.00           O  
HETATM   81  S   ASG A   4       3.838   2.184  20.050  1.00  0.00           S  
HETATM   82  H1  ASG A   4      -0.502   0.800  16.609  1.00  0.00           H  
HETATM   83  H2  ASG A   4       1.885  -0.735  17.769  1.00  0.00           H  
HETATM   84  H3  ASG A   4      -0.318   0.828  19.208  1.00  0.00           H  
HETATM   85  H4  ASG A   4       1.370   2.500  19.722  1.00  0.00           H  
HETATM   86  H5  ASG A   4       0.196   2.840  17.597  1.00  0.00           H  
HETATM   87  H61 ASG A   4       2.485   4.108  18.244  1.00  0.00           H  
HETATM   88  H62 ASG A   4       2.982   3.375  16.682  1.00  0.00           H  
HETATM   89  H81 ASG A   4      -1.494  -3.432  19.046  1.00  0.00           H  
HETATM   90  H82 ASG A   4      -0.903  -4.042  17.464  1.00  0.00           H  
HETATM   91  H83 ASG A   4      -0.190  -4.665  18.991  1.00  0.00           H  
HETATM   92  HN2 ASG A   4      -0.950  -1.424  17.628  1.00  0.00           H  
HETATM   93 CA    CA A   5       3.712   2.645   3.887  1.00  0.00          CA  
HETATM   94 CA    CA A   6      -3.712  -2.645  13.707  1.00  0.00          CA  
HETATM   95  O   HOH A  10       3.782   0.162   3.840  1.00  0.00           O  
HETATM   96  O   HOH A  11       5.227   3.910   2.355  1.00  0.00           O  
HETATM   97  O   HOH A  12       3.338   1.545   6.075  1.00  0.00           O  
HETATM   98  O   HOH A  13       4.274   4.454   4.411  0.50  0.00           O  
HETATM   99  O   HOH A  14       2.808   4.895   2.931  1.00  0.00           O  
HETATM  100  O   HOH A  15       2.695   7.972   0.857  1.00  0.00           O  
HETATM  101  O   HOH A  16       3.773  -2.524   0.930  1.00  0.00           O  
HETATM  102  O   HOH A  17      -3.782  -0.162  13.660  1.00  0.00           O  
HETATM  103  O   HOH A  18      -5.227  -3.910  12.175  1.00  0.00           O  
HETATM  104  O   HOH A  19      -3.338  -1.545  15.895  1.00  0.00           O  
HETATM  105  O   HOH A  20      -4.274  -4.454  14.231  0.50  0.00           O  
HETATM  106  O   HOH A  21      -2.808  -4.895  12.751  1.00  0.00           O  
HETATM  107  O   HOH A  22      -2.695  -7.972  10.677  1.00  0.00           O  
HETATM  108  O   HOH A  23      -3.773   2.524  10.750  1.00  0.00           O  
CONECT    1    2    7   11   14                                                 
CONECT    2    1    3    8   15                                                 
CONECT    3    2    4    9   16                                                 
CONECT    4    3    5   10   17                                                 
CONECT    5    4    6   11   18                                                 
CONECT    6    5   12   13                                                      
CONECT    7    1                                                                
CONECT    8    2                                                                
CONECT    9    3                                                                
CONECT   10    4   19                                                           
CONECT   11    1    5                                                           
CONECT   12    6                                                                
CONECT   13    6   93                                                           
CONECT   14    1                                                                
CONECT   15    2                                                                
CONECT   16    3                                                                
CONECT   17    4                                                                
CONECT   18    5                                                                
CONECT   19   10   20   30   37                                                 
CONECT   20   19   21   27   38                                                 
CONECT   21   20   22   28   39                                                 
CONECT   22   21   23   29   40                                                 
CONECT   23   22   24   30   41                                                 
CONECT   24   23   31   42   43                                                 
CONECT   25   26   27   32                                                      
CONECT   26   25   44   45   46                                                 
CONECT   27   20   25   47                                                      
CONECT   28   21   48                                                           
CONECT   29   22   36                                                           
CONECT   30   19   23                                                           
CONECT   31   24                                                                
CONECT   32   25                                                                
CONECT   33   36                                                                
CONECT   34   36   94                                                           
CONECT   35   36                                                                
CONECT   36   29   33   34   35                                                 
CONECT   37   19                                                                
CONECT   38   20                                                                
CONECT   39   21                                                                
CONECT   40   22                                                                
CONECT   41   23                                                                
CONECT   42   24                                                                
CONECT   43   24                                                                
CONECT   44   26                                                                
CONECT   45   26                                                                
CONECT   46   26                                                                
CONECT   47   27                                                                
CONECT   48   28   49   57   60                                                 
CONECT   49   48   50   54   61                                                 
CONECT   50   49   51   55   62                                                 
CONECT   51   50   52   56   63                                                 
CONECT   52   51   53   57   64                                                 
CONECT   53   52   58   59                                                      
CONECT   54   49                                                                
CONECT   55   50                                                                
CONECT   56   51   65                                                           
CONECT   57   48   52                                                           
CONECT   58   53                                                                
CONECT   59   53   94                                                           
CONECT   60   48                                                                
CONECT   61   49                                                                
CONECT   62   50                                                                
CONECT   63   51                                                                
CONECT   64   52                                                                
CONECT   65   56   66   75   82                                                 
CONECT   66   65   67   73   83                                                 
CONECT   67   66   68   84                                                      
CONECT   68   67   69   74   85                                                 
CONECT   69   68   70   75   86                                                 
CONECT   70   69   76   87   88                                                 
CONECT   71   72   73   77                                                      
CONECT   72   71   89   90   91                                                 
CONECT   73   66   71   92                                                      
CONECT   74   68   81                                                           
CONECT   75   65   69                                                           
CONECT   76   70                                                                
CONECT   77   71                                                                
CONECT   78   81                                                                
CONECT   79   81                                                                
CONECT   80   81                                                                
CONECT   81   74   78   79   80                                                 
CONECT   82   65                                                                
CONECT   83   66                                                                
CONECT   84   67                                                                
CONECT   85   68                                                                
CONECT   86   69                                                                
CONECT   87   70                                                                
CONECT   88   70                                                                
CONECT   89   72                                                                
CONECT   90   72                                                                
CONECT   91   72                                                                
CONECT   92   73                                                                
CONECT   93   13   95   96   97                                                 
CONECT   93   99                                                                
CONECT   94   34   59  102  103                                                 
CONECT   94  104  106                                                           
CONECT   95   93                                                                
CONECT   96   93                                                                
CONECT   97   93                                                                
CONECT   99   93                                                                
CONECT  102   94                                                                
CONECT  103   94                                                                
CONECT  104   94                                                                
CONECT  106   94                                                                
MASTER      214    0    6    0    0    0    0    6   76    0  104    0          
END                                                                             
