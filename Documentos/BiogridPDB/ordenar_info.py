import os 
import json
import pandas as pd


carpeta = os.listdir("./")

listado_txt = [i for i in carpeta if ".txt" in i]
listado_csv = [i for i in carpeta if ".csv" in i]

print(listado_txt)

columns_to_read = ['Index', 'BiogridId', 'SwissProtIdA', 'SwissProtIdB', 'PdbA', 'PdbB' ]


listado_pares = []
listado_info = []


for nombre_file in listado_txt:
    df = pd.read_csv(nombre_file, header=None, names=columns_to_read)
    values = df.values.tolist()

    for info in values:
        par = (info[4], info[5])
        par_invert = (info[5], info[4])

        if info[4] != "|" and info[5] != "|" and info[4] != "||" and info[5] != "||"  and info[4] != "|||" and info[5] != "|||" :
            if par not in listado_pares or par_invert not in listado_pares :
                # listado_pares.append(info[1:6])
                listado_pares.append(par)
                if info[1:6] not in listado_info:
                    listado_info.append(info[1:6])
    # print(len(values))

print("listado_pares",len(listado_pares))
print("listado_info",len(listado_info))

for nombre_file in listado_csv:
    df = pd.read_csv(nombre_file)
    values = df.values.tolist()
    for info in values:
        par = (info[3], info[4])
        par_invert = (info[4], info[3])
        if info[3] != "|" and info[4] != "|" and info[3] != "||" and info[4] != "||" and info[3] != "|||" and info[4] != "|||"  :
            if par not in listado_pares or par_invert not in listado_pares:
                # listado_pares.append(info[1:6])
                listado_pares.append(par)
                if info not in listado_info:
                    listado_info.append(info)


print("listado_pares",len(listado_pares))
print("listado_info",len(listado_info))


with open("ParesProteinasBiogrid.json", "w") as file:
    json.dump(listado_pares, file)

    