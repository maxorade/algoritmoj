# from transformers import AutoModelForCausalLM, AutoTokenizer

# model_name = "EleutherAI/gpt-neo-2.7B"
# model = AutoModelForCausalLM.from_pretrained(model_name)
# tokenizer = AutoTokenizer.from_pretrained(model_name)

# def extract_entities(text):
#     inputs = tokenizer.encode(text, return_tensors="pt")
#     outputs = model.generate(inputs, max_length=1024, do_sample=True, top_k=50, top_p=0.95)
#     decoded = tokenizer.decode(outputs, skip_special_tokens=True)
#     entities = []
#     for token in decoded.split():
#         if token.startswith("<") and token.endswith(">"):
#             entities.append(token[1:-1])
#     return entities

# text = "The human gene TP53 is associated with cancer."
# entities = extract_entities(text)
# print(entities)

# modelo = "EleutherAI/gpt-neo-1.3B"
# modelo = "EleutherAI/gpt-neo-2.7B"
# modelo = "EleutherAI/gpt-neo-125m"


# from transformers import pipeline

# info ='how to say in spanish : DisProt is a database of intrinsically disordered proteins that provides comprehensive information on protein disorder, including PPIs. The researchers employed text mining to identify protein sequence fragments that are related to PPIs. Their findings revealed key insights into the nature of PPIs that may aid in the development of new therapeutic approaches. Overall, the study highlights the utility of text mining in facilitating the search for PPIs within the context of DisProt.'
# generator = pipeline('text-generation', model='EleutherAI/gpt-neo-125m')
# print(generator(info, do_sample=True, max_length= 300, pad_token_id = 50256))

# from transformers import pipeline
# generator = pipeline('text-generation', model='EleutherAI/gpt-neo-125M')
# generator("EleutherAI has", do_sample=True, min_length=20)


# from transformers import pipeline

# translator = pipeline("translation", model="franjamonga/translate")
# text = "Hola, ¿cómo estás?"
# translated_text = translator(text,src_constituents= "spa",  tgt_constituents ="eng ")
# # print(translated_text)


from transformers import pipeline

model_name = "Helsinki-NLP/opus-mt-es-en"
translator = pipeline("translation_en_to_es", model=model_name, tokenizer=model_name)

input_text = "DisProt es una base de datos de proteínas intrínsecamente desordenadas que proporciona información completa sobre el desorden de las proteínas, incluyendo las interacciones proteína-proteína (PPIs). Los investigadores utilizaron minería de texto para identificar fragmentos de secuencia proteica relacionados con las PPIs. Sus hallazgos revelaron información clave sobre la naturaleza de las PPIs que podría ayudar en el desarrollo de nuevas aproximaciones terapéuticas. En general, el estudio destaca la utilidad de la minería de texto para facilitar la búsqueda de PPIs dentro del contexto de DisProt."
output_text = translator(input_text, max_length=512)[0]["translation_text"]
print(output_text)

