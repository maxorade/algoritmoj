import openai
import pandas as pd
from tqdm import tqdm
import time

key_gpt = "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"
openai.api_key = key_gpt

df = pd.read_excel('text_mining_protein_interactions 2017-2021 Mayor numero de citas.xlsx', index_col=False)

df_list  = df.values.tolist()

resumenes_articulos = []


for i in tqdm(range(len(df_list))): 
    autores = str(df_list[i][0]).strip()
    titulo = str(df_list[i][1]).strip()
    ano = str(df_list[i][3]).strip()

    # articulo = f"Author:{autores}  Title:{titulo} Year:{ano}"
    articulo = titulo

    mensaje =  {"role": "user", "content": f'make a 250 characters maximum summary of the most important points related to text mining for the search of protein-protein interactions of the following publication: {articulo}'}

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo", messages=[mensaje])

    response_ingles = response.choices[0].message.content

    mensaje2 ={"role": "user", "content": f'how to say in spanish : {response_ingles}'}

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo", messages=[mensaje2])

    response_espanol = response.choices[0].message.content

    info = {
        "Autores":autores,
        "Titulo":titulo,
        "Ano":ano,
        "ResumenEspanol":response_espanol,
        "ResumenIngles":response_ingles
    }

    resumenes_articulos.append(info)
    time.sleep(60)


df = pd.DataFrame(resumenes_articulos)
df.to_csv('ResumenRevision30Articulos.csv', index=False)