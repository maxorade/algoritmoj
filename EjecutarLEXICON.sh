for cls in NB SVM RF KNN
do
    for corpus in Biocreative3 AImed CorpusCompleto HPRD50 IEPA LLL
    do
        for tipo_corpus in NoFiltrado Filtrado
        do
            if [ "$tipo_corpus" = "NoFiltrado" ]
            then
                echo "Ejecutar algoritmo: Corpus: "${corpus}" Clasificador:" ${cls} "Corpus Filtrado: "${tipo_corpus}" Formato: LEXICON Filtro: ninguno"
                python ExperimentosML.py Corpus/${corpus}.csv ${cls} LEXICON ${tipo_corpus} ninguno &
            fi

            if [ "$tipo_corpus" = "Filtrado" ]
            then
                for filtro in spacy patrones
                do
                    echo "Ejecutar algoritmo: Corpus: "${corpus}" Clasificador:" ${cls} "Corpus Filtrado: "${tipo_corpus}" Formato: LEXICON Filtro: "${filtro}
                    python ExperimentosML.py Corpus/${corpus}.csv ${cls} LEXICON ${tipo_corpus} ${filtro} &
                done 
                
            fi
        done    
    done
done