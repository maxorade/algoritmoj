import subprocess
import concurrent.futures
from tqdm import tqdm

# p1 = subprocess.Popen('command1', shell=True)
# p1.wait()
# p2 = subprocess.Popen('command2', shell=True)
# p2.wait()


corpus = ["AImed","Biocreative3","CorpusCompleto","LLL","IEPA","HPRD50" ]
algoritmo_clasificacion_ml = ["NB","SVM","RF","KNN"]
representacion_documento = ["TFIDF_LEXICON","WEMB_LEXICON","WEMB_LEXICON_TFIDF" ]
informacion_corpus = ["Filtrado","NoFiltrado" ]
tipo_filtro = ["ninguno","spacy","patrones"  ]
guardar_modelo = ["NoGuardar"  ]

listado_comands = []

for representacion in representacion_documento:
        for filtro in informacion_corpus:
            for tipo in tipo_filtro:
                for corp in corpus:
                    for algoritmo in algoritmo_clasificacion_ml:
                        comand = f"python ExperimentosML.py {corp} {algoritmo} {representacion} {filtro} {tipo} NoGuardar"
                        if filtro == "NoFiltrado" and tipo == "ninguno":
                            listado_comands.append(comand)
                        elif filtro == "Filtrado" and tipo != "ninguno":
                            listado_comands.append(comand)

                            
print(len(listado_comands))

# comandos = listado_comands[0:1]

# with concurrent.futures.ProcessPoolExecutor(max_workers=len(comandos)) as executor:
#     futures = []
#     for comando in comandos:
#         future = executor.submit(subprocess.run, comando, check=True)
#         futures.append(future)

#     for future in concurrent.futures.as_completed(futures):
#         resultado = future.result()
#         print("Subproceso terminado con código de salida: ", resultado.returncode)


for i in tqdm(range(len(listado_comands))): 
    comand = listado_comands[i]
    p1 = subprocess.Popen(comand, shell=True)
    p1.wait()
     
# processes = []
# for command in listado_comands:
#     processes.append(subprocess.Popen(command, shell=True))
    
# for process in processes:
#     process.wait()


# 1:06:27 "LEXICON","TFIDF","WEMB" 