import spacy
from sklearn.feature_extraction.text import TfidfVectorizer
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Tokenizador import Tokenizador
from sklearn.pipeline import FeatureUnion
from gensim.models import KeyedVectors
import warnings
warnings.filterwarnings('ignore')


with open("Documentos/EjemploRepresentacionDocumentos.txt", "w") as f:

    nlp = spacy.load('en_ner_jnlpba_md')


    texto = "The complex structure of the SARS-CoV-2 RBD and ACE2 complex (PDB: 6M0J), SARS-CoV-2 Spike with furin cleavage site structure (PDB: 7FG7) and Human furin structure (PDB: 5MIM) were obtained from the PDB database.\n"

    f.write(texto)

    vectorizer = VectorLexicon(nlp=nlp)

    X = vectorizer.fit_transform([texto])

    f.write("Representación Lexicón Intensidad PPI\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    vectorizer = TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))

    X = vectorizer.fit_transform([texto])
    first_vector = X[0,:].todense().tolist()[0]

    f.write("Representación TFIDF\n")
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")

    representacionVectorial = KeyedVectors.load_word2vec_format(
                'Transformer/Embeddings/PubMed-w2v.bin', binary=True)

    vectorizer = VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)

    X = vectorizer.fit_transform([texto])



    f.write("Representación WE\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    vectorizer = FeatureUnion([
                ('TFIDF', TfidfVectorizer(
                    tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
                ('Lexicon', VectorLexicon(nlp=nlp))
            ])

    X = vectorizer.fit_transform([texto])
    first_vector = X[0,:].todense().tolist()[0]
    f.write("Representación LI + TFIDF\n")
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")

    vectorizer = FeatureUnion([
                ('WordEmbedding', VectorWordEmbedding(
                    nlp=nlp, representacionVectorial=representacionVectorial)),
                ('Lexicon', VectorLexicon(nlp=nlp))
            ])

    X = vectorizer.fit_transform([texto])
    # first_vector = X[0,:].todense().tolist()[0]
    f.write("Representación LI + wE\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    vectorizer = FeatureUnion([
                ('WordEmbedding', VectorWordEmbedding(
                    nlp=nlp, representacionVectorial=representacionVectorial)),
                ('Lexicon', VectorLexicon(nlp=nlp)),
                ('TFIDF', TfidfVectorizer(
                    tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
                ))
            ])

    X = vectorizer.fit_transform([texto])
    f.write("Representación LI + TFIDF + wE\n")
    # f.write(f"{len(X[0])}\n")

    first_vector = X[0,:].todense().tolist()[0]
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")


    #####################################
    f.write(f"#####################################\n")

    texto = "Combined with the results of the molecular docking, molecular dynamics simulations were performed on the abovementioned conformations as follows: SERPINE1 (3UT3)-Emodin and COL1A1 (7DV6)-Quercetin. RMSD stands for the distance between the same atoms at different simulation times, which can reveal the position changes between the protein conformation and the initial conformation during the simulation process. The changing trend of RMSD of proteins and ligands is also a momentous index to judge whether the simulation is stable or not. The analysis manifested that the SERPINE1-Emodin complex exhibited a certain degree of stability during the simulation, with the mean RMSD of 0.215nm (max=0.284nm, min=0.102nm). The value of the system increased slowly within 540ns, and the curve tended to be stable after 40ns. Meanwhile, it was also noted that there was a peak fluctuation in the RMSD curve of all complexes with SERPINE1 protein after 80ns, whilst the curve of emodin was still stable. It was speculated that there might exist a certain conformational transformation of the protein at this time (\n"

    f.write(texto)

    vectorizer = VectorLexicon(nlp=nlp)

    X = vectorizer.fit_transform([texto])

    f.write("Representación Lexicón Intensidad PPI\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    vectorizer = TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))

    X = vectorizer.fit_transform([texto])
    first_vector = X[0,:].todense().tolist()[0]

    f.write("Representación TFIDF\n")
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")

    representacionVectorial = KeyedVectors.load_word2vec_format(
                'Transformer/Embeddings/PubMed-w2v.bin', binary=True)

    vectorizer = VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)

    X = vectorizer.fit_transform([texto])



    f.write("Representación WE\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    vectorizer = FeatureUnion([
                ('TFIDF', TfidfVectorizer(
                    tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
                ('Lexicon', VectorLexicon(nlp=nlp))
            ])

    X = vectorizer.fit_transform([texto])
    first_vector = X[0,:].todense().tolist()[0]
    f.write("Representación LI + TFIDF\n")
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")

    vectorizer = FeatureUnion([
                ('WordEmbedding', VectorWordEmbedding(
                    nlp=nlp, representacionVectorial=representacionVectorial)),
                ('Lexicon', VectorLexicon(nlp=nlp))
            ])

    X = vectorizer.fit_transform([texto])
    # first_vector = X[0,:].todense().tolist()[0]
    f.write("Representación LI + wE\n")
    f.write(f"Largo: {len(X[0])}\n")
    f.write(f"Formato: {X[0]}\n")

    vectorizer = FeatureUnion([
                ('WordEmbedding', VectorWordEmbedding(
                    nlp=nlp, representacionVectorial=representacionVectorial)),
                ('Lexicon', VectorLexicon(nlp=nlp)),
                ('TFIDF', TfidfVectorizer(
                    tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
                ))
            ])

    X = vectorizer.fit_transform([texto])
    f.write("Representación LI + TFIDF + wE\n")
    # f.write(f"{len(X[0])}\n")

    first_vector = X[0,:].todense().tolist()[0]
    f.write(f"Largo: {len(first_vector)}\n")
    f.write(f"Formato: {first_vector}\n")