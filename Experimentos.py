# Librerías
import os
import random
import time
from sklearn.pipeline import Pipeline
import spacy
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
from gensim.models import KeyedVectors
from sklearn.model_selection import train_test_split
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import Herramientas.FuncionesClasificador as fc
from Transformer.VectorEncondeSentence import VectorEncondeSentence


# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta_corpus = sys.argv[1]
    clasificador = sys.argv[2]
    formato = sys.argv[3]
    filtro = sys.argv[4]
    buscador_p = sys.argv[5]
    uso_info_extra = sys.argv[6]

    info_extra = ""
    complementos = []

    nombre_corpus = (ruta_corpus.replace("Corpus/", "")).replace(".csv", "")

    jnlpba = spacy.load('en_ner_jnlpba_md')
    if buscador_p == "patrones":
        with open('Documentos/PatronProteinasSwissProt.txt') as f:
            patron_swiss = f.readlines()
        patron_swiss = patron_swiss[0]

    if filtro == "NoFiltrado":
        corpus = pd.read_csv(ruta_corpus)

        if os.path.exists(f"Documentos/{nombre_corpus}") == False:
            os.mkdir(f"Documentos/{nombre_corpus}")
            os.mkdir(f"Documentos/{nombre_corpus}/Proteinas")
            os.mkdir(f"Documentos/{nombre_corpus}/ProteinasPatrones")
            os.mkdir(f"Documentos/{nombre_corpus}/Palabras")
            os.mkdir(f"Documentos/{nombre_corpus}/PalabrasPatrones")

        corpus_filtrado = []
        for i in tqdm(range(len(list(corpus["Abstract"])))):
            abstract = corpus["Abstract"][i].lower()
            doc = jnlpba(abstract)
            if buscador_p == "spacy":
                ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/Palabras"
                buscar_proteinas = bp.ExtraerProteinas(
                    abstract=abstract, doc=doc)
                if buscar_proteinas[0] == False:
                    continue
            if buscador_p == "patrones":
                ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/PalabrasPatrones"
                buscar_proteinas = bp.ExtraerProteinasPatrones(
                    abstract=abstract, patron=patron_swiss)
                if buscar_proteinas[0] == False:
                    continue

            buscar_palabras_claves = fl.ValidarPalabrasClaves(doc=doc)
            if buscar_palabras_claves[0] == False:
                continue

            nombre_doc = len(corpus_filtrado)

            bp.RegistrarProteinas(
                listado_proteinas=buscar_proteinas[1], nombre_documento=nombre_doc, ruta=ruta_proteinas)
            fl.RegistrarPalabrasClaves(
                listado_palabras=buscar_palabras_claves[1], nombre_documento=nombre_doc, ruta=ruta_palabras_lexicon)
            datos_filtro = {
                "Abstract": abstract,
                "Clasificacion": corpus["Clasificacion"][i]
            }
            corpus_filtrado.append(datos_filtro)

        corpus_filtrado = pd.DataFrame(corpus_filtrado)
        if buscador_p == "spacy":
            ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
        if buscador_p == "patrones":
            ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"

        corpus_filtrado.to_csv(ruta_corpus_filtrado, index=False)
        corpus = corpus_filtrado
        # corpus_formateado = fl.ObtenerInformacion(
        #     corpus=corpus, listado_proteinas=buscar_proteinas[1], listado_palabras_lexicon=buscar_palabras_claves[1])

    # if filtro == "Filtrado":
    #     listado_proteinas = []
    #     listado_palabras_lexicon = []
    #     if os.path.exists(f"Documentos/{nombre_corpus}") == True:
    #         if buscador_p == "spacy":
    #             ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
    #             ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/Palabras"
    #             ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
    #         if buscador_p == "patrones":
    #             ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"
    #             ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/PalabrasPatrones"
    #             ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"

    #         corpus = pd.read_csv(ruta_corpus_filtrado)

    #         carpeta_palabras_lexicon = os.listdir(ruta_palabras_lexicon)

    #         print("Cargar Palabras Claves")
    #         for doc_palabras in tqdm(range(len(list(carpeta_palabras_lexicon)))):
    #             palabras_abs = pd.read_csv(
    #                 f"Documentos/{nombre_corpus}/Palabras/{carpeta_palabras_lexicon[doc_palabras]}")
    #             listado_palabras_lexicon.append(palabras_abs)

    #         print("Cargar Proteinas")
    #         carpeta_proteinas = os.listdir(ruta_proteinas)

    #         for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
    #             proteina_abs = pd.read_csv(
    #                 f"{ruta_proteinas}/{carpeta_proteinas[doc_proteina]}")
    #             listado_proteinas.append(proteina_abs)

    #         print("Obtener Informacion")
    #         corpus_formateado = fl.ObtenerInformacion(
    #             corpus=corpus, listado_proteinas=listado_proteinas, listado_palabras=listado_palabras_lexicon)

    #     if formato == "WEMB" or formato == "WEMB_LEXICON" or formato == "WEMB_LEXICON_TFIDF":
    #         representacionVectorial = KeyedVectors.load_word2vec_format(
    #             'Transformer/Embeddings/PubMed-w2v.bin', binary=True)
    #         abstracts = corpus["Abstract"]
    #         complementos.append(jnlpba)
    #         complementos.append(representacionVectorial)
    #         complementos.append(abstracts)
    #     if formato == "TFIDF" or formato == "TFIDF_LEXICON":
    #         complementos.append(jnlpba)

    
    # if uso_info_extra == "Si":
    #     X = np.asarray(corpus_formateado["Abstract"])
    #     X = X.ravel()
    #     y = np.asarray(corpus_formateado["Clasificacion"])
    #     y = y.ravel()

    #     info_extra = "InfoExtra"
    # else:
    #     X = np.asarray(corpus["Abstract"])
    #     X = X.ravel()
    #     y = np.asarray(corpus["Clasificacion"])
    #     y = y.ravel()

    # X_train, X_test, y_train, y_test = train_test_split(
    #     X, y, test_size=0.33, random_state=42)

    # start = time.time()

    # if uso_info_extra == "Si":
    #     X_train = X_train[0:4000]  # [0:1000] [0:20000]
    #     X_test = X_test[0:2000] # [0:600] [0:10000]
    #     y_train = y_train[0:4000]  # [0:1000] [0:20000]
    #     y_test = y_test[0:2000] # [0:600] [0:10000]

    # print(len(X_train))
    # print(len(X_test))
    # print("Entrenamiento")

    # clf = fc.SelecionarClasificador(
    #     nombre_clas=clasificador, formato=formato, complementos=complementos)
    # clf.fit(X_train, y_train)

    # y_pred = clf.predict(X_test)

    # fc.ReporteResultado(y_test=y_test, y_pred=y_pred,
    #                     carpeta=clasificador, nombre_res=f"{nombre_corpus + clasificador }_{formato}{info_extra}", tiempo_inicio=start)

    # fc.GuardarModelo(clf=clf, carpeta=clasificador,
    #                  nombre_res=f"{nombre_corpus + clasificador }_{formato}{info_extra}")

    # print("Corpus :", ruta_corpus)
    # print("Algoritmo Clasificador:", clasificador)
    # print("Formato de entrada:", formato)
    # print("Corpus Filtrado :", filtro)
    # print("Buscador :", buscador_p)
