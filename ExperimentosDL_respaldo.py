# Librerías
import json
import os
import random
import time
from typing import Counter
from sklearn.pipeline import Pipeline
import spacy
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
from gensim.models import KeyedVectors
from sklearn.model_selection import train_test_split
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import Herramientas.FuncionesClasificadorDL as fcdl
import Herramientas.FuncionesClasificador as fc
from Transformer.VectorEncondeSentence import VectorEncondeSentence


# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta_corpus = sys.argv[1]
    clasificador = sys.argv[2]
    formato = sys.argv[3]
    filtro = sys.argv[4]
    buscador_p = sys.argv[5]

    complementos = []

    nombre_corpus = (ruta_corpus.replace("Corpus/", "")).replace(".csv", "")

    jnlpba = spacy.load('en_ner_jnlpba_md')

    if filtro == "Filtrado":
        listado_proteinas = []
        listado_palabras_lexicon = []
        if os.path.exists(f"Documentos/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/Palabras"
                ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
                ruta_diccionario = f"Documentos/{nombre_corpus}/{nombre_corpus}Diccionario.json"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/PalabrasPatrones"
                ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"
                ruta_diccionario = f"Documentos/{nombre_corpus}/{nombre_corpus}PatronesDiccionario.json"

            corpus = pd.read_csv(ruta_corpus_filtrado)

            carpeta_palabras_lexicon = os.listdir(ruta_palabras_lexicon)

            print("Cargar Palabras Claves")
            for doc_palabras in tqdm(range(len(list(carpeta_palabras_lexicon)))):
                palabras_abs = pd.read_csv(
                    f"Documentos/{nombre_corpus}/Palabras/{carpeta_palabras_lexicon[doc_palabras]}")
                listado_palabras_lexicon.append(palabras_abs)

            print("Cargar Proteinas")
            carpeta_proteinas = os.listdir(ruta_proteinas)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                proteina_abs = pd.read_csv(
                    f"{ruta_proteinas}/{carpeta_proteinas[doc_proteina]}")
                listado_proteinas.append(proteina_abs)

            print("Obtener Informacion")
            corpus_formateado = fl.ObtenerInformacion(
                corpus=corpus, listado_proteinas=listado_proteinas, listado_palabras=listado_palabras_lexicon)


        if clasificador == "BERT":
            complementos.append(clasificador)
        else:
            representacionVectorial = KeyedVectors.load_word2vec_format(
                'Transformer/Embeddings/PubMed-w2v.bin', binary=True)
            dic = open(ruta_diccionario)
            dic = json.load(dic)
            counts = Counter(dic)
            complementos.append(clasificador)
            complementos.append(jnlpba)
            complementos.append(representacionVectorial)
            complementos.append(counts)

        X = np.asarray(corpus[["Abstract"]])
        X = X.ravel()
        y = np.asarray(corpus[["Clasificacion"]])
        y = y.ravel()

        print(len(X))

        train_data, test_data = fcdl.FormatearInput(
            X=X, y=y, complementos=complementos)

        start = time.time()
        print("Entrenamiento")

        clf = fcdl.SelecionarClasificador(
            nombre_clas=clasificador, formato=formato, complementos=complementos)

        fcdl.EntrenamientoReporteResultado(
            clf=clf,
            train_data=train_data,
            test_data=test_data,
            clasificador=clasificador,
            epochs=1,
            carpeta=clasificador,
            nombre_res=f"{nombre_corpus + clasificador }_{formato}",
            tiempo_inicio=start)

    print("Corpus :", ruta_corpus)
    print("Algoritmo Clasificador:", clasificador)
    print("Formato de entrada:", formato)
    print("Corpus Filtrado :", filtro)
    print("Buscador :", buscador_p)
