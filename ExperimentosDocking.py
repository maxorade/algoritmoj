# Librerías
import os
import itertools
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
import Herramientas.BusquedaProteinas as bp
import joblib
import Herramientas.FuncionesClasificador as fc
import re
import spacy
import Herramientas.FuncionesLexicon as fl

# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta_corpus = f"Corpus/{sys.argv[1]}.csv"
    buscador_p = sys.argv[2]
    opcion = sys.argv[3]
    modelo = sys.argv[4]
    nombre_modelo = modelo

    nombre_corpus = (ruta_corpus.replace("Corpus/", "")).replace(".csv", "")
    listado_proteinas = []

    if opcion == "UsoModelo":

        info_modelo = modelo.split("_")
        ruta_resultados = f"Resultados/{info_modelo[1]}/Modelo"
        ruta_modelo = f"{ruta_resultados}/{modelo}.joblib"

        if os.path.exists(f"Documentos/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
                nombre_corpus_filtrado = f"{nombre_corpus}_{modelo}.csv"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"
                nombre_corpus_filtrado = f"{nombre_corpus}_Patrones_{modelo}.csv"
            if buscador_p == "ninguno":
                ruta_corpus_filtrado = ruta_corpus
                nombre_corpus_filtrado = f"{nombre_corpus}_SinFiltro_{modelo}.csv"

            corpus = pd.read_csv(ruta_corpus_filtrado)

            X = np.asarray(corpus["Abstract"])
            X = X.ravel()
            y = np.asarray(corpus["Clasificacion"])
            y = y.ravel()

            modelo = joblib.load(ruta_modelo)

            y_pred = modelo.predict(X)

            listado_pred = [{"Index": index, "Abstract": X[index]}
                            for index, val in enumerate(y) if val == 1 and y_pred[index] == 1]

            # listado_pred = [{"Index": index, "Abstract": X[index] , "Resultado": val}
            #             for index, val in enumerate(y_pred)]

            if len(listado_pred) == 0:
                print("No se encontro un texto con una posible interacción")
            else:

                print(
                    f"Cantidad de textos clasificados correctamente : {len(listado_pred)} \n")
                df = pd.DataFrame(data=listado_pred)
                df.to_csv(
                    f'Docking/{nombre_corpus}/{nombre_corpus_filtrado}', index=False)

                fc.EvaluarModelo(y, y_pred, ruta_resultados,
                                 modelo, nombre_corpus)

    if opcion == "Final":

        # Complementos

        jnlpba = spacy.load('en_ner_jnlpba_md')
        with open('Documentos/PatronProteinasSwissProt.txt') as f:
            patron_swiss = f.readlines()

        patron = patron_swiss[0]

        ####

        if buscador_p == "spacy":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_{modelo}"
        if buscador_p == "patrones":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_Patrones_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_Patrones_{modelo}"
        if buscador_p == "ninguno":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_SinFiltro_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_SinFiltro_{modelo}"

        corpus = pd.read_csv(ruta_corpus_filtrado)

        corpus = corpus.values.tolist()

        listado_par = []

        

        for i in tqdm(range(len(corpus))):
            # variables
            index = corpus[i][0]
            abstract = corpus[i][1].lower()

            # print(i)

            # print(abstract)

            doc = jnlpba(abstract)

            proteinas_docs = []

            proteinas_spacy = bp.BusquedaProteinasSpacy(abstract, doc)
            proteinas_patron = bp.BusquedaProteinasPatrones(abstract, patron)
            proteinas_corpus = bp.BuscarProteinasCorpus(abstract)

            for j in proteinas_spacy:
                if j not in proteinas_docs: 
                    if j in proteinas_patron or j in proteinas_corpus:
                        proteinas_docs.append(j)
            for j in proteinas_patron:
                if j not in proteinas_docs: 
                    if j in proteinas_spacy or j in proteinas_corpus:
                        proteinas_docs.append(j)
            for j in proteinas_corpus:
                if j not in proteinas_docs: 
                    if j in proteinas_spacy or j in proteinas_patron:
                        proteinas_docs.append(j)
            

            if len(proteinas_docs) <= 1:
                continue
            print("_________________")
            print("largo texto :", len(abstract))
            print("proteinas validadas",proteinas_docs )
            
            proteinas = []
            for  j in proteinas_docs:
                indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
                proteinas_aparicion = [{"Proteina": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
                info_proteina = {
                    "Proteina": j,
                    "ListadoIndex": proteinas_aparicion
                }
                proteinas.append(info_proteina)
            print("proteina :", proteinas)

            pair_order_list = itertools.permutations(proteinas_docs, 2)

            listado_par_filtrado = []
            for par in list(pair_order_list):
                par_prueba = (par[1], par[0])
                if (par[1] not in  par[0]) and (par[0] not in  par[1]):
                    if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                        listado_par_filtrado.append(par)

            print("pares de proteinas", listado_par_filtrado)
            
            palabras_docs =  fl.ValidarPalabrasClaves(doc)[1]
            print("palabras positivas :", palabras_docs)
            
            palabras_claves = []
            for  j in palabras_docs:
                indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
                palabra = [{"Palabra": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
                info_palabra = {
                    "Palabra": j,
                    "ListadoIndex": palabra
                }
                palabras_claves.append(info_palabra)

            print("palabras :", palabras_claves)

            print("_________________")


                    

            # print("proteinas_spacy", proteinas_spacy)
            # print("proteinas_patron", proteinas_patron)
            # print("proteinas_corpus", proteinas_corpus)

            # listado_par = bp.BusquedaProteinasDiccionarioProteinasCorpus(index, abstract, ruta_registro_procorpus, listado_par, cantidad_min_res )

    if opcion == "BusquedaProteinasExtra":
        cantidad_min_res = 4000

        # if os.path.exists(f"Documentos/{nombre_corpus}") == True:
        if buscador_p == "spacy":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_{modelo}"
        if buscador_p == "patrones":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_Patrones_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_Patrones_{modelo}"
        if buscador_p == "ninguno":
            ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_SinFiltro_{modelo}.csv"
            ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_SinFiltro_{modelo}"

        if os.path.exists(ruta_registro_proteinas) == False:
            os.mkdir(ruta_registro_proteinas)

        ruta_registro_procorpus = ruta_registro_proteinas + "/ProteinasDiccionarioCorpus"

        if os.path.exists(ruta_registro_procorpus) == False:
            os.mkdir(ruta_registro_procorpus)

        corpus = pd.read_csv(ruta_corpus_filtrado)

        corpus = corpus.values.tolist()

        listado_par = []

        for i in tqdm(range(len(corpus))):
            # variables
            index = corpus[i][0]
            abstract = corpus[i][1].lower()
            listado_par = bp.BusquedaProteinasDiccionarioProteinasCorpus(
                index, abstract, ruta_registro_procorpus, listado_par, cantidad_min_res)

        # if len(listado_par) == 0:
        #     print("No se encontraron proteinas con el tamaño requerido")

        # else:
        #     print(f"Cantidad pares de proteinas : {len(listado_par)}")
        #     df_info_registro = pd.DataFrame(listado_par)
        #     df_info_registro.to_csv(ruta_registro_procorpus + "/pares_seleccionados.csv", index=False)

    if opcion == "RegistroProteinas":
        if os.path.exists(f"Documentos/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_{modelo}.csv"
                ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
                ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_{modelo}"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"Docking/{nombre_corpus}/{nombre_corpus}_Patrones_{modelo}.csv"
                ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"
                ruta_registro_proteinas = f"Docking/{nombre_corpus}/Proteinas_Patrones_{modelo}"

            if os.path.exists(ruta_registro_proteinas) == False:
                os.mkdir(ruta_registro_proteinas)

            print("Cargar Proteinas")
            try:
                corpus = pd.read_csv(ruta_corpus_filtrado)
                for index in tqdm(range(len(list(corpus["Abstract"])))):
                    id_index = corpus["Index"][index]

                    ruta_proteina = f"{ruta_proteinas}/{id_index}"

                    proteina_abs = pd.read_csv(
                        f"{ruta_proteinas}/{id_index}")
                    ruta_registro = f"{ruta_registro_proteinas}/{id_index}"

                    for datos_proteina in proteina_abs.values.tolist():
                        pdb_id = datos_proteina[1]
                        ruta_proteina = f"{ruta_registro}/{pdb_id}.pdb"
                        if os.path.exists(ruta_proteina) == False:
                            bp.GuardarProteina(pdb_id, ruta_registro)
            except:
                print("######## No se encontraron proteinas ########")

    if opcion == "ReducirProteinas":
        if os.path.exists(f"Docking/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas"
            if buscador_p == "patrones":
                ruta_registro = f"Docking/{nombre_corpus}/ProteinasPatrones"
            print("Cargar Proteinas")
            carpeta_proteinas = os.listdir(ruta_registro)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                carpeta_abs = os.listdir(ruta_abs)
                for proteina in carpeta_abs:
                    pdb_id = proteina.replace(".pdb", "")
                    bp.ReducePDB(pdb_id, ruta_abs)

    if opcion == "GenerarPDBQT":
        if os.path.exists(f"Docking/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas"
            if buscador_p == "patrones":
                ruta_registro = f"Docking/{nombre_corpus}/ProteinasPatrones"
            print("Cargar Proteinas")
            carpeta_proteinas = os.listdir(ruta_registro)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                carpeta_abs = os.listdir(ruta_abs)
                for proteina in carpeta_abs:
                    if ".pdbqt" not in proteina:
                        pdb_id = proteina.replace(".pdb", "")

                        if "_recH" in pdb_id:
                            validar_rec = True if f"{pdb_id}_recH.pdbqt" in carpeta_abs else False
                            if validar_rec == False:
                                bp.FormatoPDBQTReceptor(pdb_id, ruta_abs)
                        elif "_ligH" in pdb_id:
                            validar_lig = True if f"{pdb_id}_ligH.pdbqt" in carpeta_abs else False
                            if validar_lig == False:
                                bp.FormatoPDBQTLigando(pdb_id, ruta_abs)

    if opcion == "GenerarPDBQTOpenBabel":
        if os.path.exists(f"Docking/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas"
            if buscador_p == "patrones":
                ruta_registro = f"Docking/{nombre_corpus}/ProteinasPatrones"

            print("Inicio Generar PDBQT")
            carpeta_proteinas = os.listdir(ruta_registro)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                carpeta_abs = os.listdir(ruta_abs)
                for proteina in carpeta_abs:
                    if ".pdbqt" not in proteina:
                        pdb_id = proteina.replace(".pdb", "")

                        if "_recH" not in pdb_id or "_ligH" not in pdb_id:
                            validar_rec = True if f"{pdb_id}_recH.pdbqt" in carpeta_abs else False
                            if validar_rec == False:
                                bp.FormatoPDBQTOpenBabel(pdb_id, ruta_abs)

    if opcion == "GenerarParesProteinasPDB":
        if os.path.exists(f"Docking/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas_{modelo}"
            if buscador_p == "patrones":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas_Patrones_{modelo}"

            carpeta_proteinas = os.listdir(ruta_registro)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                carpeta_abs = os.listdir(ruta_abs)
                listado_pdb = [doc for doc in carpeta_abs if ".pdbqt" not in doc and ("_recH.pdb" not in doc) == True and (
                    "_ligH.pdb" not in doc) == True and ("pares_proteinas.csv" not in doc) == True]
                pair_order_list = itertools.permutations(listado_pdb, 2)
                listado_par_filtrado = []
                info_registro = []
                for par in list(pair_order_list):
                    par_prueba = (par[1], par[0])
                    if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                        listado_par_filtrado.append(par)
                        info_ipp = {
                            "pro1_pdb": "",
                            "pro1_tamano": "",
                            "pro2_pdb": "",
                            "pro2_tamano": ""
                        }
                        with open(f"{ruta_abs}/{par[0]}", "r") as f:
                            info_ipp["pro1_pdb"] = par[0]
                            info_ipp["pro1_tamano"] = len(f.readlines())

                        with open(f"{ruta_abs}/{par[1]}", "r") as f:
                            info_ipp["pro2_pdb"] = par[1]
                            info_ipp["pro2_tamano"] = len(f.readlines())

                        info_registro.append(info_ipp)
                df_info_registro = pd.DataFrame(info_registro)
                ruta_info_registro = f"{ruta_abs}/pares_proteinas.csv"
                df_info_registro.to_csv(ruta_info_registro, index=False)

                # print(len())

                # for proteina in carpeta_abs:
                #     if ".pdbqt" not in proteina:
                #         pdb_id = proteina.replace(".pdb", "")

                #         if "_recH" not in pdb_id or "_ligH" not in pdb_id :
                #             validar_rec = True if f"{pdb_id}_recH.pdbqt" in  carpeta_abs else False
                #             if validar_rec == False:
                #                 bp.FormatoPDBQTOpenBabel(pdb_id, ruta_abs)

    if opcion == "SeleccionarParesProteinasPDB":
        cantidad_min_res = 3000
        if os.path.exists(f"Docking/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas_{modelo}"
                ruta_seleccionar = f"Docking/{nombre_corpus}/{nombre_corpus}_Paresdock_{modelo}.csv"
            if buscador_p == "patrones":
                ruta_registro = f"Docking/{nombre_corpus}/Proteinas_Patrones_{modelo}"
                ruta_seleccionar = f"Docking/{nombre_corpus}/{nombre_corpus}_Patrones_Paresdock_{modelo}.csv"

            carpeta_proteinas = os.listdir(ruta_registro)
            listado_par = []
            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                ruta_info_registro = f"{ruta_abs}/pares_proteinas.csv"

                # print(ruta_info_registro)
                try:
                    pares_prot = pd.read_csv(ruta_info_registro)
                    for par in pares_prot.values.tolist():
                        if par[1] < cantidad_min_res and par[3] < cantidad_min_res:
                            info_ipp = {
                                "abstract": carpeta_proteinas[doc_proteina],
                                "pro1_pdb": par[0],
                                "pro1_tamano": par[1],
                                "pro2_pdb": par[2],
                                "pro2_tamano": par[3]
                            }
                            listado_par.append(info_ipp)

                except:
                    print(f"sin pares -> {ruta_info_registro}")

            if len(listado_par) == 0:
                print("No se encontraron proteinas con el tamaño requerido")

            else:
                print(f"Cantidad pares de proteinas : {len(listado_par)}")
                df_info_registro = pd.DataFrame(listado_par)
                df_info_registro.to_csv(ruta_seleccionar, index=False)

    print("Modelo   :", nombre_modelo)
    print("Corpus   :", ruta_corpus)
    print("Buscador :", buscador_p)
    print("Opción   :", opcion)
