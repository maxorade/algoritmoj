# Librerías
import os
import spacy
import json
import sys
import numpy as np
import pandas as pd
from sentence_transformers import SentenceTransformer
import Herramientas.FuncionesLexicon as fl


#
# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta = sys.argv[1]
    opcion = sys.argv[2]

    if opcion == "BolsaPalabras": 
        filtro = sys.argv[3]
        nombre_corpus = (ruta.replace("Corpus/", "")).replace(".csv", "")
        ruta_bolsa = f"Documentos/{nombre_corpus}"
        if filtro == "ninguno":
            corpus = pd.read_csv(ruta)
        elif filtro == "spacy":
            corpus = pd.read_csv(f"{ruta_bolsa}/{nombre_corpus}.csv")

        elif filtro == "patrones":
            corpus = pd.read_csv(f"{ruta_bolsa}/{nombre_corpus}Patrones.csv")
        
        data_list = corpus.values.tolist()
        bag = fl.BolsaPalabras(listado = data_list )
        with open(f"{ruta_bolsa}/{nombre_corpus}_{filtro}_Diccionario.json", 'w') as f:
            json.dump(bag, f)

    if opcion == "Diccionario": 
        corpus = pd.read_csv(ruta)
        nombre_corpus = (ruta.replace("Corpus/", "")).replace(".csv", "")
        
        ruta_diccionario = "Documentos/NuevoLexicon/DiccionarioFrecuenciaInversa/"+ nombre_corpus

        data_list = corpus.values.tolist()
        listado_positivo = [i for i in data_list if i[1]  == 1]
        listado_negativo = [i for i in data_list if i[1]  == 0]


        df_positivo = fl.DicicionarioPalabrasFrecuenciaInversa(listado = listado_positivo )
        df_negativo = fl.DicicionarioPalabrasFrecuenciaInversa(listado = listado_negativo )

        df_positivo.to_csv(f"{ruta_diccionario}Positivo.csv", index = False)
        df_negativo.to_csv(f"{ruta_diccionario}Negativo.csv", index = False)

        

        print("Ruta Diccionario: ",ruta_diccionario + " Positivo o Negativo" )
        print("Opcion: ",opcion)

    if opcion == "Cluster":
        PRE_TRAINED_MODEL_NAME = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext'
        embedder = SentenceTransformer(PRE_TRAINED_MODEL_NAME)
        jnlpba = spacy.load('en_ner_jnlpba_md') 
        
        ruta_cluster = "Documentos/NuevoLexicon/Clusters/" +  ruta.replace(".csv", "")
        ruta_cluster = ruta_cluster + "Cluster.json"
        ruta_diccionario = "Documentos/NuevoLexicon/Diccionario/" +  ruta
        numero_cluster = int(sys.argv[3]) 
        palabras_lexicon = pd.read_csv(ruta_diccionario, sep=',')
        
        datos_emb_lemma =  fl.EmbeddingLemmaPalabras(lexicon_csv = palabras_lexicon , jnlpba = jnlpba, embedder = embedder)
        corpus_embeddings = datos_emb_lemma[0]
        palabras_lemma = datos_emb_lemma[1]

        clustered_sentences = fl.CrearClusters(num_clusters = numero_cluster ,corpus_embeddings =  corpus_embeddings, palabras_lemma=palabras_lemma )

        with open(ruta_cluster, 'w') as f:
            json.dump(clustered_sentences, f)

        print("Ruta Cluster: ",ruta_cluster )
        print("Opcion: ",opcion)

    if opcion == "Lexicon":
        ruta_cluster = "Documentos/NuevoLexicon/Clusters/" +  ruta
        ruta_lexicon = "Documentos/NuevoLexicon/Lexicon/" +  ruta.replace("Cluster", "Lexicon").replace(".json","") + ".csv"
        f = open(ruta_cluster)
        clustered_sentences = json.load(f)
        cluster_num = len(clustered_sentences)
        puntajes = [ 0 for i in  range(cluster_num)]

        pts = 10
        pts_sum = cluster_num - 1
        pts_sum = 80 / pts_sum
        pts_sum = int(round(pts_sum, 0)) 

        for index, value in  enumerate(puntajes):
            if index  + 1  == cluster_num:
                pts = 90
            puntajes[index] = pts
            pts = pts + pts_sum

        puntajes = sorted(puntajes , reverse=True)

        fl.CrearLexicon(clusters = clustered_sentences ,puntajes = puntajes, nombre_lexicon = ruta_lexicon)

        print("Ruta Lexicon: ",ruta_lexicon )
        print("Opcion: ",opcion)