# Librerías
import os
import random
import time
from sklearn.pipeline import Pipeline
import spacy
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
from gensim.models import KeyedVectors
from sklearn.model_selection import train_test_split
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import Herramientas.FuncionesClasificador as fc
from Transformer.VectorEncondeSentence import VectorEncondeSentence


# python ExperimentosML.py AImed KNN MULTIPLE_TFIDF Filtrado spacy Guardar &
# python ExperimentosML.py Biocreative3 RF TFIDF_LEXICON NoFiltrado ninguno Guardar &
# python ExperimentosML.py AImed SVM TFIDF_LEXICON Filtrado spacy Guardar &

# python ExperimentosML.py AImed NB LEXICON Filtrado spacy Guardar &


# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta_corpus = f"Corpus/{sys.argv[1]}.csv" 
    clasificador = sys.argv[2]
    formato = sys.argv[3]
    tipo_info = sys.argv[4]
    buscador_p = sys.argv[5]
    guardar_m = sys.argv[6]

    info_extra = ""
    complementos = []

    nombre_corpus = (ruta_corpus.replace("Corpus/", "")).replace(".csv", "")

    jnlpba = spacy.load('en_ner_jnlpba_md')
    if buscador_p == "patrones":
        with open('Documentos/PatronProteinasSwissProt.txt') as f:
            patron_swiss = f.readlines()
        patron_swiss = patron_swiss[0]
    
    if tipo_info == "NoFiltrado":
        corpus = pd.read_csv(ruta_corpus)
    
    if tipo_info == "Filtrado":
        if os.path.exists(f"Documentos/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"
            corpus = pd.read_csv(ruta_corpus_filtrado)

    if tipo_info == "InfoExtra":
        listado_proteinas = []
        listado_palabras_lexicon = []
        if os.path.exists(f"Documentos/{nombre_corpus}") == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/Palabras"
                ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"
                ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/PalabrasPatrones"
                ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"

            corpus = pd.read_csv(ruta_corpus_filtrado)

            carpeta_palabras_lexicon = os.listdir(ruta_palabras_lexicon)

            print("Cargar Palabras Claves")
            for doc_palabras in tqdm(range(len(list(carpeta_palabras_lexicon)))):
                palabras_abs = pd.read_csv(
                    f"{ruta_palabras_lexicon}/{carpeta_palabras_lexicon[doc_palabras]}")
                listado_palabras_lexicon.append(palabras_abs)

            print("Cargar Proteinas")
            carpeta_proteinas = os.listdir(ruta_proteinas)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                proteina_abs = pd.read_csv(
                    f"{ruta_proteinas}/{carpeta_proteinas[doc_proteina]}")
                listado_proteinas.append(proteina_abs)

            print("Obtener Informacion")
            corpus = fl.ObtenerInformacion(
                corpus=corpus, listado_proteinas=listado_proteinas, listado_palabras=listado_palabras_lexicon)
            

    if "WEMB" in formato :
        representacionVectorial = KeyedVectors.load_word2vec_format(
            'Transformer/Embeddings/PubMed-w2v.bin', binary=True)
        abstracts = corpus["Abstract"]
        complementos.append(jnlpba)
        complementos.append(representacionVectorial)
        complementos.append(abstracts)
    else: 
        complementos.append(jnlpba)


    X = np.asarray(corpus["Abstract"])
    # X = X.ravel()
    y = np.asarray(corpus["Clasificacion"])
    # y = y.ravel()

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.33, random_state=42)

    start = time.time()

    if tipo_info == "InfoExtra":
        X_train = X_train  # [0:4000] [0:1000] [0:20000]
        X_test = X_test # [0:2000] [0:600] [0:10000]
        y_train = y_train  # [0:4000] [0:1000] [0:20000]
        y_test = y_test # [0:2000] [0:600] [0:10000]


    print("Largo X para entrenamiento: ", len(X_train))
    print("Largo y para entrenamiento: ", len(X_test))
    print("Entrenamiento")

    clf = fc.SelecionarClasificador(
        nombre_clas=clasificador, formato=formato, complementos=complementos)
    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_test)


    nombre_res = f"{nombre_corpus}_{clasificador}_{formato}_{tipo_info}"

    if tipo_info == "InfoExtra" or tipo_info == "Filtrado":
        nombre_res = nombre_res + f"_{buscador_p}"

    fc.ReporteResultado(y_test=y_test, y_pred=y_pred,
                        carpeta=clasificador, nombre_res=nombre_res, tiempo_inicio=start)

    if guardar_m == "Guardar":
        fc.GuardarModelo(clf=clf, carpeta=clasificador,
                     nombre_res=f"{nombre_corpus}_{clasificador }_{formato}{buscador_p}")

    print("Corpus :", ruta_corpus)
    print("Algoritmo Clasificador:", clasificador)
    print("Formato de entrada:", formato)
    print("Corpus Filtrado :", tipo_info)
    print("Buscador :", buscador_p)
