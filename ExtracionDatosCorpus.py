# Librerías
import os
import spacy
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl


# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    ruta_corpus = sys.argv[1]
    buscador_p = sys.argv[2]

    info_extra = ""
    complementos = []

    nombre_corpus = (ruta_corpus.replace("Corpus/", "")).replace(".csv", "")

    jnlpba = spacy.load('en_ner_jnlpba_md')
    if buscador_p == "patrones":
        with open('Documentos/PatronProteinasSwissProt.txt') as f:
            patron_swiss = f.readlines()
        patron_swiss = patron_swiss[0]

    corpus = pd.read_csv(ruta_corpus)

    if os.path.exists(f"Documentos/{nombre_corpus}") == False:
        os.mkdir(f"Documentos/{nombre_corpus}")
        os.mkdir(f"Documentos/{nombre_corpus}/Proteinas")
        os.mkdir(f"Documentos/{nombre_corpus}/ProteinasPatrones")
        os.mkdir(f"Documentos/{nombre_corpus}/Palabras")
        os.mkdir(f"Documentos/{nombre_corpus}/PalabrasPatrones")

    corpus_filtrado = []
    for i in tqdm(range(len(list(corpus["Abstract"])))):
        abstract = corpus["Abstract"][i].lower()
        doc = jnlpba(abstract)
        if buscador_p == "spacy":
            ruta_proteinas = f"Documentos/{nombre_corpus}/Proteinas"
            ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/Palabras"
            buscar_proteinas = bp.ExtraerProteinas(
                abstract=abstract, doc=doc)
            if buscar_proteinas[0] == False:
                continue
        if buscador_p == "patrones":
            ruta_proteinas = f"Documentos/{nombre_corpus}/ProteinasPatrones"
            ruta_palabras_lexicon = f"Documentos/{nombre_corpus}/PalabrasPatrones"
            buscar_proteinas = bp.ExtraerProteinasPatrones(
                abstract=abstract, patron=patron_swiss)
            if buscar_proteinas[0] == False:
                continue

        buscar_palabras_claves = fl.ValidarPalabrasClaves(doc=doc)
        if buscar_palabras_claves[0] == False:
            continue

        nombre_doc = len(corpus_filtrado)

        bp.RegistrarProteinas(
            listado_proteinas=buscar_proteinas[1], nombre_documento=nombre_doc, ruta=ruta_proteinas)
        fl.RegistrarPalabrasClaves(
            listado_palabras=buscar_palabras_claves[1], nombre_documento=nombre_doc, ruta=ruta_palabras_lexicon)
        datos_filtro = {
            "Abstract": abstract,
            "Clasificacion": corpus["Clasificacion"][i]
        }
        corpus_filtrado.append(datos_filtro)

    corpus_filtrado = pd.DataFrame(corpus_filtrado)
    if buscador_p == "spacy":
        ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}.csv"
    if buscador_p == "patrones":
        ruta_corpus_filtrado = f"Documentos/{nombre_corpus}/{nombre_corpus}Patrones.csv"

    corpus_filtrado.to_csv(ruta_corpus_filtrado, index=False)
    corpus = corpus_filtrado