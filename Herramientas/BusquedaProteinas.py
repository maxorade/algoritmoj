import os
import requests
import pandas as pd
import re
import math
import subprocess
import os.path
import json
import itertools
import xml.etree.ElementTree as ET
from urllib.parse import quote
from Bio.PDB import Superimposer, PDBParser
import warnings
warnings.filterwarnings('ignore')

clasificaciones_no_permitidas = ["rna", "transcription", "endocytosis",
                                 "exocytosis", "cell cycle regulation", "translation"
                                 "cell cycle", "dna", "elongation factor",
                                 "viral protein", "endonuclease", "rna binding domain",
                                 "dna binding", "anti-oncogene", "gene regulation",
                                 "immune system", "chromatin binding protein",
                                 # complejo palabra de descarte  --> preguntar
                                 "complex",
                                 # nuevas palabras de descarte
                                 "chaperone",
                                 "ninguna"
                                 ]


def ValidarPDBIdExistente(pdb_id: str):
    url = f"https://data.rcsb.org/rest/v1/core/entry/{pdb_id}"

    response = requests.get(url)

    validacion = True

    if response.status_code == 404:
        validacion = False

    return validacion


def IdentificarPDBId(texto: str):
    pattern = r"\d\w{3}"
    matches = re.findall(pattern, texto)
    return matches


def EtiquetasTipoPubtator(pmc_id: str):
    informacion = BusquedaInformacionPubtatorPMCId(pmc_id=pmc_id)
    annotations = []

    for i in informacion["passages"]:
        annotations = annotations + i["annotations"]

    tipos = ['Gene', 'Disease', 'Chemical', 'Species', ]

    etiquetas_gene = []
    etiquetas_disease = []
    etiquetas_chemical = []
    etiquetas_species = []

    for i in annotations:
        texto = i["text"]

        if i["infons"]["type"] == tipos[0]:
            print(tipos[0])
            if texto not in etiquetas_gene:
                etiquetas_gene.append(texto)

        elif i["infons"]["type"] == tipos[1]:
            if texto not in etiquetas_disease:
                etiquetas_disease.append(texto)

        elif i["infons"]["type"] == tipos[2]:
            if texto not in etiquetas_chemical:
                etiquetas_chemical.append(texto)

        elif i["infons"]["type"] == tipos[3]:
            if texto not in etiquetas_species:
                etiquetas_species.append(texto)

    return etiquetas_gene,    etiquetas_disease,    etiquetas_chemical,    etiquetas_species


def BusquedaInformacionPubtatorPMCId(pmc_id: str):
    api_url = f"https://www.ncbi.nlm.nih.gov/research/pubtator-api/publications/export/biocjson?pmcids=PMC{pmc_id}"
    response = requests.get(api_url)
    data = response.json()

    return data


def GenerarVariantesNombreProteina(nombre_proteina: str):
    nombre_list = []

    nombre_proteina = nombre_proteina.replace("(+)", "")
    nombre_proteina = nombre_proteina.replace("[", "")
    nombre_proteina = nombre_proteina.replace("]", "")

    nombre_list.append(nombre_proteina)
    nombre_proteina = nombre_proteina.lower()
    nombre_list.append(nombre_proteina)

    match = re.search('[^\w\s]+', nombre_proteina)

    if match != None:
        nombre_proteina = re.sub('[^\w\s]+', ' ', nombre_proteina)
        nombre_list.append(nombre_proteina)

    return nombre_list


def BuscarComplejosUniprot(uniprot_id: str, ruta):
    url = f"https://www.ebi.ac.uk/proteins/api/proteins/interaction/{uniprot_id}"
    response = requests.get(url)
    data = response.json()

    proteinas = []
    for i in data:
        interactions = i["interactions"]
        for j in interactions:
            accession1 = j["accession1"]
            accession2 = j["accession2"]

            if accession1 not in proteinas:
                proteinas.append(accession1)
            if accession2 not in proteinas:
                proteinas.append(accession2)

    with open(ruta, "w") as f:
        json.dump(proteinas, f)


def BuscarInformacionUniprot(query: str):
    url_encoded_text = quote(query)
    url = f"https://rest.uniprot.org/uniprotkb/search?format=json&query={url_encoded_text}&size=500"
    response = requests.get(url)
    data = response.json()

    valido = True
    uniprot_id = ""
    pdb_ids = []
    referencias_listado = []
    nombres_proteina = []

    if len(data["results"]) != 0:

        if "primaryAccession" in data["results"][0]:

            uniprot_id = data["results"][0]["primaryAccession"]

        if "proteinDescription" in data["results"][0]:

            if "recommendedName" in data["results"][0]["proteinDescription"]:

                recommendedName = data["results"][0]["proteinDescription"]["recommendedName"]["fullName"]["value"]

                nombres_proteina = GenerarVariantesNombreProteina(
                    nombre_proteina=recommendedName)

            if "alternativeNames" in data["results"][0]["proteinDescription"]:

                alternativeNames = data["results"][0]["proteinDescription"]["alternativeNames"]

                for i in alternativeNames:
                    nombres_proteina = nombres_proteina + \
                        GenerarVariantesNombreProteina(
                            nombre_proteina=i["fullName"]["value"])

        if "references" in data["results"][0]:

            references = data["results"][0]["references"]

            referencias_listado = [i["citation"]["citationCrossReferences"][0]["id"]
                                   for i in references if i["citation"]["citationType"] == "journal article" and "citationCrossReferences" in i["citation"]]

        if "uniProtKBCrossReferences" in data["results"][0]:

            uniProtKBCrossReferences = data["results"][0]["uniProtKBCrossReferences"]

            pdb_ids = [i["id"]
                       for i in uniProtKBCrossReferences if i["database"] == "PDB"]

            if len(pdb_ids) > 3:
                pdb_ids = pdb_ids[0:3]
    else:
        valido = False

    return valido, uniprot_id, nombres_proteina, pdb_ids, referencias_listado


def InformacionUniprotPdbId(pdb_id: str):
    pdb_id = pdb_id.upper()
    url = f"https://rest.uniprot.org/uniprotkb/search?format=json&query=%28{pdb_id}%29&size=500"
    response = requests.get(url)
    data = response.json()

    valido = True
    uniprot_id = ""
    pdb_ids = []
    referencias_listado = []
    nombres_proteina = []

    if len(data["results"]) != 0:

        if "primaryAccession" in data["results"][0]:

            uniprot_id = data["results"][0]["primaryAccession"]

        if "recommendedName" in data["results"][0]["proteinDescription"]:

            recommendedName = data["results"][0]["proteinDescription"]["recommendedName"]["fullName"]["value"]

            nombres_proteina = GenerarVariantesNombreProteina(
                nombre_proteina=recommendedName)

        if "alternativeNames" in data["results"][0]["proteinDescription"]:

            alternativeNames = data["results"][0]["proteinDescription"]["alternativeNames"]

            for i in alternativeNames:
                nombres_proteina = nombres_proteina + \
                    GenerarVariantesNombreProteina(
                        nombre_proteina=i["fullName"]["value"])

        if "references" in data["results"][0]:

            references = data["results"][0]["references"]

            referencias_listado = [i["citation"]["citationCrossReferences"][0]["id"]
                                   for i in references if i["citation"]["citationType"] == "journal article" and "citationCrossReferences" in i["citation"]]

        uniProtKBCrossReferences = data["results"][0]["uniProtKBCrossReferences"]

        pdb_ids = [i["id"]
                   for i in uniProtKBCrossReferences if i["database"] == "PDB"]

        if len(pdb_ids) > 3:
            pdb_ids = pdb_ids[0:3]
        if pdb_id not in pdb_ids:
            valido = False

    else:
        valido = False

    return valido, uniprot_id, nombres_proteina, pdb_ids, referencias_listado


def BusquedaProteinasDiccionarioProteinasCorpus(index: str, abstract: str, ruta_registro_procorpus: str, listado_par, cantidad_min_res=3000):
    ruta_registro = f"{ruta_registro_procorpus}/{index}"

    # Busqueda proteinas corpus
    proteinas_corpus = BuscarProteinasCorpus(abstract)
    proteinas = []
    for j in proteinas_corpus:
        pdb_id = GetPDBId(nombre=j)
        clasifi_pdb = BusquedaClasificacionProteina(
            pdb_id=pdb_id)
        validacion_class = ValidarTipoClasificacion(
            clasifi_pdb=clasifi_pdb)
        if validacion_class == True:

            proteinas_abstract = {
                "text": j,
                "pdb_id": pdb_id,
                "clasificacion": clasifi_pdb,
            }
            proteinas.append(proteinas_abstract)

    if os.path.exists(f"{ruta_registro}/{index}.csv") == False:
        RegistrarProteinas(listado_proteinas=proteinas,
                           nombre_documento=f"{index}.csv", ruta=ruta_registro_procorpus)

    # Guardar Proteinas
    for datos_proteina in proteinas:
        pdb_id = datos_proteina["pdb_id"]
        ruta_proteina = f"{ruta_registro}/{pdb_id}.pdb"

        if os.path.exists(ruta_proteina) == False:
            GuardarProteina(pdb_id, ruta_registro)

    if os.path.exists(ruta_registro) == True:
        # Generar Pares
        carpeta_abs = os.listdir(ruta_registro)
        listado_pdb = [doc for doc in carpeta_abs if ".pdbqt" not in doc and ("_recH.pdb" not in doc) == True and (
            "_ligH.pdb" not in doc) == True and ("pares_proteinas.csv" not in doc) == True]
        pair_order_list = itertools.permutations(listado_pdb, 2)
        listado_par_filtrado = []
        info_registro = []
        for par in list(pair_order_list):
            par_prueba = (par[1], par[0])
            if (par_prueba in listado_par_filtrado) == False and (par in listado_par_filtrado) == False:
                listado_par_filtrado.append(par)
                info_ipp = {
                    "pro1_pdb": "",
                    "pro1_tamano": "",
                    "pro2_pdb": "",
                    "pro2_tamano": ""
                }
                with open(f"{ruta_registro}/{par[0]}", "r") as f:
                    info_ipp["pro1_pdb"] = par[0]
                    info_ipp["pro1_tamano"] = len(f.readlines())

                with open(f"{ruta_registro}/{par[1]}", "r") as f:
                    info_ipp["pro2_pdb"] = par[1]
                    info_ipp["pro2_tamano"] = len(f.readlines())

                info_registro.append(info_ipp)
        df_info_registro = pd.DataFrame(info_registro)
        ruta_info_registro = f"{ruta_registro}/pares_proteinas.csv"
        if os.path.exists(ruta_info_registro) == False:
            df_info_registro.to_csv(ruta_info_registro, index=False)

        # Seleccionar Pares

        try:
            for par in info_registro:
                if par["pro1_tamano"] < cantidad_min_res and par["pro2_tamano"] < cantidad_min_res:
                    info_ipp = {
                        "abstract": index,
                        "pro1_pdb": par["pro1_pdb"],
                        "pro1_tamano": par["pro1_tamano"],
                        "pro2_pdb": par["pro2_pdb"],
                        "pro2_tamano": par["pro2_tamano"]
                    }
                    listado_par.append(info_ipp)
        except:
            print(f"sin pares -> {ruta_info_registro}")

    return listado_par


def BuscarProteinasCorpus(text):

    ruta = "Documentos/ProteinasCorpus.json"

    with open(ruta) as f:
        words = json.load(f)

    pattern = re.compile(r'\b(' + '|'.join(words) + r')\b')

    matches = re.findall(pattern, text)
    matches = [i[0] for i in matches if len(i[0]) > 1]

    return matches


def BuscarUniprot(text: str):

    text = text.lower()

    ruta = "Documentos/ProteinasHumanasUniprotKBFiltradas.json"

    with open(ruta) as f:
        words = json.load(f)

    pattern = re.compile(r'\b(' + '|'.join(words) + r')\b')

    matches = re.findall(pattern, text)
    matches = [i[0] for i in matches if len(i[0]) > 1]

    return matches


def BuscarComplejosCorpus(text):

    ruta = "Documentos/DatosProfesora/Uniprot"

    obligate = f"{ruta}/ComplejosObligateNombresProteinas.json"
    transient = f"{ruta}/ComplejosTransientNombresProteinas.json"

    with open(obligate) as f:
        words = json.load(f)

    with open(transient) as f:
        words = words[0:149] + json.load(f)

    pattern = re.compile(r'\b(' + '|'.join(words) + r')\b')

    matches = re.findall(pattern, text)
    matches = [i[0] for i in matches if len(i[0]) > 1]

    return matches


def BuscarComplejosCorpus2(text: str):

    ruta = "Documentos/DatosProfesora/Uniprot"

    obligate = f"{ruta}/ComplejosObligateNombresProteinas.json"
    transient = f"{ruta}/ComplejosTransientNombresProteinas.json"

    text = text.split(" ")

    print(text)

    with open(obligate) as f:
        words = json.load(f)

    proteinas = []
    for proteina in words:
        proteina = proteina.lower()
        if proteina not in proteinas:
            proteinas.append(proteina)

    for palabra in text:
        for proteina in proteinas:
            if palabra in proteina:
                print("palabra :", palabra)
                print("proteina :", proteina)
                print("_______________")

    print(len(words))

    matches = []
    return matches


def CrearDiccionarioCustomNLTK(ruta_palabras: str):
    with open(ruta_palabras) as f:
        data = json.load(f)

    with open('custom_words.txt', 'w') as file:
        for i in data:
            file.write(f"{i}/NNP" + '\n')


def ExtraccionDatosCorpusEstandarXML(ruta: str):
    tree = ET.parse(ruta)
    root = tree.getroot()

    document = root.findall('document')
    all_sentence = []
    all_entity = []
    all_interaction = []

    for i in document:
        sentence = i.findall('sentence')
        for j in sentence:
            id_sentence = j.attrib['id']
            abstract = j.attrib['text']

            all_sentence.append({
                "id_sentence", id_sentence,
                "abstract", abstract
            })

            interaction = j.findall('interaction')
            all_interaction = all_interaction + \
                [{"id_sentence": id_sentence, "e1": ppi.attrib['e1'],
                    "e2": ppi.attrib['e2']} for ppi in interaction]
            if len(all_interaction) != 0:
                entity = j.findall('entity')
                all_entity = all_entity + \
                    [{"id": ent.attrib['id'], "text": ent.attrib['text']}
                        for ent in entity]

    return all_sentence, all_entity, all_interaction


def ExtraerProteinasCorpusEstandar(ruta: str):

    all_sentence, all_entity, all_interaction = ExtraccionDatosCorpusEstandarXML(
        ruta)

    ppi_id = []

    for ppi in all_interaction:
        ppi_id.append(ppi["e1"])
        ppi_id.append(ppi["e2"])

    proteinas = [str(entity["text"]).lower()
                 for entity in all_entity if entity["id"] in ppi_id]

    return proteinas


def FormatoInputBioc(abstract, id, ruta):
    xml = f"""<?xml version='1.0' encoding='utf-8' standalone='yes'?>
<collection>
	<source>PubMed</source>
	<date>1999-Jan-1</date>
	<key>collection.key</key>
	<document>
		<id>{id}</id>
		<passage>
            <infon key="type">title</infon>
            <offset>0</offset>
            <text></text>
        </passage>
        <passage>
            <infon key="type">abstract</infon>
            <offset>71</offset>
            <text>{abstract}</text>
        </passage>
	</document>
</collection>
"""
    xmlf = open(f"{ruta}", "w")
    xmlf.write(xml)
    xmlf.close()


def FormatoQueryPDB(query: str):
    query = query.replace(" ", "")
    query = query.replace("\n", "")
    query = query.replace("{", "%7B")
    query = query.replace(":", "%3A")
    query = query.replace(",", "%2C")
    query = query.replace("}", "%7D")
    return query


def GetPDBId(nombre: str):
    nombre_protein = nombre.replace(" ", "%20")

    api = "https://search.rcsb.org/rcsbsearch/v2/query?"

    query = api + 'json=%7B%22query%22%3A%7B%22type%22%3A%22terminal%22%2C'

    query = query + '%22label%22%3A%22full_text%22%2C%22service%22%3A%22full_text%22%2C'

    query = query + \
        f'%22parameters%22%3A%7B%22value%22%3A%22{nombre_protein}%22%7D%7D%2C%22'

    query = query + 'return_type%22%3A%22entry%22%2C%22request_options%22%3A%7B%22paginate'

    query = query + '%22%3A%7B%22start%22%3A0%2C%22rows%22%3A1%7D%2C%22scoring_strategy'

    query = query + '%22%3A%22combined%22%2C%22sort%22%3A%5B%7B%22sort_by%22%3A%22score'

    query = query + '%22%2C%22direction%22%3A%22desc%22%7D%5D%7D%7D'

    r = requests.get(url=query)
    proteina = ""

    try:
        if len(r.text) != 0:
            content = r.json()
            proteina = content["result_set"][0]["identifier"]
            return proteina
    except:
        return proteina


def BusquedaClasificacionProteina(pdb_id):
    query = f"https://files.rcsb.org/header/{pdb_id}.pdb"
    r = requests.get(url=query)
    content = r.text
    res = ""
    try:
        res = content.split("\n")[0][0:50].split("    ")[1]
    except:
        res = "ninguna"
    return res


def ValidarTipoClasificacion(clasifi_pdb):
    res = True
    clasifi_pdb = clasifi_pdb.lower()
    len_lista = len(
        [clasifi_pdb for i in clasificaciones_no_permitidas if i in clasifi_pdb])
    if len_lista != 0:
        res = False
    return res


def BusquedaProteinasSpacy(abstract, doc):
    proteinas = []
    ents = list(doc.ents)
    for ent in ents:
        if ent.label_ == "PROTEIN":
            proteinas.append(ent.text)

    return proteinas


def ExtraerProteinas(abstract, doc):
    res = True
    num_proteinas = 0
    proteinas = []

    ents = list(doc.ents)
    for ent in ents:
        if ent.label_ == "PROTEIN":
            pdb_id = GetPDBId(nombre=ent.text)
            clasifi_pdb = BusquedaClasificacionProteina(
                pdb_id=pdb_id)
            validacion_class = ValidarTipoClasificacion(
                clasifi_pdb=clasifi_pdb)
            inicio_t = abstract.find(ent.text)
            if validacion_class == True:

                proteinas_abstract = {
                    "text": ent.text,
                    "pdb_id": pdb_id,
                    "clasificacion": clasifi_pdb,
                    "inicio_t": inicio_t
                }
                proteinas.append(proteinas_abstract)
                num_proteinas += 1
    if num_proteinas <= 1:
        res = False

    return res, proteinas


def BusquedaProteinasPatrones(abstract, patron):
    proteinas_patron = re.findall(patron,
                                  abstract, flags=re.IGNORECASE)

    return proteinas_patron


def ExtraerProteinasPatrones(abstract, patron):
    res = True
    num_proteinas = 0
    proteinas = []

    proteinas_patron = re.findall(patron,
                                  abstract, flags=re.IGNORECASE)

    for ent in proteinas_patron:
        pdb_id = GetPDBId(nombre=ent)
        clasifi_pdb = BusquedaClasificacionProteina(
            pdb_id=pdb_id)
        validacion_class = ValidarTipoClasificacion(
            clasifi_pdb=clasifi_pdb)
        inicio_t = abstract.find(ent)
        if validacion_class == True:

            proteinas_abstract = {
                "text": ent,
                "pdb_id": pdb_id,
                "clasificacion": clasifi_pdb,
                "inicio_t": inicio_t
            }
            proteinas.append(proteinas_abstract)
            num_proteinas += 1
    if num_proteinas <= 1:
        res = False

    return res, proteinas


def RegistrarProteinas(listado_proteinas, nombre_documento, ruta):
    df = pd.DataFrame(listado_proteinas)
    df.to_csv(f"{ruta}/{nombre_documento}", index=False)


# Metodos Docking


def GuardarProteina(pdb_id, ruta_registro):
    if os.path.exists(ruta_registro) == False:
        os.mkdir(ruta_registro)

    query = f"https://files.rcsb.org/view/{pdb_id}.pdb"
    r = requests.get(url=query)
    content = r.text
    ruta_proteina = f"{ruta_registro}/{pdb_id}.pdb"
    with open(ruta_proteina, 'w') as out:
        out.write(content)
    return ruta_proteina

def GuardarProteinaFasta(pdb_id, ruta_registro):
    if os.path.exists(ruta_registro) == False:
        os.mkdir(ruta_registro)

    query = f"https://www.rcsb.org/fasta/entry/{pdb_id}/display"
    r = requests.get(url=query)
    content = r.text
    ruta_proteina = f"{ruta_registro}/{pdb_id}.fasta"
    with open(ruta_proteina, 'w') as out:
        out.write(content)
    return ruta_proteina


def ReducePDB(pdb_id, ruta_registro):
    return subprocess.run([f"cd {ruta_registro}; reduce {pdb_id}.pdb > {pdb_id}_recH.pdb; reduce {pdb_id}.pdb > {pdb_id}_ligH.pdb"],  shell=True)


def FormatoPDBQTReceptor(pdb_id, ruta_registro):
    return subprocess.run([f"cd {ruta_registro}; prepare_receptor -r {pdb_id}.pdb"],  shell=True)


def FormatoPDBQTLigando(pdb_id, ruta_registro):
    return subprocess.run([f"cd {ruta_registro}; prepare_ligand -l {pdb_id}.pdb"],  shell=True)


def FormatoPDBQTOpenBabel(pdb_id, ruta_registro):
    return subprocess.run([f"cd {ruta_registro}; obabel -i pdb {pdb_id}.pdb -o pdbqt -O {pdb_id}.pdbqt -p 7.4 -xr "],  shell=True)


def FormatoPDBQTOpenBabelTotal(ruta_registro, carpeta):
    return subprocess.run([f"cd {ruta_registro}/{carpeta}; obabel -i pdb *.pdb -o pdbqt -O *.pdbqt -p 7.4 -xr "],  shell=True)


def FormatoProteinaPDBQT(corpus, proteina):
    nombre_reduce = f"Docking/{corpus}/Proteinas/{proteina}.pdb"

    reduce = f"reduce {proteina}.pdb > {proteina}H.pdb"

    prepare_receptor = f"prepare_receptor -r {proteina}H.pdb"
    prepare_ligand = f"prepare_ligand -l {proteina}H.pdb"


def DockingAutodockVina(proteina1, proteina2):
    ruta_docking = ""


def CalcularRMSD(ruta_complex1, ruta_complex2):
    parser = PDBParser()
    pred_struct_1 = parser.get_structure('pred_1', ruta_complex1)
    pred_struct_2 = parser.get_structure('pred_2', ruta_complex2)

    sup = Superimposer()
    atoms1 = []
    atoms2 = []
    for model in pred_struct_1:
        for chain in model:
            for residue in chain:
                for atom in residue:
                    atoms1.append(atom)
    for model in pred_struct_2:
        for chain in model:
            for residue in chain:
                for atom in residue:
                    atoms2.append(atom)
    sup.set_atoms(atoms1, atoms2)
    sup.apply(pred_struct_2.get_atoms())

    rmsd = sup.rms

    return rmsd


def CalcularLogLR(rmsd):
    loglr = -math.log(1 + (rmsd**2 / 2))

    return round(loglr, 2)


def DescargarListadoComplejosPDB():
    query_url = "https://search.rcsb.org/rcsbsearch/v2/query?json="

    query_params = {
        "query": {
            "type": "group",
            "logical_operator": "and",
            "nodes": [
                {
                    "type": "terminal",
                    "service": "text",
                    "parameters": {
                        "attribute": "rcsb_entry_info.polymer_entity_count",
                        "operator": "greater_or_equal",
                        "value": 2
                    }
                }
            ]
        },
        "request_options": {
            "paginate": {
                "start": 0,
                "rows": 52040
            }
        },
        "return_type": "entry"
    }

    url_encoded_text = quote(str(query_params).replace("'", '"'))
    url = f"{query_url}{url_encoded_text}"
    response = requests.get(url)

    data = [i["identifier"] for i in response.json()["result_set"]]
    response_json = json.dumps(data)

    with open("ListadosComplejosPdb.json", "w") as outfile:
        outfile.write(response_json)


def DescargarListadoProteinNucleicAcidComplexes():
    query_url = "https://search.rcsb.org/rcsbsearch/v2/query?json="

    query_params = {
        "query": {
            "type": "group",
            "nodes": [
                {
                    "type": "group",
                    "logical_operator": "and",
                    "nodes": [
                        {
                            "type": "terminal",
                            "service": "text",
                            "parameters": {
                                "attribute": "rcsb_entry_info.selected_polymer_entity_types",
                                "operator": "exact_match",
                                "value": "Protein/NA"
                            }
                        }
                    ]
                }
            ],
            "logical_operator": "and"
        },
        "request_options": {
            "paginate": {
                "start": 0,
                "rows": 11487
            }
        },
        "return_type": "entry"
    }

    url_encoded_text = quote(str(query_params).replace("'", '"'))
    url = f"{query_url}{url_encoded_text}"
    response = requests.get(url)

    data = [i["identifier"] for i in response.json()["result_set"]]
    response_json = json.dumps(data)

    with open("Documentos/ListadoProteinNucleicAcidComplexes.json", "w") as outfile:
        outfile.write(response_json)


def MapeoPDBUniprot(pdb_id: str):
    pdb_id = pdb_id.lower()
    url = f"https://www.ebi.ac.uk/pdbe/api/mappings/uniprot/{pdb_id}"
    response = requests.get(url)
    if response.ok:
        data = response.json()
        if pdb_id in data:
            data = data[pdb_id]["UniProt"]
            data = [{"UniprotId": i, "PdbId": pdb_id, "Chain": data[f"{i}"]["mappings"][0]["chain_id"]}
                    for i in data]
            return data

    return None


def BusquedaBiogridParesUniprot(uniprot_id1, uniprot_id2):
    accesskey = "b324f18e0acf2122bc1829e75ab9c768"
    uniprot_concat = f"{uniprot_id1}|{uniprot_id2}"

    url = "https://webservice.thebiogrid.org/interactions"

    params = {
        "searchNames": "true",
        "includeInteractors": "true",
        "geneList": uniprot_concat,
        "additionalIdentifierTypes": "UNIPROTKB",
        "format": "jsonExtended",
        "accesskey": accesskey
    }

    response = requests.get(url=url, params=params)
    data = response.json()

    listado_filtro = []
    listado_pares = []

    for i in list(data):
        external_a = data[f"{i}"]["EXTERNALS_A"]
        external_a = [i for i in str(external_a).split(
            "|") if "SWISS-PROT" in i]
        if len(external_a) > 0:
            external_a = str(external_a[0]).split(":")[1]
        else:
            external_a = ""

        external_b = data[f"{i}"]["EXTERNALS_B"]
        external_b = [i for i in str(external_b).split(
            "|") if "SWISS-PROT" in i]
        if len(external_b) > 0:
            external_b = str(external_b[0]).split(":")[1]
        else:
            external_b = ""

        if (uniprot_id1 == external_a and uniprot_id2 == external_b) or (uniprot_id2 == external_a and uniprot_id1 == external_b) and i not in listado_filtro:
            datos_pares = {
                "BiogridId": i,
                "UniprotId1": uniprot_id1,
                "UniprotId2": uniprot_id2,
            }
            listado_pares.append(datos_pares)
            listado_filtro.append(i)

    return listado_pares