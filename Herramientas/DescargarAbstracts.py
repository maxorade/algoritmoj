import requests
import re
import xml.etree.ElementTree as ET
import time
import json
import os
import pandas as pd 
from Bio import Entrez

email = "yourname\@institute.edu"
tool = "GRATO"
db_name = "pubmed"
pmc_switch = "full_text"
base = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/'

RETMAX_ESEARCH =  1 #5000000  #10000000 
RETURN_RETMAX = 0
RETMAX_EFETCH = 1000

def ListadoIdsArticulosAbstractsQuerys(query: str = "complex", db: str = "pmc", index_inicio: int = 0, cantidad_maxima_articulos=100):
    url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"

    params = {
        "db": db,
        "term": query,
        "retstart": index_inicio,
        "retmax": cantidad_maxima_articulos
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        data = response.content
        tree = ET.fromstring(data)
        id_list = tree.findall( 'IdList' )[0]
        id_list =  id_list.findall('Id')

        listados_id = [ i.text for i in  id_list ]
        print(response.url)


        return listados_id
        # print(response.content)
    else:
        print(f'Error {response.status_code}')
        return []


def NombreProteina(ruta):
    with open(ruta, 'r') as f:
        contenido_total = f.read().split("\n")
        contenido = contenido_total[1].replace("TITLE     ", "").strip()
        if "TITLE    2" in contenido_total[2]:
            contenido = contenido + " " + contenido_total[2].replace("TITLE    2", "").strip()
        return contenido

def OrdenarInformacionComplejos(ruta_complejos, ruta_pdb, tipo,  ruta_registro):
    proteinas = []

    with open( ruta_complejos, 'r') as f:
        contenido = (f.read().split("\n"))
        for i in contenido:
            words = re.split(r'[ :]', i)
            if len(words) > 1 :
                proteina = words[0]
                chain1 = words[1]
                chain2 = words[2]
                id_complex = f"{proteina}_{chain1}_{chain2}"
                ruta_p = ruta_pdb + f"/{proteina}.pdb"
                if os.path.exists(ruta_p) == True:
                    nombre_proteina = NombreProteina(ruta_p)
                    tipo = tipo
                    data = {
                        "IdComplex":id_complex,
                        "Proteina":proteina,
                        "Chain1": chain1,
                        "Chain2": chain2,
                        "NombreProteina":nombre_proteina,
                        "Tipo":tipo
                    }
                    proteinas.append(data)
        
    df = pd.DataFrame(proteinas)

    df.to_csv(ruta_registro, index=False)

def FormateoParrafos(texto:str):
    texto = texto.replace("\n","")
    texto = re.sub(' {2,}', ' ', texto)
    return texto

def BuscarSecciones(parrafos, child):
    for j in child:
        if j.tag == "sec":
            BuscarSecciones(parrafos=parrafos,child=j)
        elif j.tag == "p":
            if j.text != None:
                parrafo =  FormateoParrafos(j.text)
                parrafos.append(parrafo)

def ExtraccionAbstractsXML(ruta):
    tree = ET.parse(ruta)
    root = tree.getroot()
    abstract = ""

    articulo = root[0].find( 'MedlineCitation' ).find("Article")
    if articulo != None:
        abstract =  articulo.find("Abstract")
        if abstract != None:
            abstract =  abstract.find("AbstractText").text
            abstract = abstract.replace('\n', '')
            abstract = re.sub(r'\s{2,}', ' ', abstract)
    

    return abstract


def ExtraccionParrafosXML(ruta):
    parrafos = []

    tree = ET.parse(ruta)
    root = tree.getroot()
    #############
    front = root[0].find( 'front' )

    abstracts = front.find("article-meta").findall("abstract")

    for i in abstracts:
        for child in i:
            if child.tag == "sec":
                BuscarSecciones(parrafos= parrafos, child= child)
            elif child.tag == "p":
                if child.text != None:
                    parrafo =  FormateoParrafos(child.text)
                    parrafos.append(parrafo)

    #############

    body = root[0].find( 'body' )

    if body != None:

        secciones  = body.findall( 'sec' )
    
        for i in secciones:
            for child in i:
                if child.tag == "sec":
                    BuscarSecciones(parrafos= parrafos, child= child)
                elif child.tag == "p":
                    if child.text != None:
                        parrafo =  FormateoParrafos(child.text)
                        parrafos.append(parrafo)


    # back = root[0].find( 'back' )
    return parrafos

def FormatoParrafoJSON(ruta_articulo):
    ruta =  "Documentos/DatosProfesora/"
    ruta_articulos = f"{ruta}Articulos/"

    parrafos_format = []
    parrafos =  ExtraccionParrafosXML(ruta_articulo)
        
    for index, parrafo in enumerate(parrafos):
        if len(parrafo) > 100:
            id_complex, id_pmc = ruta_articulo.replace(ruta_articulos,"").split("/")
            data = {
                "IdComplex": id_complex,
                "IdPMC": id_pmc,
                "Parrafo": index + 1,
                "Texto": parrafo
            }
            
            parrafos_format.append(data)

    return parrafos_format

def BusquedaCitasPubmedPDBId(pdb_id:str):

    api_url = f"https://data.rcsb.org/rest/v1/core/entry/{pdb_id}"
    response = requests.get(api_url)
    data = response.json()

    citaciones =  []

    if "rcsb_primary_citation" in data:
        citaciones.append(data["rcsb_primary_citation"]["pdbx_database_id_pub_med"])

    if "citation" in data:
        for i in data["citation"]:
            if "pdbx_database_id_pub_med" in i: 
                if  i["pdbx_database_id_pub_med"] not in citaciones:
                    citaciones.append(i["pdbx_database_id_pub_med"])


    return citaciones


def DescargarXMLPubmed(id, ruta):
    Entrez.email = "jonathan.gatica@gmail.com"
    fetch = Entrez.efetch(db='pubmed', rettype='Medline', retmode='xml', id=id)
    article = fetch.read()

    root = ET.fromstring(article)
    tree = ET.ElementTree(root)
    tree.write(f"{ruta}/{id}.xml")

def DescargarXMLPMC(id, ruta):
    Entrez.email = "jonathan.gatica@gmail.com"
    fetch = Entrez.efetch(db='pmc', resetmode='xml', id=id, rettype='full')
    article = fetch.read()

    root = ET.fromstring(article)
    tree = ET.ElementTree(root)
    tree.write(f"{ruta}/{id}.xml")

def ListadoPMCIds(termino):
    url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pmc&term={termino}"
    response = requests.get(url)
    data = response.content
    tree = ET.fromstring(data)
    id_list = tree.findall( 'IdList' )[0]
    id_list =  id_list.findall('Id')

    listados_pmc_id = [ i.text for i in  id_list ]

    return listados_pmc_id

def BusquedaAbstractsPubmed(query):
    path = "abstract_query_" + query 
    path =  path.replace(" ", "_")

    if os.path.isdir(path) != True:
        os.mkdir(path) 

    abst_error = {
        "error":[],
        "descargados":0
    }
    bmi = 0
    listado_abstracts = list()
    register = f"&email={email}&tool={tool}"
    protein_query = ""
    protein_query = query
    protein_query = protein_query.replace(' ', '%20')
    url = base + \
        f"esearch.fcgi?db={db_name}&term={protein_query}&usehistory=y&field=word"
    url = url + register  
    url = url + f"&retmax={RETMAX_ESEARCH}"
    url = url + "&datetype=dcom"



    r = requests.get(url=url)
    output = r.content

    print(url)

    if len(re.findall("error", str(output))) == 0:
  
        tree = ET.fromstring(output)

        web = tree.find("WebEnv").text
        key = tree.find("QueryKey").text
        count = tree.find("Count").text

        print(f"Cantidad de articulos {count}" )
        
        grupos = int(count)/100
        grupos =  round(grupos, 0)

        print(f"Grupos de descarga {grupos}" )

        print("Articulos descargados:")

        for i in range(int(grupos)):
            m = i * 100
            
            rettype = "abstract"  # pubmed
            retmode = "text"
            RETMAX_EFETCH = 100
            url = base + \
                f"efetch.fcgi?db={db_name}&query_key={key}&WebEnv={web}"
            url = url + f"&retstart={m}"
            url = url + f"&retmax={RETMAX_EFETCH}"
            url = url + f"&rettype={rettype}&remode={retmode}"

            try:
                r = requests.get(url=url)
                data = r.content
                tree = ET.fromstring(data)

                article_array = tree.findall("PubmedArticle")

                if(article_array != None):
                    for article in article_array:
                        abst = article.find("MedlineCitation").find(
                            "Article").find("Abstract")
                        pmid = article.find("MedlineCitation").find(
                            "PMID")
                        if abst != None:
                            try:
                                
                                abst = abst.find("AbstractText").text
                                RET_FILE = open(f"{path}/{pmid.text}.txt", "w")
                                RET_FILE.write(abst)
                                RET_FILE.close()
                                bmi = bmi + 1
                                print(bmi, end='\r')
                                time.sleep(2)
                            except:
                                abst_error["error"].append(pmid.text)
            except:
                print("error de conexión")

    abst_error["descargados"] = bmi
    with open('errores_prueba_folder.json', 'w') as f:
        json.dump(abst_error, f)

    return bmi

# print(f'Total articulos descargados: {BusquedaAbstractsPubmed(query="protein complex")}')