# Librerías
import Herramientas.FuncionesClasificadorCombiClasificadores as combi_ac
import Herramientas.FuncionesClasificadorCombiRepresentaciones as combi_rd
import Herramientas.FuncionesClasificadorLexicon as rd_lexicon
import Herramientas.FuncionesClasificadorTFIDF as rd_tfidf
import Herramientas.FuncionesClasificadorWemb as rd_wemb

from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from joblib import dump
import os
import random
import sys
import time
import numpy as np
import pandas as pd
from Transformer.Vector_Denso import DenseTransformer
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.VectorEncondeSentence import VectorEncondeSentence
import Transformer.FuncionesTransformers as ft
from Transformer.Tokenizador import Tokenizador
import warnings
warnings.filterwarnings('ignore')

# from Transformer.PadSequencesTransformer import PadSequencesTransformer
# import Herramientas.FuncionesModelos as fm
 

def SelecionarClasificador(nombre_clas, formato, complementos):
    # Lexicon
    if nombre_clas == "NB" and formato == "LEXICON":
        return rd_lexicon.ClasificadorNaiveBayesLexicon(complementos=complementos)
    if nombre_clas == "SVM" and formato == "LEXICON":
        return rd_lexicon.ClasificadorSVMLexicon(complementos=complementos)
    if nombre_clas == "RF" and formato == "LEXICON":
        return rd_lexicon.ClasificadorRandomForestLexicon(complementos=complementos)
    if nombre_clas == "KNN" and formato == "LEXICON":
        return rd_lexicon.ClasificadorKNeighborsLexicon(complementos=complementos)

    # TFIDF
    if nombre_clas == "NB" and formato == "TFIDF":
        return rd_tfidf.ClasificadorNaiveBayesTFIDF(complementos=complementos)
    if nombre_clas == "SVM" and formato == "TFIDF":
        return rd_tfidf.ClasificadorSVMTFIDF(complementos=complementos)
    if nombre_clas == "RF" and formato == "TFIDF":
        return rd_tfidf.ClasificadorRandomForestTFIDF(complementos=complementos)
    if nombre_clas == "KNN" and formato == "TFIDF":
        return rd_tfidf.ClasificadorKNeighborsTFIDF(complementos=complementos)

    # TFIDF + Lexicon
    if nombre_clas == "NB" and formato == "TFIDF_LEXICON":
        return combi_rd.ClasificadorNaiveBayesTFIDFLexicon(complementos=complementos)
    if nombre_clas == "SVM" and formato == "TFIDF_LEXICON":
        return combi_rd.ClasificadorSVMTFIDFLexicon(complementos=complementos)
    if nombre_clas == "RF" and formato == "TFIDF_LEXICON":
        return combi_rd.ClasificadorRandomForestTFIDFLexicon(complementos=complementos)
    if nombre_clas == "KNN" and formato == "TFIDF_LEXICON":
        return combi_rd.ClasificadorKNeighborsTFIDFLexicon(complementos=complementos)

    # WordEmbedding
    if nombre_clas == "NB" and formato == "WEMB":
        return rd_wemb.ClasificadorNaiveBayesWordEmbedding(complementos=complementos)
    if nombre_clas == "SVM" and formato == "WEMB":
        return rd_wemb.ClasificadorSVMWordEmbedding(complementos=complementos)
    if nombre_clas == "RF" and formato == "WEMB":
        return rd_wemb.ClasificadorRandomForestWordEmbedding(complementos=complementos)
    if nombre_clas == "KNN" and formato == "WEMB":
        return rd_wemb.ClasificadorKNeighborsWordEmbedding(complementos=complementos)

    # WordEmbedding + Lexicon
    if nombre_clas == "NB" and formato == "WEMB_LEXICON":
        return combi_rd.ClasificadorNaiveBayesWordEmbeddingLexicon(complementos=complementos)
    if nombre_clas == "SVM" and formato == "WEMB_LEXICON":
        return combi_rd.ClasificadorSVMWordEmbeddingLexicon(complementos=complementos)
    if nombre_clas == "RF" and formato == "WEMB_LEXICON":
        return combi_rd.ClasificadorRandomForestWordEmbeddingLexicon(complementos=complementos)
    if nombre_clas == "KNN" and formato == "WEMB_LEXICON":
        return combi_rd.ClasificadorKNeighborsWordEmbeddingLexicon(complementos=complementos)
    

     # WordEmbedding + TFIDF
    if nombre_clas == "NB" and formato == "WEMB_TFIDF":
        return combi_rd.ClasificadorNaiveBayesWordEmbeddingTFIDF(complementos=complementos)
    if nombre_clas == "SVM" and formato == "WEMB_TFIDF":
        return combi_rd.ClasificadorSVMWordEmbeddingTFIDF(complementos=complementos)
    if nombre_clas == "RF" and formato == "WEMB_TFIDF":
        return combi_rd.ClasificadorRandomForestWordEmbeddingTFIDF(complementos=complementos)
    if nombre_clas == "KNN" and formato == "WEMB_TFIDF":
        return combi_rd.ClasificadorKNeighborsWordEmbeddingTFIDF(complementos=complementos)


    # WordEmbedding + Lexicon + TFIDF
    if nombre_clas == "NB" and formato == "WEMB_LEXICON_TFIDF":
        return combi_rd.ClasificadorNaiveBayesWordEmbeddingLexiconTFIDF(complementos=complementos)
    if nombre_clas == "SVM" and formato == "WEMB_LEXICON_TFIDF":
        return combi_rd.ClasificadorSVMWordEmbeddingLexiconTFIDF(complementos=complementos)
    if nombre_clas == "RF" and formato == "WEMB_LEXICON_TFIDF":
        return combi_rd.ClasificadorRandomForestWordEmbeddingLexiconTFIDF(complementos=complementos)
    if nombre_clas == "KNN" and formato == "WEMB_LEXICON_TFIDF":
        return combi_rd.ClasificadorKNeighborsWordEmbeddingLexiconTFIDF(complementos=complementos)

    

    # Multiple TFIDF + Lexicon +  (TFIDF + Lexicon)

    if nombre_clas == "NB" and formato == "MULTIPLE_TFIDF":
        return combi_ac.ClasificadorNBMultipleRepresentacionTFIDF(complementos=complementos)

    if nombre_clas == "SVM" and formato == "MULTIPLE_TFIDF":
        return combi_ac.ClasificadorSVMMultipleRepresentacionTFIDF(complementos=complementos)

    if nombre_clas == "RF" and formato == "MULTIPLE_TFIDF":
        return combi_ac.ClasificadorRFMultipleRepresentacionTFIDF(complementos=complementos)

    if nombre_clas == "KNN" and formato == "MULTIPLE_TFIDF":
        return combi_ac.ClasificadorKNNMultipleRepresentacionTFIDF(complementos=complementos)

    # Multiple WEMB + Lexicon +  (WEMB + Lexicon)

    if nombre_clas == "NB" and formato == "MULTIPLE_WEMB":
        return combi_ac.ClasificadorNBMultipleRepresentacionWordEmbedding(complementos)

    if nombre_clas == "SVM" and formato == "MULTIPLE_WEMB":
        return combi_ac.ClasificadorSVMMultipleRepresentacionWordEmbedding(complementos)

    if nombre_clas == "RF" and formato == "MULTIPLE_WEMB":
        return combi_ac.ClasificadorRFMultipleRepresentacionWordEmbedding(complementos)

    if nombre_clas == "KNN" and formato == "MULTIPLE_WEMB":
        return combi_ac.ClasificadorKNNMultipleRepresentacionWordEmbedding(complementos)
    
    
    # Ensemble

    # Lexicon
    
    if nombre_clas == "SVM_NB_RF" and formato == "LEXICON":
        return combi_ac.ClasificadorSVMNBRFLexicon(complementos=complementos)

    if nombre_clas == "SVM_KNN_RF" and formato == "LEXICON":
        return combi_ac.ClasificadorSVMKNNRFLexicon(complementos=complementos)

    if nombre_clas == "KNN_NB_RF" and formato == "LEXICON":
        return combi_ac.ClasificadorKNNNBRFLexicon(complementos=complementos)

    if nombre_clas == "SVM_NB_KNN" and formato == "LEXICON":
        return combi_ac.ClasificadorSVMNBKNNLexicon(complementos=complementos)

    # TFIDF
    if nombre_clas == "SVM_NB_RF" and formato == "TFIDF":
        return combi_ac.ClasificadorSVMNBRFTFIDF(complementos=complementos)

    if nombre_clas == "SVM_KNN_RF" and formato == "TFIDF":
        return combi_ac.ClasificadorSVMKNNRFTFIDF(complementos=complementos)

    if nombre_clas == "KNN_NB_RF" and formato == "TFIDF":
        return combi_ac.ClasificadorKNNNBRFTFIDF(complementos=complementos)

    if nombre_clas == "SVM_NB_KNN" and formato == "TFIDF":
        return combi_ac.ClasificadorSVMNBKNNTFIDF(complementos=complementos)

    # TFIDF + Lexicon
    if nombre_clas == "SVM_NB_RF" and formato == "TFIDF_LEXICON":
        return combi_ac.ClasificadorSVMNBRFTFIDFLexicon(complementos=complementos)

    if nombre_clas == "SVM_KNN_RF" and formato == "TFIDF_LEXICON":
        return combi_ac.ClasificadorSVMKNNRFTFIDFLexicon(complementos=complementos)

    if nombre_clas == "KNN_NB_RF" and formato == "TFIDF_LEXICON":
        return combi_ac.ClasificadorKNNNBRFTFIDFLexicon(complementos=complementos)

    if nombre_clas == "SVM_NB_KNN" and formato == "TFIDF_LEXICON":
        return combi_ac.ClasificadorSVMNBKNNTFIDFLexicon(complementos=complementos)

    # WordEmbedding
    
    if nombre_clas == "SVM_NB_RF" and formato == "WEMB":
        return combi_ac.ClasificadorSVMNBRFWordEmbedding(complementos=complementos)

    if nombre_clas == "SVM_KNN_RF" and formato == "WEMB":
        return combi_ac.ClasificadorSVMKNNRFWordEmbedding(complementos=complementos)

    if nombre_clas == "KNN_NB_RF" and formato == "WEMB":
        return combi_ac.ClasificadorKNNNBRFWordEmbedding(complementos=complementos)

    if nombre_clas == "SVM_NB_KNN" and formato == "WEMB":
        return combi_ac.ClasificadorSVMNBKNNWordEmbedding(complementos=complementos)


    # WordEmbedding + Lexicon

    if nombre_clas == "SVM_NB_RF" and formato == "WEMB_LEXICON":
        return combi_ac.ClasificadorSVMNBRFWordEmbeddingLexicon(complementos=complementos)

    if nombre_clas == "SVM_KNN_RF" and formato == "WEMB_LEXICON":
        return combi_ac.ClasificadorSVMKNNRFWordEmbeddingLexicon(complementos=complementos)

    if nombre_clas == "KNN_NB_RF" and formato == "WEMB_LEXICON":
        return combi_ac.ClasificadorKNNNBRFWordEmbeddingLexicon(complementos=complementos)

    if nombre_clas == "SVM_NB_KNN" and formato == "WEMB_LEXICON":
        return combi_ac.ClasificadorSVMNBKNNWordEmbeddingLexicon(complementos=complementos)
    




def ReporteResultado(y_test, y_pred, carpeta, nombre_res, tiempo_inicio):
    if os.path.exists(f"Resultados/{carpeta}") == False:
        os.mkdir(f"Resultados/{carpeta}")

    precision, recall, f1 = EvaluarPrecisionRecallF1(y_test,y_pred)

    acc = f"Accuracy : {accuracy_score(y_test, y_pred )} \n"
    recall = f"Recall   : {recall} \n"
    prec = f"Precision: {precision} \n"
    f1 = f"F1 Score : {f1} \n"
    print(acc)
    print(recall)
    print(prec)
    print(f1)
    end = time.time()
    tiempo = f"Tiempo   : {end - tiempo_inicio} \n"
    print(tiempo)
    with open(f"Resultados/{carpeta}/{nombre_res}.txt", 'w') as out:
        out.write(acc + recall + prec + f1 + tiempo)


def EvaluarModelo(y, y_pred, ruta_modelo, nombre_modelo, nombre_corpus):
    acc = f"Accuracy : {accuracy_score(y, y_pred)} \n"
    recall = f"Recall   : {recall_score(y, y_pred, average='weighted')} \n"
    prec = f"Precision: {precision_score(y, y_pred, average='weighted')} \n"
    f1 = f"F1 Score : {f1_score(y, y_pred, average='weighted')} \n"
    print(acc)
    print(recall)
    print(prec)
    print(f1)

    # if os.path.exists(f"{ruta_modelo}/Evaluacion") == False:
    #     os.mkdir(f"{ruta_modelo}/Evaluacion")

    # with open(f"{ruta_modelo}/Evaluacion/{nombre_modelo}_{nombre_corpus}.txt", 'w') as out:
    #     out.write(acc + recall + prec + f1 )


def GuardarModelo(clf, carpeta, nombre_res):
    if os.path.exists(f"Resultados/{carpeta}/Modelo") == False:
        os.mkdir(f"Resultados/{carpeta}/Modelo")
    dump(clf, f"Resultados/{carpeta}/Modelo/{nombre_res}_Model.joblib")


def EvaluarPrecisionRecallF1(y_true,y_pred):
    # Calculate the number of true positives, false positives, and false negatives
    true_positives = sum([1 for i in range(len(y_true)) if y_true[i] == 1 and y_pred[i] == 1])
    true_negatives = sum([1 for i in range(len(y_true)) if y_true[i] == 0 and y_pred[i] == 0])
    false_positives = sum([1 for i in range(len(y_true)) if y_true[i] == 0 and y_pred[i] == 1])
    false_negatives = sum([1 for i in range(len(y_true)) if y_true[i] == 1 and y_pred[i] == 0])

    print(true_positives)
    print(true_negatives)
    print(false_positives)
    print(false_negatives)

    # Calculate precision score
    if true_positives + false_positives == 0:
        precision = 0
    else:
        precision = true_positives / (true_positives + false_positives)

    # Calculate recall score
    if true_positives + false_negatives == 0:
        recall = 0
    else:
        recall = true_positives / (true_positives + false_negatives)

    # Calculate F1 score
    if precision + recall == 0:
        f1 = 0
    else:
        f1 = 2 * (precision * recall) / (precision + recall)

    return  precision, recall, f1
