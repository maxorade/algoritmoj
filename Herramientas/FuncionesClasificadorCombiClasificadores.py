# Librerías
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from joblib import dump
import os
import random
import sys
import time
import numpy as np
import pandas as pd
from Transformer.Vector_Denso import DenseTransformer
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.VectorEncondeSentence import VectorEncondeSentence
import Transformer.FuncionesTransformers as ft
from Transformer.Tokenizador import Tokenizador
import warnings
warnings.filterwarnings('ignore')


# Ensemble

# SVM + NB + RF

def ClasificadorSVMNBRFWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + RF")
    print("Representación de documentos: WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf

def ClasificadorSVMNBRFWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + RF")
    print("Representación de documentos: WordEmbedding + Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBRFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + RF")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]

    SVM = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBRFTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + RF")
    print("Representación de documentos: TFIDF")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBRFTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + RF")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf

# SVM + KNN + RF

def ClasificadorSVMKNNRFWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + KNN + RF")
    print("Representación de documentos: WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
         ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('SVM', SVC(kernel='linear'))])

    KNN = Pipeline([
         ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('K Neighbors', KNeighborsClassifier())])

    RF = Pipeline([
         ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('KNN', KNN), ('RF', RF)], voting='hard'))
    ])
    return clf

def ClasificadorSVMKNNRFWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + KNN + RF")
    print("Representación de documentos: WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
         ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    KNN = Pipeline([
         ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    RF = Pipeline([
         ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('KNN', KNN), ('RF', RF)], voting='hard'))
    ])
    return clf



def ClasificadorSVMKNNRFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + KNN + RF")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]

    SVM = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))])

    KNN = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())])

    RF = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('KNN', KNN), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorSVMKNNRFTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + KNN + RF")
    print("Representación de documentos: TFIDF")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('SVM', SVC(kernel='linear'))])

    KNN = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('K Neighbors', KNeighborsClassifier())])

    RF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('KNN', KNN), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorSVMKNNRFTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + KNN + RF")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    KNN = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    RF = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('KNN', KNN), ('RF', RF)], voting='hard'))
    ])
    return clf

# KNN + NB + RF


def ClasificadorKNNNBRFWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: KNN + NB + RF")
    print("Representación de documentos:  WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    KNN = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('K Neighbors', KNeighborsClassifier())])

    NB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN', KNN), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf

def ClasificadorKNNNBRFWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: KNN + NB + RF")
    print("Representación de documentos:  WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    KNN = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN', KNN), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf

def ClasificadorKNNNBRFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: KNN + NB + RF")
    print("Representación de documentos:  Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]

    KNN = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())])

    NB = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN', KNN), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorKNNNBRFTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: KNN + NB + RF")
    print("Representación de documentos: TFIDF")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    KNN = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('K Neighbors', KNeighborsClassifier())])

    NB = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN', KNN), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf


def ClasificadorKNNNBRFTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: KNN + NB + RF")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    KNN = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    RF = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier(n_estimators=80))])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN', KNN), ('NB', NB), ('RF', RF)], voting='hard'))
    ])
    return clf

# SVM + NB + KNN

def ClasificadorSVMNBKNNWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + KNN")
    print("Representación de documentos: WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    KNN = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('K Neighbors', KNeighborsClassifier())])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('KNN', KNN)], voting='hard'))
    ])
    return clf

def ClasificadorSVMNBKNNWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + KNN")
    print("Representación de documentos: WordEmbedding")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    KNN = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('KNN', KNN)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBKNNLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + KNN")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]

    SVM = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    KNN = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('KNN', KNN)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBKNNTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + KNN")
    print("Representación de documentos: TFIDF")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    KNN = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
        )),
        ('K Neighbors', KNeighborsClassifier())])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('KNN', KNN)], voting='hard'))
    ])
    return clf


def ClasificadorSVMNBKNNTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM + NB + KNN")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))])

    NB = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())])

    KNN = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            )),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM', SVM), ('NB', NB), ('KNN', KNN)], voting='hard'))
    ])
    return clf




# Combinacion diferentes representaciones de documento
# MULTIPLE_TFIDF/MULTIPLE_WEMB

# NB


def ClasificadorNBMultipleRepresentacionTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad + (TFIDF + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    NB_TFIDF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())
    ])

    NB_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Naive Bayes', GaussianNB())
    ])

    NB_TFIDF_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('NB_TFIDF', NB_TFIDF), ('NB_LEXICON', NB_LEXICON), ('NB_TFIDF_LEXICON', NB_TFIDF_LEXICON)], voting='hard'))
    ])
    return clf


# SVM
def ClasificadorSVMMultipleRepresentacionTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad + (TFIDF + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    SVM_TFIDF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
        ('SVM', SVC(kernel='linear'))
    ])

    SVM_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))
    ])

    SVM_TFIDF_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM_TFIDF', SVM_TFIDF), ('SVM_LEXICON', SVM_LEXICON), ('SVM_TFIDF_LEXICON', SVM_TFIDF_LEXICON)], voting='hard'))
    ])
    return clf

# RF


def ClasificadorRFMultipleRepresentacionTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad + (TFIDF + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    RF_TFIDF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
        ('Random Forest', RandomForestClassifier())
    ])

    RF_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier())
    ])

    RF_TFIDF_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('RF_TFIDF', RF_TFIDF), ('RF_LEXICON', RF_LEXICON), ('RF_TFIDF_LEXICON', RF_TFIDF_LEXICON)], voting='hard'))
    ])
    return clf

# KNN


def ClasificadorKNNMultipleRepresentacionTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Neighbors")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad + (TFIDF + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    KNN_TFIDF = Pipeline([
        ('TFIDF', TfidfVectorizer(
            tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 1))),
        ('K Neighbors', KNeighborsClassifier())
    ])

    KNN_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())
    ])

    KNN_TFIDF_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN_TFIDF', KNN_TFIDF), ('KNN_LEXICON', KNN_LEXICON), ('KNN_TFIDF_LEXICON', KNN_TFIDF_LEXICON)], voting='hard'))
    ])
    return clf

# MULTIPLE_WEMB

# NB


def ClasificadorNBMultipleRepresentacionWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: WordEmbedding + Lexicon de Intensidad + (WordEmbedding + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    NB_WEMB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Naive Bayes', GaussianNB())
    ])

    NB_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Naive Bayes', SVC(kernel='linear'))
    ])

    NB_WEMB_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Naive Bayes', GaussianNB())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('NB_WEMB', NB_WEMB), ('NB_LEXICON', NB_LEXICON), ('NB_WEMB_LEXICON', NB_WEMB_LEXICON)], voting='hard'))
    ])

    return clf

# SVM


def ClasificadorSVMMultipleRepresentacionWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: SVM")
    print("Representación de documentos: WordEmbedding + Lexicon de Intensidad + (WordEmbedding + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    SVM_WEMB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('SVM', SVC(kernel='linear'))
    ])

    SVM_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))
    ])

    SVM_WEMB_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('SVM_WEMB', SVM_WEMB), ('SVM_LEXICON', SVM_LEXICON), ('SVM_WEMB_LEXICON', SVM_WEMB_LEXICON)], voting='hard'))
    ])

    return clf

# RF


def ClasificadorRFMultipleRepresentacionWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: WordEmbedding + Lexicon de Intensidad + (WordEmbedding + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    RF_WEMB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Random Forest', RandomForestClassifier())
    ])

    RF_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier())
    ])

    RF_WEMB_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('RF_WEMB', RF_WEMB), ('RF_LEXICON', RF_LEXICON), ('RF_WEMB_LEXICON', RF_WEMB_LEXICON)], voting='hard'))
    ])
    return clf

# KNN


def ClasificadorKNNMultipleRepresentacionWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Neighbors")
    print("Representación de documentos: WordEmbedding + Lexicon de Intensidad + (WordEmbedding + Lexicon de Intensidad)")
    print("###########################################")
    nlp = complementos[0]
    representacionVectorial = complementos[1]

    KNN_WEMB = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('K Neighbors', KNeighborsClassifier())
    ])

    KNN_LEXICON = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())
    ])

    KNN_WEMB_LEXICON = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])

    clf = Pipeline([
        ('Clasificador', VotingClassifier(estimators=[
         ('KNN_WEMB', KNN_WEMB), ('KNN_LEXICON', KNN_LEXICON), ('KNN_WEMB_LEXICON', KNN_WEMB_LEXICON)], voting='hard'))
    ])
    return clf