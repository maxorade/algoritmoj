# Librerías
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from joblib import dump
import os
import random
import sys
import time
import numpy as np
import pandas as pd
from Transformer.Vector_Denso import DenseTransformer
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.VectorEncondeSentence import VectorEncondeSentence
import Transformer.FuncionesTransformers as ft
from Transformer.Tokenizador import Tokenizador
import warnings
warnings.filterwarnings('ignore')



### TFIDF + Lexicon


def ClasificadorNaiveBayesTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])
    return clf


def ClasificadorRandomForestTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])
    return clf


def ClasificadorKNeighborsTFIDFLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: TFIDF + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3))),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])
    return clf


### WordEmbedding + Lexicon

def ClasificadorNaiveBayesWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])
    return clf


def ClasificadorRandomForestWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])
    return clf


def ClasificadorKNeighborsWordEmbeddingLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])
    return clf



### WordEmbedding + TFIDF

def ClasificadorNaiveBayesWordEmbeddingTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: Word Embedding + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMWordEmbeddingTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: Word Embedding + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])
    return clf


def ClasificadorRandomForestWordEmbeddingTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: Word Embedding + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])
    return clf


def ClasificadorKNeighborsWordEmbeddingTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: Word Embedding + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])
    return clf

### WordEmbedding + Lexicon + TFIDF


def ClasificadorNaiveBayesWordEmbeddingLexiconTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('Denso', DenseTransformer(activar=True)),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMWordEmbeddingLexiconTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('SVM', SVC(kernel='linear'))
    ])
    return clf


def ClasificadorRandomForestWordEmbeddingLexiconTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('Random Forest', RandomForestClassifier())
    ])
    return clf


def ClasificadorKNeighborsWordEmbeddingLexiconTFIDF(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: Word Embedding + Lexicon de Intensidad + TFIDF")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    tokenizador = Tokenizador(nlp=nlp, tipo="spacy")

    clf = Pipeline([
        ('Datos', FeatureUnion([
            ('WordEmbedding', VectorWordEmbedding(
                nlp=nlp, representacionVectorial=representacionVectorial)),
            ('Lexicon', VectorLexicon(nlp=nlp)),
            ('TFIDF', TfidfVectorizer(
                tokenizer=tokenizador.SelecionarTokenizador, max_df=1.0, ngram_range=(1, 3)
            ))
        ])),
        ('K Neighbors', KNeighborsClassifier())
    ])
    return clf