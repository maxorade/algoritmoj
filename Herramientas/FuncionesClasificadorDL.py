import os
import time
import Transformer.FuncionesTransformers as ft
import torch
import numpy as np

from Transformer.VectorEncondeSentence import VectorEncondeSentence
from transformers import BertTokenizer, logging
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
import warnings
import sklearn.exceptions

warnings.filterwarnings("ignore", category=sklearn.exceptions.UndefinedMetricWarning)
##############################

def FormatearInput(X, y, complementos):
    clasificador = complementos[0]
    if clasificador == "BERT":
        # return FormatearInputBERT(X, y, complementos)
        return FormatearInputBERT2(X, y, complementos)
    else:
        return FormatearInputWEMB(X, y, complementos)

def FormatearInputWEMB(X, y, complementos):
    nlp = complementos[1]
    counts = complementos[3]
    X = VectorEncondeSentence(counts=counts, nlp=nlp).fit_transform(X)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    train_ds = ft.FormatoDataset(X=X_train, Y=y_train)
    test_ds = ft.FormatoDataset(X=X_test, Y=y_test)

    batch_size = 350

    train_dl = DataLoader(train_ds, batch_size=batch_size, shuffle=True)
    test_dl = DataLoader(test_ds, batch_size=batch_size)

    return train_dl, test_dl


def FormatearInputBERT(X, y, complementos):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    nombre_modelo_pre = 'dmis-lab/biobert-base-cased-v1.1'
    # nombre_modelo_pre = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext'
    tokenizer = BertTokenizer.from_pretrained(nombre_modelo_pre)
    batch_size = 5000
    # max_len = 60
    max_len = 60
    train_ds = ft.FormatoDatasetBERT(
        abstracts=X_train, clasificaciones=y_train, tokenizer=tokenizer, max_len=max_len)
    test_ds = ft.FormatoDatasetBERT(
        abstracts=X_test, clasificaciones=y_test, tokenizer=tokenizer, max_len=max_len)

    train_dl = DataLoader(train_ds, batch_size=batch_size, num_workers=4)
    test_dl = DataLoader(test_ds, batch_size=batch_size, num_workers=4)

    return train_dl, test_dl

def FormatearInputBERT2(X, y, complementos):
    # Inicialización
    RANDOM_SEED = 42
    MAX_LEN = 400
    BATCH_SIZE = 350
    # DATASET_PATH = '/content/drive/My Drive/videos/2020-07-20/BERT_sentiment_IMDB_Dataset.csv'
    NCLASSES = 2
    np.random.seed(RANDOM_SEED)
    torch.manual_seed(RANDOM_SEED)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    nombre_modelo_pre = 'dmis-lab/biobert-base-cased-v1.1'
    PRE_TRAINED_MODEL_NAME = 'dmis-lab/biobert-base-cased-v1.1'
    # nombre_modelo_pre = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext'
    tokenizer = BertTokenizer.from_pretrained(nombre_modelo_pre)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    train_data_loader = ft.BERTDataLoader(X_train, y_train , tokenizer, MAX_LEN, BATCH_SIZE)
    test_data_loader = ft.BERTDataLoader(X_test, y_test, tokenizer, MAX_LEN, BATCH_SIZE)
    
    return train_data_loader, test_data_loader


def BERTWordEmbedding(complementos):
    logging.set_verbosity_error()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    n_clases = 2
    nombre_modelo_pre = 'dmis-lab/biobert-base-cased-v1.1'
    # nombre_modelo_pre = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext'
    model = ft.MBERT(n_clases=n_clases, nombre_modelo_pre=nombre_modelo_pre)
    model = model.to(device)
    return model


def LSTMWordEmbedding(complementos):
    representacionVectorial = complementos[2]
    counts = complementos[3]

    vocab_size = len(counts) + 2

    pretrained_weights, vocab, vocab2index = ft.EmbMatrix(
        pretrained=representacionVectorial, word_counts=counts)

    model = ft.MLSTM(vocab_size, 200, 200, pretrained_weights)

    return model

def GRUWordEmbedding(complementos):
    representacionVectorial = complementos[2]
    counts = complementos[3]

    vocab_size = len(counts) + 2

    pretrained_weights, vocab, vocab2index = ft.EmbMatrix(
        pretrained=representacionVectorial, word_counts=counts)

    model = ft.MGRU(vocab_size, 200, 200, pretrained_weights)

    return model


def CNNWordEmbedding(complementos):
    representacionVectorial = complementos[2]
    counts = complementos[3]

    vocab_size = len(counts) + 2

    pretrained_weights, vocab, vocab2index = ft.EmbMatrix(
        pretrained=representacionVectorial, word_counts=counts)

    model = ft.MCNN2(vocab_size=vocab_size, embedding_dim=200,
                     hidden_dim=200, glove_weights=pretrained_weights)

    return model


def EntrenamientoReporteResultado(clf, train_data, test_data, clasificador, epochs, carpeta, nombre_res, tiempo_inicio):
    if clasificador == "BERT":
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        epochs = 1
        val_acc, val_prec, val_recall, val_f1 =  ft.BERTEntrenamiento(clf,train_data, test_data ,device, epochs)
    else:  
        val_acc, val_prec, val_recall, val_f1 = ft.TrainModel(
            model=clf, train_dl=train_data, val_dl=test_data, clasificador=clasificador, epochs=epochs, lr=0.1)

    acc = f"Accuracy : {val_acc} \n"
    recall = f"Recall   : {val_recall} \n"
    prec = f"Precision: {val_prec} \n"
    f1 = f"F1 Score : {val_f1} \n"
    print(acc)
    print(recall)
    print(prec)
    print(f1)
    end = time.time()
    tiempo = f"Tiempo   : {end - tiempo_inicio} \n"
    print(tiempo)
    with open(f"Resultados/{carpeta}/{nombre_res}.txt", 'w') as out:
        out.write(acc + recall + prec + f1 + tiempo)


def GuardarModelo(clf, carpeta, nombre_res):
    if os.path.exists(f"Resultados/{carpeta}/Modelo") == False:
        os.mkdir(f"Resultados/{carpeta}/Modelo")
    torch.save(clf, f"Resultados/{carpeta}/Modelo/{nombre_res}_Model.pt")

# Extras


def SelecionarClasificador(nombre_clas, formato, complementos):
    if nombre_clas == "LSTM" and formato == "WEMB":
        return LSTMWordEmbedding(complementos)
    if nombre_clas == "GRU" and formato == "WEMB":
        return GRUWordEmbedding(complementos)
    if nombre_clas == "CNN" and formato == "WEMB":
        return CNNWordEmbedding(complementos)
    if nombre_clas == "BERT" and formato == "WEMB":
        return BERTWordEmbedding(complementos)
        
