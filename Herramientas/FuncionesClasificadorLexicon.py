# Librerías
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from joblib import dump
import os
import random
import sys
import time
import numpy as np
import pandas as pd
from Transformer.Vector_Denso import DenseTransformer
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.VectorEncondeSentence import VectorEncondeSentence
import Transformer.FuncionesTransformers as ft
from Transformer.Tokenizador import Tokenizador
import warnings
warnings.filterwarnings('ignore')




def ClasificadorNaiveBayesLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]

    clf = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]

    clf = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('SVM', SVC(kernel='linear'))
    ])

    return clf


def ClasificadorRandomForestLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]

    clf = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('Random Forest', RandomForestClassifier())
    ])

    return clf


def ClasificadorKNeighborsLexicon(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: Lexicon de Intensidad")
    print("###########################################")

    nlp = complementos[0]

    clf = Pipeline([
        ('Lexicon', VectorLexicon(nlp=nlp)),
        ('K Neighbors', KNeighborsClassifier())
    ])

    return clf
