# Librerías
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from joblib import dump
import os
import random
import sys
import time
import numpy as np
import pandas as pd
from Transformer.Vector_Denso import DenseTransformer
from Transformer.Word_Embedding import VectorWordEmbedding
from Transformer.Vector_Lexicon import VectorLexicon
from Transformer.VectorEncondeSentence import VectorEncondeSentence
import Transformer.FuncionesTransformers as ft
from Transformer.Tokenizador import Tokenizador
import warnings
warnings.filterwarnings('ignore')


def ClasificadorNaiveBayesWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Naive Bayes")
    print("Representación de documentos: Word Embedding")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Naive Bayes', GaussianNB())
    ])
    return clf


def ClasificadorSVMWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Maquina de Soporte Vectorial")
    print("Representación de documentos: Word Embedding")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('SVM', SVC(kernel='linear'))
    ])
    return clf


def ClasificadorRandomForestWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: Random Forest")
    print("Representación de documentos: Word Embedding")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('Random Forest', RandomForestClassifier())
    ])
    return clf


def ClasificadorKNeighborsWordEmbedding(complementos):
    print("###########################################")
    print("Modelo:  ")
    print("Clasificador: K Vecinos más cercanos")
    print("Representación de documentos: Word Embedding")
    print("###########################################")

    nlp = complementos[0]
    representacionVectorial = complementos[1]
    clf = Pipeline([
        ('WordEmbedding', VectorWordEmbedding(
            nlp=nlp, representacionVectorial=representacionVectorial)),
        ('K Neighbors', KNeighborsClassifier())
    ])
    return clf

