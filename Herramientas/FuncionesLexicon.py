import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from itertools import combinations
from tqdm import tqdm
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from string import punctuation
interaccion_lexicon = pd.read_csv(
    'Documentos/LexiconPositivo.csv', sep=',', encoding='latin-1')
interaccion_lexiconPalabras = np.asarray(interaccion_lexicon['Palabras'])
interaccion_lexiconPalabras = interaccion_lexiconPalabras.ravel()
no_palabras = list(punctuation) 
no_palabras.extend(['¿', '¡'])


def Tokenizador(doc, nlp):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    return [x.orth_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras)]


def  ValidarPalabrasClaves(doc):
    res =  True
    palabras_claves = []
    for palabra in doc:
        if str(palabra.lemma_) in interaccion_lexiconPalabras:
            palabras_claves.append(palabra.lemma_)
    if len(palabras_claves) == 0:
        res = False
    
    return res, palabras_claves 

def RegistrarPalabrasClaves(listado_palabras, nombre_documento, ruta):
    df =  pd.DataFrame(listado_palabras)
    df.to_csv(f"{ruta}/{nombre_documento}", index=False) 


def ObtenerInformacion(corpus, listado_proteinas, listado_palabras):
    listado_abstract = []

    for index in tqdm(range(len(list(corpus["Abstract"])))):
        abstract = corpus["Abstract"][index]
        f_prot = listado_proteinas[index]
        f_palabras = listado_palabras[index]

        temp = combinations(f_prot["text"]  , 2)

        for i in list(temp):
            index_prot1 = abstract.find(i[0])
            if i[0] == i[1]:
                index_prot2 = abstract.find(i[0], abstract.find(i[1]) + 1)
            else:
                index_prot2 = abstract.find(i[1])

                if index_prot2 < index_prot1:
                    index_prot2 = abstract.find(i[1], abstract.find(i[1]) + 1)
            segmento =  abstract
            segmento =  segmento.replace(i[0],"PROT1")
            if i[0] == i[1]:
                index_p = segmento.find("PROT1", segmento.find("PROT1") + 1)
                segmento = segmento[0:index_p]
                segmento = segmento + "PROT2"
            segmento =  segmento.replace(i[1],"PROT2")
            if len(segmento) > 50:
                datos = {
                    "Index": index,
                    "Abstract": segmento,
                    "Prot1":i[0],
                    "Prot2":i[1],
                    "Numero_palabras":len(f_palabras),
                    "Clasificacion":corpus["Clasificacion"][index]
                }

                listado_abstract.append(datos)
    df = pd.DataFrame(listado_abstract)
    return df

def BolsaPalabras(listado):
    vectorizer = CountVectorizer()
    total_palabras = [' '.join([i[0].lower() for i in listado ])]
    bag = vectorizer.fit_transform(total_palabras)

    df_bag = pd.DataFrame(
        bag.toarray(), columns=vectorizer.get_feature_names_out()
    )
    list_bag = { f"{i}":int(df_bag[i]) for i in df_bag  }
    return list_bag

# Funciones para crear Lexicon

def NormalizeData(data):
    return  round((data - np.min(data)) / (np.max(data) - np.min(data))*100, 2)

def DicicionarioPalabrasFrecuenciaInversa(listado):
  tfidf_vectorizer = TfidfVectorizer(ngram_range=[1, 1])


  total_palabras = [' '.join([i[0] for i in listado ])]
  tfidf = tfidf_vectorizer.fit_transform(total_palabras)
  df_tfidf = pd.DataFrame(
    tfidf.toarray(), columns=tfidf_vectorizer.get_feature_names_out()
  )
  list_tfidf = [{ "palabra":i,"porcentaje":float(df_tfidf[i])} for i in df_tfidf  ]
  df_tfidf_list = pd.DataFrame(list_tfidf)
  
  scaled_x = NormalizeData(df_tfidf_list.porcentaje)
  for index,value in enumerate(df_tfidf_list.porcentaje):
      df_tfidf_list.porcentaje[index]  = scaled_x[index]
  
  return df_tfidf_list

def EmbeddingLemmaPalabras(lexicon_csv, jnlpba, embedder):
  palabras = []

  for i in lexicon_csv["palabras"]:
    if i not in palabras:
      palabras.append(i)
  print("Numero de palabras", len(palabras))

  palabras_lemma = []
  for i in palabras:
    word = jnlpba(i)
    word_lema = [j.lemma_ for  j in  word][0]
    if word_lema not in palabras_lemma:
      palabras_lemma.append(word_lema)
  print("Numero de palabras lemma", len(palabras_lemma))

  corpus_embeddings = embedder.encode(palabras_lemma)
  return corpus_embeddings , palabras_lemma


def InerciaClusters(corpus_embeddings):
  inercias = []
  for k in range(2, 10):
      kmeans = KMeans(n_clusters=k).fit(corpus_embeddings)    
      inercias.append(kmeans.inertia_)

  plt.figure(figsize=(6, 5), dpi=100)
  plt.scatter(range(2, 10), inercias, marker="o", s=180, color="purple")
  plt.xlabel("Número de Clusters", fontsize=25)
  plt.ylabel("Inercia", fontsize=25)
  plt.show()



def CrearClusters(num_clusters,corpus_embeddings, palabras_lemma ):
  clustering_model = KMeans(n_clusters=num_clusters)

  clustering_model.fit(corpus_embeddings)

  cluster_assignment = clustering_model.labels_

  clustered_sentences = [[] for i in range(num_clusters)]
  for sentence_id, cluster_id in enumerate(cluster_assignment):
      clustered_sentences[cluster_id].append(palabras_lemma[sentence_id])

  for i, cluster in enumerate(clustered_sentences):
      print("Cluster ", i+1)
      print(cluster)
      print("")
  return clustered_sentences



def CrearLexicon(clusters,puntajes, nombre_lexicon):
  lexicon = []

  for index,  value in enumerate(clusters):
    grupo =  (index +  1 )
    porcentaje = puntajes[index]
    for i in value:
      datos = {
          "Palabras": i,
          "Intensidad": porcentaje
      }

      lexicon.append(datos)

  df = pd.DataFrame(lexicon)
  df.head()

  df.to_csv(nombre_lexicon, index = False)