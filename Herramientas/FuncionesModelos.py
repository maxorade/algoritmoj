from keras.utils import np_utils
from keras.layers.core import Dense, Dropout, Activation, Lambda
from keras.preprocessing.text import Tokenizer
from collections import defaultdict
from keras.layers.convolutional import Convolution1D
from keras import backend as K

from keras.models import Sequential
from keras.layers import Embedding, LSTM, Dense


def max_1d(X):
    return K.max(X, axis=1)

def ClasificadorCNN(max_features=1000,
                    nb_filter=250,
                    filter_length=3,
                    hidden_dims=250,
                    nb_epoch=2,
                    nb_classes=5):

    model = Sequential()
    model.add(Embedding(max_features, 128, dropout=0.2))
    model.add(Convolution1D(nb_filter=nb_filter,
                            filter_length=filter_length,
                            border_mode='valid',
                            activation='relu',
                            subsample_length=1))

    model.add(Lambda(max_1d, output_shape=(nb_filter,)))
    model.add(Dense(hidden_dims))
    model.add(Dropout(0.2))
    model.add(Activation('relu'))
    model.add(Dense(nb_classes))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model


def CrearModeloLSTM(embedding_input_dim, embedding_output_dim, embedding_weights):
    model = Sequential([
        Embedding(input_dim=embedding_input_dim,
                  output_dim=embedding_output_dim,
                  weights=[embedding_weights],
                  trainable=False,
                  mask_zero=True),
        LSTM(128),
        Dense(1, activation='sigmoid')
    ])
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',  metrics=['accuracy'])
    return model