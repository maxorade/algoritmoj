# Librerías
import itertools
import os
import joblib
import spacy
from tqdm import tqdm
import sys
import numpy as np
import pandas as pd
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl


#prueba spacy

# python ImplementacionAJ.py FiltroProteinas 0 500 spacy
# python ImplementacionAJ.py UsoModelo 0 500 spacy
# python ImplementacionAJ.py RegistroProteinas 0 500 spacy
# python ImplementacionAJ.py GenerarPDBQT 0 500 spacy openbabel

# python ImplementacionAJ.py GenerarParesProteinasPDB 0 500 spacy 
# python ImplementacionAJ.py SeleccionarParesProteinasPDB 0 500 spacy 

###############

#python ImplementacionAJ.py FiltroProteinas 500 1000 spacy &
#python ImplementacionAJ.py FiltroProteinas 500 1000 patrones &

# python ImplementacionAJ.py UsoModelo 500 1000 spacy &
# python ImplementacionAJ.py RegistroProteinas 500 1000 spacy &

# python ImplementacionAJ.py GenerarParesProteinasPDB 500 1000 spacy &
# python ImplementacionAJ.py SeleccionarParesProteinasPDB 500 1000 spacy &


#python ImplementacionAJ.py FiltroProteinas 1000 1500 spacy &
# python ImplementacionAJ.py UsoModelo 1000 1500 spacy &
# python ImplementacionAJ.py RegistroProteinas 1000 1500 spacy &
# python ImplementacionAJ.py GenerarParesProteinasPDB 1000 1500 spacy &
# python ImplementacionAJ.py SeleccionarParesProteinasPDB 1000 1500 spacy &


#python ImplementacionAJ.py FiltroProteinas 1500 2000 spacy &
# python ImplementacionAJ.py UsoModelo 1500 2000 spacy &
# python ImplementacionAJ.py RegistroProteinas 1500 2000 spacy &
# python ImplementacionAJ.py GenerarParesProteinasPDB 1500 2000 spacy &
# python ImplementacionAJ.py SeleccionarParesProteinasPDB 1500 2000 spacy &


#python ImplementacionAJ.py FiltroProteinas 2000 2500 spacy &
# python ImplementacionAJ.py UsoModelo 2000 2500 spacy &
# python ImplementacionAJ.py RegistroProteinas 2000 2500 spacy &
# python ImplementacionAJ.py GenerarParesProteinasPDB 2000 2500 spacy &
# python ImplementacionAJ.py SeleccionarParesProteinasPDB 2000 2500 spacy &

#prueba patrones

# python ImplementacionAJ.py FiltroProteinas 0 500 patrones
# python ImplementacionAJ.py UsoModelo 0 500 patrones
# python ImplementacionAJ.py RegistroProteinas 0 500 patrones
# python ImplementacionAJ.py GenerarPDBQT 0 500 patrones

# Main
if __name__ == "__main__":

    print("Inicio Proceso")
    opcion = sys.argv[1]
    inicio = int(sys.argv[2])
    termino = int(sys.argv[3])
    buscador_p = sys.argv[4]

    carpeta_abs = os.listdir("Corpus/Abstracts")
    listado_abs = carpeta_abs[inicio:termino]
    ruta_filtro = f"Documentos/Abstracts/{inicio}_{termino}"

    if opcion == "FiltroProteinas":
        if os.path.exists(ruta_filtro) == False:
            os.mkdir(ruta_filtro)
            os.mkdir(f"{ruta_filtro}/Proteinas")
            os.mkdir(f"{ruta_filtro}/ProteinasPatrones")
            os.mkdir(f"{ruta_filtro}/Palabras")
            os.mkdir(f"{ruta_filtro}/PalabrasPatrones")

        jnlpba = spacy.load('en_ner_jnlpba_md')
        if buscador_p == "patrones":
            with open('Documentos/PatronProteinasSwissProt.txt') as f:
                patron_swiss = f.readlines()
            patron_swiss = patron_swiss[0]

        corpus_filtrado = []
        for i in tqdm(range(len(list(listado_abs)))):
            index = listado_abs[i]
            id_abs = index.replace(".txt","")
            with open(f"Corpus/Abstracts/{index}") as f:
                abstract = f.readlines()
            abstract = abstract[0]
            doc = jnlpba(abstract)
            if buscador_p == "spacy":
                ruta_proteinas = f"{ruta_filtro}/Proteinas"
                ruta_palabras_lexicon = f"{ruta_filtro}/Palabras"
                buscar_proteinas = bp.ExtraerProteinas(
                    abstract=abstract, doc=doc)
                if buscar_proteinas[0] == False:
                    continue
            if buscador_p == "patrones":
                ruta_proteinas = f"{ruta_filtro}/ProteinasPatrones"
                ruta_palabras_lexicon = f"{ruta_filtro}/PalabrasPatrones"
                buscar_proteinas = bp.ExtraerProteinasPatrones(
                    abstract=abstract, patron=patron_swiss)
                if buscar_proteinas[0] == False:
                    continue

            buscar_palabras_claves = fl.ValidarPalabrasClaves(doc=doc)
            if buscar_palabras_claves[0] == False:
                continue

            nombre_doc = id_abs

            bp.RegistrarProteinas(
                listado_proteinas=buscar_proteinas[1], nombre_documento=nombre_doc, ruta=ruta_proteinas)
            fl.RegistrarPalabrasClaves(
                listado_palabras=buscar_palabras_claves[1], nombre_documento=nombre_doc, ruta=ruta_palabras_lexicon)
            datos_filtro = {
                "Id": id_abs,
                "Abstract": abstract
            }
            corpus_filtrado.append(datos_filtro)

        corpus_filtrado = pd.DataFrame(corpus_filtrado)
        if buscador_p == "spacy":
            ruta_corpus_filtrado = f"{ruta_filtro}/{inicio}_{termino}.csv"
        if buscador_p == "patrones":
            ruta_corpus_filtrado = f"{ruta_filtro}/{inicio}_{termino}Patrones.csv"

        corpus_filtrado.to_csv(ruta_corpus_filtrado, index=False)

    if opcion == "UsoModelo":
        if os.path.exists(ruta_filtro) == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"{ruta_filtro}/{inicio}_{termino}.csv"
                nombre_corpus_filtrado = f"{inicio}_{termino}.csv"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"{ruta_filtro}/{inicio}_{termino}Patrones.csv"
                nombre_corpus_filtrado = f"{inicio}_{termino}Patrones.csv"

            corpus = pd.read_csv(ruta_corpus_filtrado)

            ids = np.asarray(corpus["Id"])
            ids = ids.ravel()

            X = np.asarray(corpus["Abstract"])
            X = X.ravel()
            
            ruta_modelo = "Resultados/NB/Modelo/"
            nombre_modelo = "AImedNB_LEXICON_Model"
            modelo = joblib.load(f'{ruta_modelo}/{nombre_modelo}.joblib')

            y_pred = modelo.predict(X)

            listado_pred = [{"Index": ids[index] , "Abstract": X[index]}
                            for index, val in enumerate(y_pred) if val == 1]
            df = pd.DataFrame(data=listado_pred)

            ruta_docking = f"Docking/Abstracts/{inicio}_{termino}"

            if os.path.exists(ruta_docking) == False:
                os.mkdir(ruta_docking)
                os.mkdir(f"{ruta_docking}/Proteinas")
                os.mkdir(f"{ruta_docking}/ProteinasPatrones")

            df.to_csv(
                f'{ruta_docking}/{nombre_corpus_filtrado}', index=False)

    if opcion == "RegistroProteinas":
        ruta_docking = f"Docking/Abstracts/{inicio}_{termino}"
        if os.path.exists(ruta_filtro) == True and os.path.exists(ruta_filtro) == True:
            if buscador_p == "spacy":
                ruta_corpus_filtrado = f"{ruta_docking}/{inicio}_{termino}.csv"
                ruta_proteinas = f"{ruta_filtro}/Proteinas"
                ruta_registro_proteinas = f"{ruta_docking}/Proteinas"
            if buscador_p == "patrones":
                ruta_corpus_filtrado = f"{ruta_docking}/{inicio}_{termino}Patrones.csv"
                ruta_proteinas = f"{ruta_filtro}/ProteinasPatrones"
                ruta_registro_proteinas = f"{ruta_docking}/ProteinasPatrones"

            print("Cargar Proteinas")
            try:
                corpus = pd.read_csv(ruta_corpus_filtrado)
                for index in tqdm(range(len(list(corpus["Abstract"])))):
                    id_index = corpus["Index"][index]

                    proteina_abs = pd.read_csv(
                        f"{ruta_proteinas}/{id_index}")
                    ruta_registro = f"{ruta_registro_proteinas}/{id_index}"

                    for datos_proteina in proteina_abs.values.tolist():
                        pdb_id = datos_proteina[1]
                        ruta_proteina = f"{ruta_registro}/{pdb_id}.pdb"
                        if os.path.exists(ruta_proteina) == False:
                            bp.GuardarProteina(pdb_id, ruta_registro)
            except:
                print("######## No se encontraron proteinas ########")

    if opcion == "GenerarPDBQT":
        ruta_docking = f"Docking/Abstracts/{inicio}_{termino}"
        if os.path.exists(ruta_filtro) == True and os.path.exists(ruta_filtro) == True:
            if buscador_p == "spacy":
                ruta_registro_proteinas = f"{ruta_docking}/Proteinas"
            if buscador_p == "patrones":
                ruta_registro_proteinas = f"{ruta_docking}/ProteinasPatrones"

            print("Inicio Generar PDBQT")

            carpeta_abs = os.listdir(ruta_registro_proteinas)
            
            for carpeta in carpeta_abs:
                bp.FormatoPDBQTOpenBabel(ruta_registro_proteinas,carpeta)

    if opcion == "GenerarParesProteinasPDB":
        ruta_docking = f"Docking/Abstracts/{inicio}_{termino}"
        if os.path.exists(ruta_filtro) == True and os.path.exists(ruta_filtro) == True:
            if buscador_p == "spacy":
                ruta_registro = f"{ruta_docking}/Proteinas"
            if buscador_p == "patrones":
                ruta_registro = f"{ruta_docking}/ProteinasPatrones"

            carpeta_proteinas = os.listdir(ruta_registro)

            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                carpeta_abs = os.listdir(ruta_abs)
                listado_pdb = [doc for doc in carpeta_abs if ".pdbqt" not in doc and ("_recH.pdb" not in doc ) == True and ("_ligH.pdb" not in doc ) == True  ]
                pair_order_list = itertools.permutations(listado_pdb, 2) 
                listado_par_filtrado = []
                info_registro = []
                for par in list(pair_order_list):
                    par_prueba = (par[1], par[0])
                    if (par_prueba in listado_par_filtrado ) == False and (par in listado_par_filtrado ) == False:
                        listado_par_filtrado.append(par)
                        info_ipp = {
                            "pro1_pdb":"",
                            "pro1_tamano":"",
                            "pro2_pdb":"",
                            "pro2_tamano":""
                        }
                        with open(f"{ruta_abs}/{par[0]}","r") as f: 
                            info_ipp["pro1_pdb"] = par[0]
                            info_ipp["pro1_tamano"] = len(f.readlines())

                        with open(f"{ruta_abs}/{par[1]}","r") as f: 
                            info_ipp["pro2_pdb"] = par[1]
                            info_ipp["pro2_tamano"] = len(f.readlines())

                        info_registro.append(info_ipp)
                df_info_registro = pd.DataFrame(info_registro)
                ruta_info_registro = f"{ruta_abs}/pares_proteinas.csv"
                df_info_registro.to_csv(ruta_info_registro, index=False)

            
    if opcion == "SeleccionarParesProteinasPDB":
        cantidad_min_res = 3000
        ruta_docking = f"Docking/Abstracts/{inicio}_{termino}"
        if os.path.exists(ruta_filtro) == True and os.path.exists(ruta_filtro) == True:
            if buscador_p == "spacy":
                ruta_registro = f"{ruta_docking}/Proteinas"
                ruta_seleccionar = f"{ruta_docking}/{inicio}_{termino}_paresdock_spacy.csv"
            if buscador_p == "patrones":
                ruta_registro = f"{ruta_docking}/ProteinasPatrones"
                ruta_seleccionar = f"{ruta_docking}/{inicio}_{termino}_paresdock_patrones.csv"

            carpeta_proteinas = os.listdir(ruta_registro)
            listado_par = []
            for doc_proteina in tqdm(range(len(list(carpeta_proteinas)))):
                ruta_abs = f"{ruta_registro}/{carpeta_proteinas[doc_proteina]}"
                ruta_info_registro = f"{ruta_abs}/pares_proteinas.csv"
                
                print(ruta_info_registro) 
                try: 
                    pares_prot = pd.read_csv(ruta_info_registro)
                    for par in pares_prot.values.tolist():
                        if par[1] < cantidad_min_res and  par[3] < cantidad_min_res:
                            info_ipp = {
                                "abstract":carpeta_proteinas[doc_proteina],
                                "pro1_pdb":par[0],
                                "pro1_tamano":par[1],
                                "pro2_pdb":par[2],
                                "pro2_tamano":par[3]
                            }
                            listado_par.append(info_ipp)

                except:
                    print(f"sin pares -> {ruta_info_registro}")
                
            df_info_registro = pd.DataFrame(listado_par)
            df_info_registro.to_csv(ruta_seleccionar, index=False)
                
