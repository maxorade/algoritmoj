from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

import numpy as np

import keras

from keras.models import Model
from keras.layers import Dense, Activation, concatenate, Embedding, Input

from keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences


print("cargar corpus ....")

import pandas as pd
ruta_corpus_filtrado = f"Documentos/Biocreative3/Biocreative3.csv"
corpus = pd.read_csv(ruta_corpus_filtrado)

X = np.asarray(corpus[["Abstract"]])
X = X.ravel()
y = np.asarray(corpus[["Clasificacion"]])
y = y.ravel()


X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.33, random_state=42)


vectorizer = TfidfVectorizer(max_features=300)
vectorizer = vectorizer.fit(X_train)

df_train = vectorizer.transform(X_train)

tokenizer = Tokenizer()
tokenizer.fit_on_texts(X_train)

maxlen = 50

sequences_train = tokenizer.texts_to_sequences(X_train)
sequences_train = pad_sequences(sequences_train, maxlen=maxlen)

print(df_train.shape )



print(len(sequences_train[0]))
print(sequences_train.shape)


##### modelo

vocab_size = len(tokenizer.word_index) + 1

print(vocab_size)

embedding_size = 300

input_tfidf = Input(shape=(300,))
input_text = Input(shape=(maxlen,))

# # # vocab_size 23731
# # # embedding_size 300
# # # maxlen  50

embedding = Embedding(vocab_size, embedding_size, input_length=maxlen)(input_text)

# this averaging method taken from:
# https://stackoverflow.com/a/54217709/1987598

mean_embedding = keras.layers.Lambda(lambda x: keras.backend.mean(x, axis=1))(embedding)

concatenated = concatenate([input_tfidf, mean_embedding])

dense1 = Dense(256, activation='relu')(concatenated)
dense2 = Dense(32, activation='relu')(dense1)
dense3 = Dense(8, activation='sigmoid')(dense2)

model = Model(inputs=[input_tfidf, input_text], outputs=dense3)

model.summary()

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])




model.fit(df_train, y_train, epochs=1)



# from scikeras.wrappers import KerasClassifier

# my_model = KerasClassifier(
#                model=model,
#                epochs=1
# )


# from sklearn.pipeline import Pipeline


# pipeline = Pipeline([
#               ('model', my_model)
# ])


# pipeline.fit(X_train, y_train)
# y_pred = pipeline.predict(X_test)

# print("resultados ....")

# from sklearn.metrics import (
#     classification_report,
#     accuracy_score,
#     f1_score,
#     precision_score,
#     recall_score,
# )


# acc = f"Accuracy : {accuracy_score(y_test, y_pred)} \n"
# recall = f"Recall   : {recall_score(y_test, y_pred)} \n"
# prec = f"Precision: {precision_score(y_test, y_pred)} \n"
# f1 = f"F1 Score : {f1_score(y_test, y_pred)} \n"
# print(acc)
# print(recall)
# print(prec)
# print(f1)

# with open(f"RED_TFIDF.txt", 'w') as out:
#     out.write(acc + recall + prec + f1 )