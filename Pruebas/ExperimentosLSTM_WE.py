from keras.preprocessing.text import Tokenizer
from sklearn.base import TransformerMixin, BaseEstimator

class TokenizerTransformer(BaseEstimator, TransformerMixin, Tokenizer):

    def __init__(self, **tokenizer_params):
        Tokenizer.__init__(self, **tokenizer_params)

    def fit(self, X, y=None):
        self.fit_on_texts(X)
        return self

    def transform(self, X, y=None):
        X_transformed = self.texts_to_sequences(X)
        return X_transformed


#####


from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.base import TransformerMixin, BaseEstimator

class PadSequencesTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, maxlen):
        self.maxlen = maxlen

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X_padded = pad_sequences(X, maxlen=self.maxlen)
        return X_padded
    

#############


from keras.models import Sequential
from keras.layers import Embedding, LSTM, Dense


def create_model(embedding_input_dim, embedding_output_dim, embedding_weights):
    model = Sequential([
        Embedding(input_dim=embedding_input_dim,
                  output_dim=embedding_output_dim,
                  weights=[embedding_weights],
                  trainable=False,
                  mask_zero=True),
        LSTM(128),
        Dense(1, activation='sigmoid')
    ])
    model.compile(loss='binary_crossentropy', optimizer='adam')
    return model


#############

print("Inicio_proceso")

from gensim.models import KeyedVectors
word_vectors = KeyedVectors.load_word2vec_format('Transformer/Embeddings/PubMed-w2v.bin', binary=True)


#####

print("embedding_weights ...")

import numpy as np

embedding_weights = np.vstack([
           np.zeros(word_vectors.vectors.shape[1]),
           word_vectors.vectors
])

print( "embedding_weights.shape ", embedding_weights.shape)

##########

print("vocab_size ...")

vocab_size = len(word_vectors.key_to_index.keys()) + 1

print("vocab_size ", vocab_size)


###########

EMBEDDING_DIM = 200

print("cargar corpus ....")

import pandas as pd
from sklearn.model_selection import train_test_split

ruta_corpus_filtrado = f"Documentos/Biocreative3/Biocreative3.csv"
corpus = pd.read_csv(ruta_corpus_filtrado)

X = np.asarray(corpus[["Abstract"]])
X = X.ravel()
y = np.asarray(corpus[["Clasificacion"]])
y = y.ravel()

##############

longest_text = corpus["Abstract"].str.split().str.len().max()

print("longest_text ", longest_text)


############

print("crear modelo ....")

from scikeras.wrappers import KerasClassifier
from sklearn.pipeline import Pipeline 

num_epochs = 1 #100

my_tokenizer = TokenizerTransformer()
my_padder = PadSequencesTransformer(maxlen=longest_text)
my_model = KerasClassifier(
               model=create_model, 
               epochs=num_epochs,
               embedding_input_dim=vocab_size,
               embedding_output_dim=EMBEDDING_DIM,
               embedding_weights=embedding_weights
)

pipeline = Pipeline([
              ('tokenizer', my_tokenizer),
              ('padder', my_padder),
              ('model', my_model)
])

################


print("split corpus y entrenamiento ...")

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.33, random_state=42)

pipeline.fit(X_train, y_train)
y_pred = pipeline.predict(X_test)

##################

print("resultados ....")

from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)


acc = f"Accuracy : {accuracy_score(y_test, y_pred)} \n"
recall = f"Recall   : {recall_score(y_test, y_pred)} \n"
prec = f"Precision: {precision_score(y_test, y_pred)} \n"
f1 = f"F1 Score : {f1_score(y_test, y_pred)} \n"
print(acc)
print(recall)
print(prec)
print(f1)


print("guardar modelo ...")
from joblib import dump

dump(pipeline, f"LSTM_Model.joblib")