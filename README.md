# AlgoritmoJ

# Corpus utilizados
AIMED
Biocreative 3
Corpus Completo (AImed, BioInfer, LLL, IEPA y HPRD50 )

`wget http://tttran.net/mirror/PubMed-w2v.txt.gz`
`wget http://evexdb.org/pmresources/vec-space-models/PubMed-and-PMC-w2v.bin &`
`wget http://evexdb.org/pmresources/vec-space-models/PubMed-w2v.bin`

`wget https://www.ncbi.nlm.nih.gov/CBBresearch/Lu/Demo/tmTools/download/GNormPlus/GNormPlusJava.zip`
unzip GNormPlusJava.zip -d 
chmod +x configure
.\configure
make 


`gunzip PubMed-w2v.txt.gz`


# Sistema Operativo

La aplición debe ser ejecutada en ubuntu o utilizar WSL o Docker


# Requerimientos

Python 3.9.12 and anaconda


Crear ambiente:

`conda create --name algoritmoj python=3.9`

`conda activate algoritmoj`

nltk.download('stopwords')


Habilitar Jupyter notebook

`conda install -y jupyter`

`conda install nb_conda`

`python -m ipykernel install --user --name algoritmoj --display-name "Python 3.9 (algoritmoj)"`

`jupyter notebook`



Instalar requerimientos CPU:

`pip install -r requerimientos.txt`
`conda install -c anaconda tensorflow`
`conda install pytorch -c pytorch`
`conda install -c conda-forge skorch`

Instalar requerimientos CPU/GPU:

`pip install -r requerimientos_gpu.txt`
`conda install -c anaconda tensorflow-gpu`
`conda install pytorch cudatoolkit -c pytorch`
`conda install -c conda-forge skorch`



# Algoritmos Clasificadores

RF = RANDOM FOREST

NB = NAIVE BAYES

SVM = MAQUINA DE SOPORTE VECTORIAL

KNN = K VECINOS MÁS CERCANOS

CNN = RED NEURONAL CONVOLUCIONAL

RNN = RED NEURONAL RECURRENTE

# Creacion Bolsa de palabras para algoritmos de DL
`python ExperimentosLexicon.py Corpus/AImed.csv BolsaPalabras`

python ExperimentosLexicon.py Corpus/AImed.csv BolsaPalabras ninguno &
python ExperimentosLexicon.py Corpus/Biocreative3.csv BolsaPalabras ninguno & 
python ExperimentosLexicon.py Corpus/CorpusCompleto.csv BolsaPalabras ninguno &  
python ExperimentosLexicon.py Corpus/LLL.csv BolsaPalabras ninguno &    
python ExperimentosLexicon.py Corpus/IEPA.csv BolsaPalabras ninguno &   
python ExperimentosLexicon.py Corpus/HPRD50.csv  BolsaPalabras ninguno &   



# Creacion Lexicon de Intensidad
`python ExperimentosLexicon.py <Ruta> <Opcion> <Numero de Clusters>`

`python ExperimentosLexicon.py Corpus/Biocreative3.csv Diccionario`

Si se utiliza la opción de Cluster se debe agregar el diccionario de palabras seleccionadas dentro de la ruta "Documentos/NuevoLexicon/Diccionario/" y indicar el nombre del diccionario como parametro 


`python ExperimentosLexicon.py DiccionarioPositivo.csv Cluster 4`

Si se utiliza la opción de Cluster se debe agregar el diccionario de palabras seleccionadas dentro de la ruta "Documentos/NuevoLexicon/Diccionario/" y indicar el nombre del diccionario como parametro 

`python ExperimentosLexicon.py DiccionarioPositivoCluster.json Lexicon`



# Experimentos Docking 

`python ExperimentosDocking.py <Corpus> <Filtro>`

1- Busqueda de proteinas y formato de las mismas
2- Docking de las proteinas 

UsoModelo
RegistroProteinas
ReducirProteinas
GenerarPDBQT

Docking


python ExperimentosDocking.py Corpus/AImed.csv spacy UsoModelo &
python ExperimentosDocking.py Corpus/Biocreative3.csv spacy UsoModelo & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv spacy UsoModelo & 
python ExperimentosDocking.py Corpus/LLL.csv spacy UsoModelo &
python ExperimentosDocking.py Corpus/IEPA.csv spacy UsoModelo &
python ExperimentosDocking.py Corpus/HPRD50.csv spacy UsoModelo &

python ExperimentosDocking.py Corpus/AImed.csv patrones UsoModelo &
python ExperimentosDocking.py Corpus/Biocreative3.csv patrones UsoModelo & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv patrones UsoModelo & 
python ExperimentosDocking.py Corpus/LLL.csv patrones UsoModelo &
python ExperimentosDocking.py Corpus/IEPA.csv patrones UsoModelo &
python ExperimentosDocking.py Corpus/HPRD50.csv patrones UsoModelo &


python ExperimentosDocking.py Corpus/AImed.csv spacy UsoModelo &


# RegistroProteinas

python ExperimentosDocking.py Corpus/AImed.csv spacy RegistroProteinas & 36:15
python ExperimentosDocking.py Corpus/Biocreative3.csv spacy RegistroProteinas & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv spacy RegistroProteinas & 
python ExperimentosDocking.py Corpus/LLL.csv spacy RegistroProteinas & 04:25
python ExperimentosDocking.py Corpus/IEPA.csv spacy RegistroProteinas & 05:28
python ExperimentosDocking.py Corpus/HPRD50.csv spacy RegistroProteinas & 05:04

python ExperimentosDocking.py Corpus/AImed.csv patrones RegistroProteinas &  
python ExperimentosDocking.py Corpus/Biocreative3.csv patrones RegistroProteinas & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv patrones RegistroProteinas &  
python ExperimentosDocking.py Corpus/LLL.csv patrones RegistroProteinas & 
python ExperimentosDocking.py Corpus/IEPA.csv patrones RegistroProteinas & 
python ExperimentosDocking.py Corpus/HPRD50.csv patrones RegistroProteinas & 

# ReducirProteinas

python ExperimentosDocking.py Corpus/AImed.csv spacy ReducirProteinas &
python ExperimentosDocking.py Corpus/Biocreative3.csv spacy ReducirProteinas & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv spacy ReducirProteinas &
python ExperimentosDocking.py Corpus/LLL.csv spacy ReducirProteinas & 
python ExperimentosDocking.py Corpus/IEPA.csv spacy ReducirProteinas & 
python ExperimentosDocking.py Corpus/HPRD50.csv spacy ReducirProteinas & 

python ExperimentosDocking.py Corpus/AImed.csv patrones ReducirProteinas &
python ExperimentosDocking.py Corpus/Biocreative3.csv patrones ReducirProteinas & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv patrones ReducirProteinas &
python ExperimentosDocking.py Corpus/LLL.csv patrones ReducirProteinas & 
python ExperimentosDocking.py Corpus/IEPA.csv patrones ReducirProteinas & 
python ExperimentosDocking.py Corpus/HPRD50.csv patrones ReducirProteinas & 

# GenerarPDBQT

python ExperimentosDocking.py Corpus/AImed.csv spacy GenerarPDBQT &
python ExperimentosDocking.py Corpus/Biocreative3.csv spacy GenerarPDBQT & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv spacy GenerarPDBQT &
python ExperimentosDocking.py Corpus/LLL.csv spacy GenerarPDBQT & 
python ExperimentosDocking.py Corpus/IEPA.csv spacy GenerarPDBQT & 
python ExperimentosDocking.py Corpus/HPRD50.csv spacy GenerarPDBQT & 

python ExperimentosDocking.py Corpus/AImed.csv patrones GenerarPDBQT &
python ExperimentosDocking.py Corpus/Biocreative3.csv patrones GenerarPDBQT & 
python ExperimentosDocking.py Corpus/CorpusCompleto.csv patrones GenerarPDBQT &
python ExperimentosDocking.py Corpus/LLL.csv patrones GenerarPDBQT & 
python ExperimentosDocking.py Corpus/IEPA.csv patrones GenerarPDBQT & 
python ExperimentosDocking.py Corpus/HPRD50.csv patrones GenerarPDBQT & 

reduce 1a72.pdb > 1a72_H.pdb
prepare_receptor -r 1a72_H.pdb
prepare_ligand -l 1a72_H.pdb

Docking/AImed/Proteinas/0/1ZRP.pdb

reduce Docking/AImed/Proteinas/0/1ZRP.pdb > Docking/AImed/Proteinas/0/1ZRP_H.pdb

reduce 2gtl.pdb > 1a72_H.pdb


prepare_receptor -r 2gtl_H.pdb
prepare_ligand -l 2gtl_H.pdb

agfr -r 1a72_H.pdbqt -l 1a72_H.pdbqt -asv 1.1 -o 1a72_2gtl

adcp -t 3Q47.trg -s npisdvd -N 20 -n 1000000 -o 3Q47_redocking -ref 3Q47_pepH.pdb

# Uso de los Modelos 

`python Experimentos.py <Corpus> <Algoritmo de Clasificacion> <Formato de entrada> <Tipo de busqueda de proteinas> <Utilizar texto ya filtrado> <Uso de información extra>`


## Ejemplos:
## ML:

`python Experimentos.py Corpus/Biocreative3.csv NB TFIDF Filtrado spacy No &` #
`python Experimentos.py Corpus/Biocreative3.csv SVM TFIDF Filtrado spacy No &` #
`python Experimentos.py Corpus/Biocreative3.csv RF TFIDF Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv KNN TFIDF Filtrado spacy No &` 

`python Experimentos.py Corpus/Biocreative3.csv NB TFIDF_LEXICON Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv SVM TFIDF_LEXICON Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv RF TFIDF_LEXICON Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv KNN TFIDF_LEXICON Filtrado spacy No &` 

`python Experimentos.py Corpus/Biocreative3.csv NB WEMB Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv SVM WEMB Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv RF WEMB Filtrado spacy No &` 
`python Experimentos.py Corpus/Biocreative3.csv KNN WEMB Filtrado spacy No &` 

`python Experimentos.py Corpus/Biocreative3.csv NB WEMB_LEXICON Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv SVM WEMB_LEXICON Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv RF WEMB_LEXICON Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv KNN WEMB_LEXICON Filtrado spacy &` 

`python Experimentos.py Corpus/Biocreative3.csv NB WEMB_LEXICON_TFIDF Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv SVM WEMB_LEXICON_TFIDF Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv RF WEMB_LEXICON_TFIDF Filtrado spacy &` 
`python Experimentos.py Corpus/Biocreative3.csv KNN WEMB_LEXICON_TFIDF Filtrado spacy &` 

## ML
`python <Tipo de Experimento>.py Corpus/<Corpus>.csv <Algoritmo de Clasificacion> <Formato de entrada> <Informacion del Corpus> <Tipo de Filtro> <Guardar Modelo>` 

Tipo de Experimento = ExperimentosML/ExperimentosDL
Corpus = AImed/Biocreative3/CorpusCompleto/LLL/IEPA/HPRD50
Algoritmo de Clasificacion ML = NB/SVM/RF/KNN/ SVM_NB_RF/SVM_KNN_RF/KNN_NB_RF/SVM_NB_KNN
Algoritmo de Clasificacion DL = CNN/LSTM/GRU/BERT
Formato de entrada = LEXICON/TFIDF/TFIDF_LEXICON/WEMB/WEMB_LEXICON/WEMB_LEXICON_TFIDF/MULTIPLE_TFIDF/MULTIPLE_WEMB
Informacion del Corpus = Filtrado/NoFiltrado/InfoExtra
Tipo de Filtro = ninguno/spacy/patrones
Guardar Modelo = Guardar/NoGuardar

python ExperimentosML.py AImed SVM LEXICON NoFiltrado spacy Guardar &

python ExperimentosML.py Corpus/AImed.csv SVM TFIDF_LEXICON Filtrado spacy Guardar &
python ExperimentosML.py Corpus/AImed.csv KNN MULTIPLE_TFIDF Filtrado spacy Guardar &

# TFIDF
# NB
`python ExperimentosML.py Corpus/LLL.csv NB TFIDF NoFiltrado ninguno &`

`python ExperimentosML.py Corpus/HPRD50.csv NB TFIDF Filtrado spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv NB TFIDF Filtrado patrones &`

`python ExperimentosML.py Corpus/Biocreative3.csv NB TFIDF InfoExtra spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv NB TFIDF InfoExtra patrones &`

# SVM
`python ExperimentosML.py Corpus/IEPA.csv SVM TFIDF NoFiltrado ninguno &`

`python ExperimentosML.py Corpus/LLL.csv SVM TFIDF Filtrado spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv SVM TFIDF Filtrado patrones &`

`python ExperimentosML.py Corpus/Biocreative3.csv SVM TFIDF InfoExtra spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv SVM TFIDF InfoExtra patrones &`

# RF
`python ExperimentosML.py Corpus/LLL.csv RF TFIDF NoFiltrado ninguno &`

`python ExperimentosML.py Corpus/LLL.csv RF TFIDF Filtrado spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv RF TFIDF Filtrado patrones &`

`python ExperimentosML.py Corpus/Biocreative3.csv RF TFIDF InfoExtra spacy &`
`python ExperimentosML.py Corpus/Biocreative3.csv RF TFIDF InfoExtra patrones &`


## DL:

`python ExperimentosDL.py Corpus/AImed.csv LSTM WEMB Filtrado spacy &` #

`python ExperimentosDL.py Corpus/AImed.csv CNN WEMB Filtrado spacy &` #

`python ExperimentosDL.py Corpus/LLL.csv LSTM WEMB Filtrado spacy &` #
`python ExperimentosDL.py Corpus/Biocreative3.csv LSTM WEMB Filtrado spacy &` #
`python ExperimentosDL.py Corpus/Biocreative3.csv CNN WEMB Filtrado spacy &` #
`python ExperimentosDL.py Corpus/Biocreative3.csv BERT BERTWEMB Filtrado spacy &` #

`python Experimentos.py Corpus/Biocreative3.csv RF Filtrado patrones`

`python Experimentos.py Corpus/AImed.csv RF Filtrado patrones`
`python Experimentos.py Corpus/AImed.csv RF Filtrado spacy`


## Ejemplos No Filtrados:

`python Experimentos.py Corpus/Biocreative3.csv RF NoFiltrado spacy` # 29 horas
`python Experimentos.py Corpus/Biocreative3.csv RF NoFiltrado patrones` # 90 horas

`python Experimentos.py Corpus/AImed.csv RF NoFiltrado patrones` # Falta cambiar el nombre  90 horas
`python Experimentos.py Corpus/AImed.csv RF NoFiltrado spacy`
`python Experimentos.py Corpus/AImed.csv NB TFIDF NoFiltrado spacy &` 16:58  6:30

`python Experimentos.py Corpus/LLL.csv NB TFIDF NoFiltrado spacy No &` 50 minutos


`python Experimentos.py Corpus/CorpusCompleto.csv RF NoFiltrado spacy` # 2 horas
`python Experimentos.py Corpus/CorpusCompleto.csv RF NoFiltrado patrones`# 4 horas




`python Experimentos.py Corpus/AImed.csv RF`
`python Experimentos.py Corpus/CorpusCompleto.csv RF`


## Busqueda de Información y Filtro de corpus

`python ExtracionDatosCorpus.py <Corpus> <Tipo de busqueda de proteinas>`
`buscador_p  patrones - spacy `

# AImed
`python ExtracionDatosCorpus.py Corpus/AImed.csv patrones &`
`python ExtracionDatosCorpus.py Corpus/AImed.csv spacy &`

# Biocreative3
`python ExtracionDatosCorpus.py Corpus/Biocreative3.csv patrones &`
`python ExtracionDatosCorpus.py Corpus/Biocreative3.csv spacy &`

# BioInfer
`python ExtracionDatosCorpus.py Corpus/BioInfer.csv patrones &` 45 hrs
`python ExtracionDatosCorpus.py Corpus/BioInfer.csv spacy &` 22 hrs

# Corpus Completo
`python ExtracionDatosCorpus.py Corpus/CorpusCompleto.csv patrones &`
`python ExtracionDatosCorpus.py Corpus/CorpusCompleto.csv spacy &`

# HPRD50
`python ExtracionDatosCorpus.py Corpus/HPRD50.csv patrones &` 1.40 horas
`python ExtracionDatosCorpus.py Corpus/HPRD50.csv spacy &` 50 min 

# IEPA
`python ExtracionDatosCorpus.py Corpus/IEPA.csv patrones &` 2.12 horas
`python ExtracionDatosCorpus.py Corpus/IEPA.csv spacy &`  1.23 horas

# LLL
`python ExtracionDatosCorpus.py Corpus/LLL.csv patrones &` 1.26 horas
`python ExtracionDatosCorpus.py Corpus/LLL.csv spacy &` 50 min 

Buscar proteinas con patrones  

Buscar proteinas con spacy 

Biocreative 3 

6279

pip install 