
import re
from collections import Counter
from string import punctuation
import torch
import torch.nn as nn
import numpy as np
import json

# Librerías de  red sin  pipeline

import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from transformers import BertModel, BertTokenizer, AdamW, get_linear_schedule_with_warmup
from sklearn.metrics import f1_score, mean_squared_error, precision_score, recall_score


def Tokenize(text, nlp):
    text = re.sub(r"[^\x00-\x7F]+", " ", text)
    # remove punctuation and numbers
    regex = re.compile('[' + re.escape(punctuation) + '0-9\\r\\t\\n]')
    nopunct = regex.sub(" ", text.lower())
    return [token.text for token in nlp.tokenizer(nopunct)]


def CrearDiccionario(corpus, nombre_diccionario):
    counts = Counter()
    for index, row in corpus.iterrows():
        counts.update(Tokenize(row['Abstract']))

    for word in list(counts):
        if counts[word] < 2:
            del counts[word]

    with open(f'{nombre_diccionario}.json', 'w') as f:
        json.dump(counts, f)


def DiccionarioPalabras(X, nlp):
    counts = Counter()
    for row in X:
        counts.update(Tokenize(text=row, nlp=nlp))

    for word in list(counts):
        if counts[word] < 2:
            del counts[word]
    return counts


def EmbMatrix(pretrained, word_counts, emb_size=200):
    vocab_size = len(word_counts) + 2
    vocab_to_idx = {}
    vocab = ["", "UNK"]
    W = np.zeros((vocab_size, emb_size), dtype="float32")
    W[0] = np.zeros(emb_size, dtype='float32')  # adding a vector for padding
    # adding a vector for unknown words
    W[1] = np.random.uniform(-0.25, 0.25, emb_size)
    vocab_to_idx["UNK"] = 1
    i = 2
    for word in word_counts:
        if word in pretrained:
            W[i] = pretrained[word]
        else:
            W[i] = np.random.uniform(-0.25, 0.25, emb_size)
        vocab_to_idx[word] = i
        vocab.append(word)
        i += 1
    return W, np.array(vocab), vocab_to_idx


class ModeloLSTM(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, glove_weights):
        super().__init__()
        self.embeddings = nn.Embedding(
            vocab_size, embedding_dim, padding_idx=0)
        self.embeddings.weight.data.copy_(torch.from_numpy(glove_weights))
        self.embeddings.weight.requires_grad = False  # freeze embeddings
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.linear = nn.Linear(hidden_dim, 5)
        self.dropout = nn.Dropout(0.2)

    def forward(self, x):
        x = self.embeddings(x)
        x = self.dropout(x)
        lstm_out, (ht, ct) = self.lstm(x)
        return self.linear(ht[-1])


# Funciones Red sin Pipeline

# BERT

class BERTDataset(Dataset):

    def __init__(self, reviews, labels, tokenizer, max_len):
        self.reviews = reviews
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_len = max_len

    def __len__(self):
        return len(self.reviews)

    def __getitem__(self, item):
        review = str(self.reviews[item])
        label = self.labels[item]
        encoding = self.tokenizer.encode_plus(
            review,
            max_length=self.max_len,
            truncation=True,
            add_special_tokens=True,
            return_token_type_ids=False,
            padding='max_length',
            return_attention_mask=True,
            return_tensors='pt'
        )

        return {
            'review': review,
            'input_ids': encoding['input_ids'].flatten(),
            'attention_mask': encoding['attention_mask'].flatten(),
            'label': torch.tensor(label, dtype=torch.long)
        }


def BERTDataLoader(X, y, tokenizer, max_len, batch_size):
    dataset = BERTDataset(
        reviews=X,
        labels=y,
        tokenizer=tokenizer,
        max_len=max_len
    )

    return DataLoader(dataset, batch_size=batch_size, num_workers=4)


#


class FormatoDatasetBERT(Dataset):

    def __init__(self, abstracts, clasificaciones, tokenizer, max_len):
        self.abstracts = abstracts
        self.clasificaciones = clasificaciones
        self.tokenizer = tokenizer
        self.max_len = max_len

    def __len__(self):
        return len(self.abstracts)

    def __getitem__(self, item):
        abstract = str(self.abstracts[item])
        clasificacion = self.clasificaciones[item]
        encoding = self.tokenizer.encode_plus(
            abstract,
            max_length=self.max_len,
            truncation=True,
            add_special_tokens=True,
            return_token_type_ids=False,
            padding=True,
            return_attention_mask=True,
            return_tensors='pt'
        )

        return {
            'abstract': abstract,
            'input_ids': encoding['input_ids'].flatten(),
            'attention_mask': encoding['attention_mask'].flatten(),
            'label': torch.tensor(clasificacion, dtype=torch.long)
        }


class FormatoDataset(Dataset):
    def __init__(self, X, Y):
        self.X = X
        self.y = Y

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx]


class MBERT(nn.Module):

    def __init__(self, n_clases, nombre_modelo_pre):
        super(MBERT, self).__init__()
        self.bert = BertModel.from_pretrained(
            nombre_modelo_pre, return_dict=False)
        self.drop = nn.Dropout(p=0.3)
        self.linear = nn.Linear(self.bert.config.hidden_size, n_clases)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input_ids, attention_mask):
        _, cls_output = self.bert(
            input_ids=input_ids,
            attention_mask=attention_mask
        )
        drop_output = self.drop(cls_output)
        output = self.linear(drop_output)
        out = self.softmax(output)
        return out
        # return output


class MLSTM(torch.nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, glove_weights):
        super().__init__()
        self.embeddings = nn.Embedding(
            vocab_size, embedding_dim, padding_idx=0)
        self.embeddings.weight.data.copy_(torch.from_numpy(glove_weights))
        self.embeddings.weight.requires_grad = False  # freeze embeddings
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)
        self.linear = nn.Linear(hidden_dim, 1)
        self.dropout = nn.Dropout(0.2)

    def forward(self, x):
        x = self.embeddings(x)
        x = self.dropout(x)
        lstm_out, (ht, ct) = self.lstm(x)
        return self.linear(ht[-1])


class MGRU(torch.nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, glove_weights):
        super().__init__()
        self.embeddings = nn.Embedding(
            vocab_size, embedding_dim, padding_idx=0)
        self.embeddings.weight.data.copy_(torch.from_numpy(glove_weights))
        self.embeddings.weight.requires_grad = False  # freeze embeddings
        self.gru = nn.GRU(embedding_dim, hidden_dim, batch_first=True)
        self.linear = nn.Linear(hidden_dim, 1)
        self.dropout = nn.Dropout(0.2)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.embeddings(x)
        x = self.dropout(x)
        gru_out, _ = self.gru(x)
        out = self.linear(gru_out[:, -1, :])
        out = self.softmax(out)
        return out


class MCNN(nn.Module):

    def __init__(self, vocab_size, embedding_dim, hidden_dim, glove_weights):
        super(MCNN, self).__init__()
        # self.args = args

        V = vocab_size
        D = embedding_dim
        C = hidden_dim
        Ci = 1
        Co = 100  # numero de kernel
        Ks = [3, 4, 5]  # kernel sizes

        # Embedding
        self.embed = nn.Embedding(
            vocab_size, embedding_dim, padding_idx=0)
        self.embed.weight.data.copy_(torch.from_numpy(glove_weights))
        self.embed.weight.requires_grad = False  # freeze embeddings

        self.convs = nn.ModuleList([nn.Conv2d(Ci, Co, (K, D)) for K in Ks])
        self.dropout = nn.Dropout(0.5)
        self.fc1 = nn.Linear(len(Ks) * Co, C)

        # if self.args.static:
        #     self.embed.weight.requires_grad = False

    def forward(self, x):
        x = self.embed(x)  # (N, W, D)

        x = x.unsqueeze(1)  # (N, Ci, W, D)

        x = [F.relu(conv(x)).squeeze(3)
             for conv in self.convs]  # [(N, Co, W), ...]*len(Ks)

        x = [F.max_pool1d(i, i.size(2)).squeeze(2)
             for i in x]  # [(N, Co), ...]*len(Ks)

        x = torch.cat(x, 1)

        x = self.dropout(x)  # (N, len(Ks)*Co)
        logit = self.fc1(x)  # (N, C)
        return logit


class MCNN2(nn.Module):

    def __init__(self, vocab_size, embedding_dim, hidden_dim, glove_weights):
        super(MCNN2, self).__init__()
        filter_sizes = [1, 2, 3, 5]
        num_filters = 36
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.embedding.weight = nn.Parameter(
            torch.tensor(glove_weights, dtype=torch.float32))
        self.embedding.weight.requires_grad = False
        self.convs1 = nn.ModuleList(
            [nn.Conv2d(1, num_filters, (K, embedding_dim)) for K in filter_sizes])
        self.dropout = nn.Dropout(0.1)
        self.fc1 = nn.Linear(len(filter_sizes)*num_filters, 1)

    def forward(self, x):
        x = self.embedding(x)
        x = x.unsqueeze(1)
        x = [F.relu(conv(x)).squeeze(3) for conv in self.convs1]
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        x = torch.cat(x, 1)
        x = self.dropout(x)
        logit = self.fc1(x)
        return logit


class CNN_Text(nn.Module):

    def __init__(self, args):
        super(CNN_Text, self).__init__()
        self.args = args

        V = args.embed_num
        D = args.embed_dim
        C = args.class_num
        Ci = 1
        Co = args.kernel_num
        Ks = args.kernel_sizes

        self.embed = nn.Embedding(V, D)
        self.convs = nn.ModuleList([nn.Conv2d(Ci, Co, (K, D)) for K in Ks])
        self.dropout = nn.Dropout(args.dropout)
        self.fc1 = nn.Linear(len(Ks) * Co, C)

        if self.args.static:
            self.embed.weight.requires_grad = False

    def forward(self, x):
        x = self.embed(x)  # (N, W, D)

        x = x.unsqueeze(1)  # (N, Ci, W, D)

        x = [F.relu(conv(x)).squeeze(3)
             for conv in self.convs]  # [(N, Co, W), ...]*len(Ks)

        x = [F.max_pool1d(i, i.size(2)).squeeze(2)
             for i in x]  # [(N, Co), ...]*len(Ks)

        x = torch.cat(x, 1)

        x = self.dropout(x)  # (N, len(Ks)*Co)
        logit = self.fc1(x)  # (N, C)
        return logit

# BERT


def BERTEntrenamiento(model, train_data_loader, test_data_loader, device, epochs):
    # ENTRENAMIENTO
    EPOCHS = epochs
    optimizer = AdamW(model.parameters(), lr=2e-5, correct_bias=False)
    total_steps = len(train_data_loader) * EPOCHS
    scheduler = get_linear_schedule_with_warmup(
        optimizer,
        num_warmup_steps=0,
        num_training_steps=total_steps
    )
    loss_fn = nn.CrossEntropyLoss().to(device)

    # Entrenamiento!!!

    for epoch in range(EPOCHS):
        print('Epoch {} de {}'.format(epoch+1, EPOCHS))
        print('------------------')
        train_acc, train_prec, train_recall, train_f1, train_loss = BERTrainModel(
            model, train_data_loader, loss_fn, optimizer, device, scheduler,  len(
                train_data_loader.dataset)
        )
        test_acc, test_prec, test_recall, test_f1, test_loss = BERTEvalModel(
            model, test_data_loader, loss_fn, device, len(
                test_data_loader.dataset)
        )
        print('Entrenamiento: Loss: {}, accuracy: {}'.format(train_loss, train_acc))
        print('Validación: Loss: {}, accuracy: {}'.format(test_loss, test_acc))
        print('')
        
        return test_acc, test_prec, test_recall, test_f1


def BERTrainModel(model, data_loader, loss_fn, optimizer, device, scheduler, n_examples):
    model = model.train()
    losses = []
    correct_predictions = 0

    val_prec, val_recall, val_f1 = 0.0, 0.0, 0.0

    for batch in data_loader:
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['label'].to(device)
        outputs = model(input_ids=input_ids, attention_mask=attention_mask)
        _, preds = torch.max(outputs, dim=1)
        loss = loss_fn(outputs, labels)
        correct_predictions += torch.sum(preds == labels)
        losses.append(loss.item())
        loss.backward()

        val_prec = precision_score(
            labels, preds, average='weighted')
        val_recall = recall_score(labels, preds, average='weighted')

        nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
        optimizer.step()
        scheduler.step()
        optimizer.zero_grad()

    val_f1 = 2 * val_prec * val_recall / (val_prec + val_recall)

    return correct_predictions.double()/n_examples, val_prec, val_recall, val_f1, np.mean(losses)


def BERTEvalModel(model, data_loader, loss_fn, device, n_examples):
    model = model.eval()
    losses = []
    correct_predictions = 0
    val_prec, val_recall, val_f1 = 0.0, 0.0, 0.0
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)
            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            _, preds = torch.max(outputs, dim=1)
            loss = loss_fn(outputs, labels)
            correct_predictions += torch.sum(preds == labels)
            losses.append(loss.item())

            val_prec = precision_score(
                labels, preds, average='weighted')
            val_recall = recall_score(labels, preds, average='weighted')

        val_f1 = 2 * val_prec * val_recall / (val_prec + val_recall)

    return correct_predictions.double()/n_examples, val_prec, val_recall, val_f1, np.mean(losses)


#

def TrainModel(model, train_dl, val_dl, clasificador, epochs=10, lr=0.001):
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, lr=lr)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    val_acc, val_prec, val_recall, val_f1 = 0.0, 0.0, 0.0, 0.0
    for i in range(epochs):
        model.train()
        total = 0
        if clasificador == "BERT":
            for batch in train_dl:
                input_ids = batch['input_ids'].to(device)
                attention_mask = batch['attention_mask'].to(device)
                y = batch['label'].to(device)
                outputs = model(input_ids=input_ids,
                                attention_mask=attention_mask)
                _, y_pred = torch.max(outputs, dim=1)
                optimizer.zero_grad()
                optimizer.step()
                total += y.shape[0]
        else:
            for x, y in train_dl:
                x = x.long()
                y = y.long()
                y_pred = model(x)
                optimizer.zero_grad()
                optimizer.step()
                total += y.shape[0]
        val_acc, val_prec, val_recall, val_f1 = ValidationMetrics(
            model=model, valid_dl=val_dl, clasificador=clasificador, device=device)
        if i % 5 == 1:
            print("val acc %.3f, prec %.3f, recall %.3f, f1 %.3f" % (
                val_acc,  val_prec, val_recall, val_f1))

    return val_acc, val_prec, val_recall, val_f1


def ValidationMetrics(model, valid_dl, clasificador, device):
    model.eval()
    correct = 0
    total = 0
    val_prec, val_recall, val_f1 = 0.0, 0.0, 0.0
    if clasificador == "BERT":
        for batch in valid_dl:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            y = batch['label'].to(device)
            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            _, pred = torch.max(outputs, dim=1)
            correct += (pred == y).float().sum()
            total += y.shape[0]
            val_prec = precision_score(
                y, pred, average='weighted', zero_division=1)
            val_recall = recall_score(y, pred, average='weighted')
            val_f1 = f1_score(y, pred, average='weighted')
    else:
        for x, y in valid_dl:
            x = x.long()
            y = y.long()
            y_hat = model(x)
            pred = torch.max(y_hat, 1)[1]
            correct += (pred == y).float().sum()
            total += y.shape[0]
            val_prec = precision_score(
                y, pred, average='weighted')
            val_recall = recall_score(y, pred, average='weighted')
            val_f1 = f1_score(y, pred, average='weighted')

    return correct/total, val_prec, val_recall, val_f1
