import re
from string import punctuation

no_palabras = list(punctuation) 
no_palabras.extend(['¿', '¡'])

class Tokenizador():

    def __init__(self, nlp, tipo):
        self.nlp = nlp
        self.tipo = tipo

    def SelecionarTokenizador(self, doc):
        if self.tipo == "spacy":
            return self.TokenizadorSpacy(doc)

    def TokenizadorSpacy(self,doc):
        doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
        return [x.orth_ for x in self.nlp(doc.lower()) if (x.orth_ not in no_palabras)]

