
from sklearn.base import TransformerMixin, BaseEstimator
import numpy as np
import Transformer.FuncionesTransformers  as ft



class VectorEncondeSentence(BaseEstimator, TransformerMixin):
    def __init__(self, counts, nlp):
        self.counts = counts
        self.nlp = nlp

    def fit(self, X, y=None):
        return self
    
    def EncodeSentence(self, text, vocab2index, N=70):
        tokenized = ft.Tokenize(text, self.nlp)
        encoded = np.zeros(N, dtype=int)
        enc1 = np.array([vocab2index.get(word, vocab2index["UNK"]) for word in tokenized])
        length = min(N, len(enc1))
        encoded[:length] = enc1[:length]
        return encoded

    def transform(self, X, y=None):
        vocab2index = {"":0, "UNK":1}
        words = ["", "UNK"]
        for word in self.counts:
            vocab2index[word] = len(words)
            words.append(word)    
        X_new = [self.EncodeSentence(i,vocab2index) for i in X  ] 
        return  np.asarray(X_new) 