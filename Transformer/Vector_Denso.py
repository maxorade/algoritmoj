from sklearn.base import TransformerMixin,BaseEstimator
# Tranformer para convertir vector en denso (lo requiere NB)
class DenseTransformer(BaseEstimator,TransformerMixin):
    def __init__(self,activar=True):
        self.activar = activar
        
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        if self.activar:
            return X.toarray()
        else:
            return X