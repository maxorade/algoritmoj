# coding: utf-8
# Librerías
import re
import spacy
import nltk
import sys
import os
import numpy as np
import pandas as pd
from string import punctuation
from sklearn.base import TransformerMixin, BaseEstimator
import sys
sys.path.append('../Herramientas') 
import Herramientas.BusquedaProteinas as bp
# import ..Herramientas.BusquedaProteinas as bp

# Modifica la ruta de ejecucion para permitir la lectura de los archivo
rutaOriginal = os.getcwd()
os.chdir(os.path.abspath(__file__).replace(os.path.basename(__file__), ""))

# Carga modelo spacy español
# nlp = spacy.load("es_core_news_sm", disable=['ner', 'parser', 'tagger'])


listaStopword = set(nltk.corpus.stopwords.words('english')) 

# Archivo que contiene una lista de Stop Word
# with open('archivos/stopwords.txt') as archivo_stopword:
#     listaStopword=[line.rstrip('\n') for line in archivo_stopword]

# # Archivo que contiene una lista de malas palabras
# with open("archivos/badWords.csv") as archivo_badwords:
#     listaBadWords=[line.rstrip(',\n') for line in archivo_badwords]

# Lista que se usa para eliminar los signos
no_palabras = list(punctuation)
no_palabras.extend(['¿', '¡'])

from Bio.PDB import PDBList 



interaccion = pd.read_csv(
    '../Documentos/LexiconPositivo.csv', sep=',', encoding='latin-1')
interaccionPalabras = np.asarray(interaccion['Palabras'])
interaccionPalabras = interaccionPalabras.ravel()
interaccionIntensidad = np.asarray(interaccion['Intensidad'])
interaccionIntensidad = interaccionIntensidad.ravel()

nointeraccion = pd.read_csv(
    '../Documentos/LexiconNegativo.csv', sep=',', encoding='latin-1')
nointeraccionPalabras = np.asarray(nointeraccion['Palabras'])
nointeraccionPalabras = nointeraccionPalabras.ravel()
nointeraccionIntensidad = np.asarray(nointeraccion['Intensidad'])
nointeraccionIntensidad = nointeraccionIntensidad.ravel()


# Se vuelve a la ruta original (para no afectar la ejecución de donde importa)
os.chdir(rutaOriginal)


class VectorLexicon(BaseEstimator, TransformerMixin):
    def __init__(self, nlp):
        self.nlp = nlp

    def fit(self, X, y=None):
        return self

    def normalize(self, s):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
        )
        for a, b in replacements:
            s = s.replace(a, b).replace(a.upper(), b.upper())
        return s

    def BuscarLexiconAfecto(self, palabra):
        vectorPalabra = np.zeros(3)
        palabra = self.normalize(palabra)

        if palabra in interaccionPalabras:
            vectorPalabra[0] = interaccionIntensidad[np.where(
                interaccionPalabras == str(palabra))[0][0]]
        
        # # Identificar Proteinas
        # pdblist = PDBList()
        # all_pdb_ids = pdblist.get_all_entries() 
        # matches =  bp.IdentificarPDBId(texto=palabra)
        # for proteina in matches:
        #     proteina = proteina.upper()
        #     try:
        #         numero = float(proteina)
        #     except ValueError:
        #         if proteina in all_pdb_ids: 
        #             vectorPalabra[0] = 90


        if palabra in nointeraccionPalabras:
            vectorPalabra[1] = nointeraccionIntensidad[np.where(
                nointeraccionPalabras == str(palabra))[0][0]]

        return vectorPalabra

    def CalcularVectorAfecto(self, frase):
        vectorFrase = np.zeros(3)
        numeroPalabras = 0

        for palabra in self.nlp(frase.lower()):
            if palabra.lemma_ not in listaStopword and palabra.lemma_ not in no_palabras:
                vectorFrase += self.BuscarLexiconAfecto(palabra.lemma_)
                numeroPalabras += 1
        vectorFrase[2] = numeroPalabras

        return vectorFrase

    def transform(self, X, y=None):
        X_vectorLexicon = []
        for frase in X:
            X_vectorLexicon.append(self.CalcularVectorAfecto(frase))
        return np.asarray(X_vectorLexicon)
