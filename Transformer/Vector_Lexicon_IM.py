# coding: utf-8
# Librerías
import re, spacy, sys,os
import numpy as np
import pandas as pd
from string import punctuation
from sklearn.base import TransformerMixin,BaseEstimator

# Modifica la ruta de ejecucion para permitir la lectura de los archivo
rutaOriginal = os.getcwd()
os.chdir(os.path.abspath(__file__).replace(os.path.basename(__file__),""))

# Carga modelo spacy español
nlp = spacy.load("es_core_news_sm", disable=['ner', 'parser', 'tagger'])

# Archivo que contiene una lista de Stop Word
with open('archivos/stopwords.txt') as archivo_stopword:
    listaStopword=[line.rstrip('\n') for line in archivo_stopword]

# Archivo que contiene una lista de malas palabras
with open("archivos/badWords.csv") as archivo_badwords:
    listaBadWords=[line.rstrip(',\n') for line in archivo_badwords]

# Archivo que contiene una lista de intensificadores
with open('archivos/intensificadores.txt') as archivo_intensificadores:
    listaInten=[line.rstrip('\n') for line in archivo_intensificadores]

# Archivo que contiene una lista de mitigadores
with open('archivos/mitigadores.txt') as archivo_mitigadores:
    listaMiti= [line.rstrip('\n') for line in archivo_mitigadores]

# Lista que se usa para eliminar los signos
no_palabras = list(punctuation) 
no_palabras.extend(['¿', '¡'])

# Lexicon enojo con intensidad
enojo = pd.read_csv('archivos/LexiconEmociones/angerSinTildes.csv', sep=';', encoding='latin-1')
enojoPalabras = np.asarray(enojo['Palabras'])
enojoPalabras = enojoPalabras.ravel()
enojoIntensidad = np.asarray(enojo['Intensidad'])
enojoIntensidad = enojoIntensidad.ravel()
# Lexicon anticipacion con intensidad
anticipacion = pd.read_csv('archivos/LexiconEmociones/anticSinTildes.csv', sep=';', encoding='latin-1')
anticipacionPalabras = np.asarray(anticipacion['Palabras'])
anticipacionPalabras = anticipacionPalabras.ravel()
anticipacionIntensidad = np.asarray(anticipacion['Intensidad'])
anticipacionIntensidad = anticipacionIntensidad.ravel()
# Lexicon disgusto con intensidad
disgusto = pd.read_csv('archivos/LexiconEmociones/disgustSinTildes.csv', sep=';', encoding='latin-1')
disgustoPalabras = np.asarray(disgusto['Palabras'])
disgustoPalabras = disgustoPalabras.ravel()
disgustoIntensidad = np.asarray(disgusto['Intensidad'])
disgustoIntensidad = disgustoIntensidad.ravel()
# Lexicon miedo con intensidad
miedo = pd.read_csv('archivos/LexiconEmociones/fearSinTildes.csv', sep=';', encoding='latin-1')
miedoPalabras = np.asarray(miedo['Palabras'])
miedoPalabras = miedoPalabras.ravel()
miedoIntensidad = np.asarray(miedo['Intensidad'])
miedoIntensidad = miedoIntensidad.ravel()
# Lexicon alegria con intensidad
alegria = pd.read_csv('archivos/LexiconEmociones/joySinTildes.csv', sep=';', encoding='latin-1')
alegriaPalabras = np.asarray(alegria['Palabras'])
alegriaPalabras = alegriaPalabras.ravel()
alegriaIntensidad = np.asarray(alegria['Intensidad'])
alegriaIntensidad = alegriaIntensidad.ravel()
# Lexicon tristeza con intensidad
tristeza = pd.read_csv('archivos/LexiconEmociones/sadnessSinTildes.csv', sep=';', encoding='latin-1')
tristezaPalabras = np.asarray(tristeza['Palabras'])
tristezaPalabras = tristezaPalabras.ravel()
tristezaIntensidad = np.asarray(tristeza['Intensidad'])
tristezaIntensidad = tristezaIntensidad.ravel()
# Lexicon sorpresa con intensidad
sorpresa = pd.read_csv('archivos/LexiconEmociones/surpriseSinTildes.csv', sep=';', encoding='latin-1')
sorpresaPalabras = np.asarray(sorpresa['Palabras'])
sorpresaPalabras = sorpresaPalabras.ravel()
sorpresaIntensidad = np.asarray(sorpresa['Intensidad'])
sorpresaIntensidad = sorpresaIntensidad.ravel()
# Lexicon confianza con intensidad
confianza = pd.read_csv('archivos/LexiconEmociones/trustSinTildes.csv', sep=';', encoding='latin-1')
confianzaPalabras = np.asarray(confianza['Palabras'])
confianzaPalabras = confianzaPalabras.ravel()
confianzaIntensidad = np.asarray(confianza['Intensidad'])
confianzaIntensidad = confianzaIntensidad.ravel()

# Se vuelve a la ruta original (para no afectar la ejecución de donde importa)
os.chdir(rutaOriginal)

# Transformer para hacer el Vector Lexicon
class VectorLexicon(BaseEstimator,TransformerMixin):
    def __init__(self,lema= True, stopword=True):
        self.lema = lema
        self.stopword = stopword
        
    def fit(self, X, y=None):
        return self
    
    def normalize(self, s):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
        )
        for a, b in replacements:
            s = s.replace(a, b).replace(a.upper(), b.upper())
        return s
    
    def BuscarLexiconAfecto(self,palabra):
        vectorPalabra = np.zeros(12)
        palabra = self.normalize(palabra)
        if palabra in listaMiti:
            vectorPalabra[11] = 1
        if palabra in listaInten:
            vectorPalabra[10] = 1
        if palabra in listaBadWords:
            vectorPalabra[8] = 1
        if palabra in enojoPalabras:
            vectorPalabra[0] = enojoIntensidad[np.where(enojoPalabras == str(palabra))[0][0]]
        if palabra in anticipacionPalabras:
            vectorPalabra[1] = anticipacionIntensidad[np.where(anticipacionPalabras == str(palabra))[0][0]]
        if palabra in disgustoPalabras:
            vectorPalabra[2] = disgustoIntensidad[np.where(disgustoPalabras == str(palabra))[0][0]]
        if palabra in miedoPalabras:
            vectorPalabra[3] = miedoIntensidad[np.where(miedoPalabras == str(palabra))[0][0]]
        if palabra in alegriaPalabras:
            vectorPalabra[4] = alegriaIntensidad[np.where(alegriaPalabras == str(palabra))[0][0]]
        if palabra in tristezaPalabras:
            vectorPalabra[5] = tristezaIntensidad[np.where(tristezaPalabras == str(palabra))[0][0]]
        if palabra in sorpresaPalabras:
            vectorPalabra[6] = sorpresaIntensidad[np.where(sorpresaPalabras == str(palabra))[0][0]]
        if palabra in confianzaPalabras:
            vectorPalabra[7] = confianzaIntensidad[np.where(confianzaPalabras == str(palabra))[0][0]]
        return vectorPalabra
    
    def CalcularVectorAfecto(self,frase):
        vectorFrase = np.zeros(12)
        numeroPalabras = 0
        for palabra in nlp(frase.lower()):
            if self.lema:
                if self.stopword:
                    if palabra.lemma_ not in listaStopword and palabra.lemma_ not in no_palabras:
                        vectorFrase += self.BuscarLexiconAfecto(palabra.lemma_)
                        numeroPalabras +=1
                else:
                    if palabra.lemma_ not in no_palabras:
                        vectorFrase += self.BuscarLexiconAfecto(palabra.lemma_)
                        numeroPalabras +=1
            else:
                if self.stopword:
                    if palabra.orth_ not in listaStopword and palabra.orth_ not in no_palabras:
                        vectorFrase += self.BuscarLexiconAfecto(palabra.orth_)
                        numeroPalabras +=1
                else:
                    if palabra.orth_ not in no_palabras:
                        vectorFrase += self.BuscarLexiconAfecto(palabra.orth_)
                        numeroPalabras +=1
                        
        vectorFrase[8] = vectorFrase[8] / numeroPalabras
        vectorFrase[9] = numeroPalabras
        return vectorFrase
    
    def transform(self, X, y=None):
        X_vectorLexicon =[]
        for frase in X:
            X_vectorLexicon.append(self.CalcularVectorAfecto(frase))
        return np.asarray(X_vectorLexicon)