# coding: utf-8
# Librerías
import os
import sys
import re
import spacy
import numpy as np
from string import punctuation
from sklearn.base import TransformerMixin, BaseEstimator
from gensim.models import KeyedVectors
from numpy.linalg import norm

# Modifica la ruta de ejecucion para permitir la lectura de los archivo
rutaOriginal = os.getcwd()
os.chdir(os.path.abspath(__file__).replace(os.path.basename(__file__), ""))

# Carga modelo spacy español
# nlp = spacy.load("en_ner_jnlpba_md")

# Archivo que contiene una lista de Stop Word
# with open('archivos/stopwords.txt') as archivo_stopword:
#     listaStopword=[line.rstrip('\n') for line in archivo_stopword]

# Lista que se usa para eliminar los signos
no_palabras = list(punctuation)
no_palabras.extend(['¿', '¡'])

# Carga modelo Word Embedding
# representacionVectorial = KeyedVectors.load_word2vec_format('Embeddings/PubMed-w2v.bin', binary=True)

# Se vuelve a la ruta original (para no afectar la ejecución de donde importa)
os.chdir(rutaOriginal)

# Transformer para hacer el Vector WordEbedding


class VectorWordEmbedding(BaseEstimator, TransformerMixin):
    def __init__(self, nlp, representacionVectorial):
        self.nlp = nlp
        self.representacionVectorial = representacionVectorial

    def fit(self, X, y=None):
        return self

    def normalize(self, s):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
        )
        for a, b in replacements:
            s = s.replace(a, b).replace(a.upper(), b.upper())
        return s

    def CalcularWordEbedding(self, frase):
        vectorFrase = np.zeros(200)
        palabrasEncontradas = 0
        frase = re.sub('[%s]' % re.escape(punctuation), ' ', frase)

        for palabra in self.nlp(frase.lower()):
            if palabra.lemma_ not in no_palabras and palabra.lemma_ in self.representacionVectorial:
                palabrasEncontradas += 1
                vectorFrase += self.representacionVectorial[palabra.lemma_]

        if palabrasEncontradas != 0:
            vectorFrase = vectorFrase / norm(vectorFrase)
        return vectorFrase

    def transform(self, X, y=None):
        X_WordEbedding = []
        for frase in X:
            X_WordEbedding.append(self.CalcularWordEbedding(frase))
        return np.asarray(X_WordEbedding)
