# #!/usr/bin/env python3

# ##########################################################
# ## For a given list of proteins the script resolves them
# ## (if possible) to the best matching STRING identifier
# ## and prints out the mapping on screen in the TSV format
# ##
# ## Requires requests module:
# ## type "python -m pip install requests" in command line
# ## (win) or terminal (mac/linux) to install the module
# ###########################################################

# import requests ## python -m pip install requests

# string_api_url = "https://version-11-5.string-db.org/api"
# output_format = "tsv-no-header"
# method = "get_string_ids"

# ##
# ## Set parameters
# ##

# params = {

#     "identifiers" : "\r".join(["4GCJ", "3QTW"]), # your protein list
#     "limit" : 2, # only one (best) identifier per input protein
#     "echo_query" : 2, # see your input identifiers in the output
#     "caller_identity" : "www.awesome_app.org" # your app name

# }

# ##
# ## Construct URL
# ##


# request_url = "/".join([string_api_url, output_format, method])

# ##
# ## Call STRING
# ##

# results = requests.post(request_url, data=params)

# ##
# ## Read and parse the results
# ##

# with open('ResultadoString.txt', 'w') as f:
#     # Write some text to the file
#     f.write(results.text)


# # print(results.content)

# # for line in results.text.strip().split("\n"):
# #     l = line.split("\t")
# #     input_identifier, string_identifier = l[0], l[2]
# #     print(input_identifier)

# from Bio.PDB import PDBList, PDBParser

# # Código PDB a analizar
# pdb_code = "5ou8"

# # Descarga la estructura correspondiente al código PDB
# pdb_list = PDBList()
# pdb_file = pdb_list.retrieve_pdb_file(pdb_code)

# # Parsea el archivo PDB y cuenta el número de cadenas
# parser = PDBParser()
# structure = parser.get_structure(pdb_code, pdb_file)
# n_chains = len(structure[0])

# # Determina si se trata de una proteína o de un complejo
# if n_chains == 1:
#     print(f"El código PDB {pdb_code} corresponde a una proteína")
# else:
#     print(f"El código PDB {pdb_code} corresponde a un complejo")


##############################


import requests

# Define the UniProt ID of the protein
# uniprot_id = "Q14315"

# # Send a GET request to the UniProt API to retrieve the entry information
# url = f"https://www.uniprot.org/uniprot/{uniprot_id}.xml"
# response = requests.get(url)

# # Parse the XML response and extract the PDB code
# xml = response.content.decode("utf-8")
# pdb_code = ""
# for line in xml.split("\n"):
#     if "<dbReference type=\"PDB\"" in line:
#         start = line.find("id=") + 4
#         end = line.find("\"", start)
#         pdb_code = line[start:end]
#         break

# # Print the PDB code
# print(f"The PDB code of {uniprot_id} is {pdb_code}")


#####################

# import requests

# # Define the BioGRID ID of the protein
# biogrid_id = "ETG6416"
# accesskey = "b324f18e0acf2122bc1829e75ab9c768"

# https://webservice.thebiogrid.org/interactions/?searchNames=true&geneList=ETG6416&accesskey=b324f18e0acf2122bc1829e75ab9c768

# # Send a GET request to the BioGRID API to retrieve the interaction information
# url = f"https://webservice.thebiogrid.org/interactions/{biogrid_id}?format=json&accesskey={accesskey}"
# response = requests.get(url)

# print(response.url)

# # Parse the JSON response and extract the UniProt code
# json_data = response.json()
# uniprot_code = ""
# for interact in json_data["interactions"]:
#     for participant in interact["participants"]:
#         if participant["biogrid_id"] == biogrid_id:
#             uniprot_code = participant["uniprot_id"]
#             break

# # Print the UniProt code
# print(f"The UniProt code of BioGRID ID {biogrid_id} is {uniprot_code}")
# from sklearn.feature_extraction.text import TfidfVectorizer
# from transformers import AutoTokenizer, TFAutoModel
# import numpy as np

# # Load the BioBERT tokenizer and model
# tokenizer = AutoTokenizer.from_pretrained('monologg/biobert_v1.1_pubmed')
# model = TFAutoModel.from_pretrained('monologg/biobert_v1.1_pubmed')

# # Define the documents
# docs = ['This is the first document.', 'This is the second document.', 'And this is the third document.']

# # Tokenize the documents
# tokens = [tokenizer(doc, padding=True, truncation=True, return_tensors='tf') for doc in docs]

# # Get the BioBERT embeddings
# outputs = [model(token)['last_hidden_state'][:, 0, :].numpy() for token in tokens]

# # Flatten the embeddings
# embeddings = [embedding.flatten() for embedding in outputs]

# # Convert the embeddings to a numpy array
# embeddings = np.array(embeddings)

# # Compute the TF-IDF scores
# vectorizer = TfidfVectorizer()
# tfidf_scores = vectorizer.fit_transform(docs)

# # Print the TF-IDF scores
# print(tfidf_scores.toarray())


####################################


# import Herramientas.BusquedaProteinas as bp 

# bp.DescargarListadoProteinNucleicAcidComplexes()

###################3

# import pandas as pd
# import json

# with open('Documentos/ListadosComplejosPdb.json', 'r') as file:
#     listadoscomplejospdb = json.load(file)

# with open('Documentos/ListadoProteinNucleicAcidComplexes.json', 'r') as file:
#     listadoproteinnucleic = json.load(file)

# with open('Documentos/BiogridPDB/ParesProteinasBiogrid.json', 'r') as file:
#     listadopares = json.load(file)


# complejosobligate = pd.read_csv("Documentos/DatosProfesora/ComplejosObligate.csv")
# complejostransient = pd.read_csv("Documentos/DatosProfesora/ComplejosTransient.csv")


# total_proteinas_pares = []
# listado_complejos_pares = []
# listado_query2_pares = []

# for i in listadopares:
#     pdb1 = i[0]
#     pdb2 = i[1]
#     total_proteinas_pares.append(pdb1)
#     total_proteinas_pares.append(pdb2)

#     if pdb1 in listadoscomplejospdb:
#         listado_complejos_pares.append(pdb1)

#     if pdb2 in listadoscomplejospdb:
#         listado_complejos_pares.append(pdb1)

#     if pdb1 in listadoproteinnucleic:
#         listado_query2_pares.append(pdb1)

#     if pdb2 in listadoproteinnucleic:
#         listado_query2_pares.append(pdb1)    

# print("len(listado_complejos_pares) ",len(listado_complejos_pares))
# print("len(listado_query2_pares) ",len(listado_query2_pares))

# print("len(total_proteinas_pares) ",len(total_proteinas_pares))

# len(complejosobligate['Proteina'])  107
# len(listado_complejos)  105
# len(listado_query2)  5
# len(complejostransient['Proteina'])  211
# len(listado_complejos)  206
# len(listado_query2)  3


# len(listadopares)  12939

# len(listado_complejos_pares)  14356
# len(listado_query2_pares)  4192
# len(total_proteinas_pares)  25878


# biogrid api
# accesskey = "b324f18e0acf2122bc1829e75ab9c768"

# biogrid orcs api
# accesskey = "b324f18e0acf2122bc1829e75ab9c768"

###################################
# import requests
# from Bio.PDB import *
# import urllib.request

# # Get PDB file
# pdb_id = "1CRN"
# pdb_file = pdb_id + ".pdb"
# url = "https://files.rcsb.org/download/" + pdb_file
# urllib.request.urlretrieve(url, pdb_file)

# # Get ORCS data
# biogrid_url = "https://webservice.thebiogrid.org/interactions?searchNames=true&includeInteractors=true&taxId=9606&geneList=CDK2&accesskey=<your_access_key>"
# response = requests.get(biogrid_url)
# interactions = response.json()

# # Print interactions
# for interaction in interactions:
#     print(interaction)


import requests
import sys
import os
import Herramientas.DescargarAbstracts as da
import json
import xml.etree.ElementTree as ET
import pandas as pd
from tqdm import tqdm
import numpy as np
import joblib
import spacy
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl
import itertools
from Bio.PDB import PDBList



# carpeta_abs = os.listdir("Documentos/Articulos/PMC")

# listado_textos = []

# for i in tqdm(range(len(carpeta_abs))):
#     carpeta = carpeta_abs[i]
#     ruta_carpeta_xml = f"Documentos/Articulos/PMC/{carpeta}"
#     archivos = os.listdir(ruta_carpeta_xml)
#     for ar in archivos:
#         if os.path.exists(ruta_carpeta_xml + f"/{carpeta}.json") == True:
#             with open(ruta_carpeta_xml + f"/{carpeta}.json", "r") as archivo:
#                 parrafos = json.load(archivo)
                
#             for index_p, p in enumerate(parrafos):
#                 listado_textos.append(p)


# print("Total parrafos 10000 articulos", len(listado_textos))

# Total parrafos 10000 articulos 836578

##############################

# pdblist = PDBList()
# all_pdb_ids = pdblist.get_all_entries()

# carpeta_abs = os.listdir("Documentos/Articulos/PMC")

# listado_articulos_proteinas = []
# listado_textos_proteinas = []

# for i in tqdm(range(len(carpeta_abs))):
#     carpeta = carpeta_abs[i]
#     ruta_carpeta_xml = f"Documentos/Articulos/PMC/{carpeta}"
#     archivos = os.listdir(ruta_carpeta_xml)
#     for ar in archivos:
#         if os.path.exists(ruta_carpeta_xml + f"/{carpeta}.json") == True:
#             with open(ruta_carpeta_xml + f"/{carpeta}.json", "r") as archivo:
#                 parrafos = json.load(archivo)
#             listado_total_proteinas = []
#             paper_valido = False
#             for index_p, p in enumerate(parrafos):
#                 matches = bp.IdentificarPDBId(texto=p)
#                 proteinas = []
#                 for proteina in matches:
#                     proteina = proteina.upper()
#                     try:
#                         numero = float(proteina)
#                     except ValueError:
#                         if proteina in all_pdb_ids:  # check if the protein ID is in the list
#                             proteinas.append(proteina)

#                 if len(proteinas) != 0:
#                     paper_valido = True
#                     datos = {
#                         "PMCId": carpeta,
#                         "Parrafo": p,
#                         "NumeroParrafo": index_p,
#                         "Proteinas": (', '.join(proteinas))
#                     }

#                     listado_total_proteinas = listado_total_proteinas + proteinas

#                     listado_textos_proteinas.append(datos)
#             if paper_valido == True:
#                 frecuencias = {}

#                 # Cuenta la cantidad de veces que se repite cada elemento en la lista
#                 for elemento in set(listado_total_proteinas):
#                     frecuencias[elemento] = listado_total_proteinas.count(
#                         elemento)

#                 datos_articulo = {
#                     "PMCId": carpeta,
#                     "Proteinas": frecuencias
#                 }

#                 if datos_articulo not in listado_articulos_proteinas:
#                     listado_articulos_proteinas.append(datos_articulo)

# print("cantidad textos :", len(listado_articulos_proteinas))

# 2630

########################
from gensim.models import KeyedVectors 


# representacionVectorial = KeyedVectors.load_word2vec_format(
#         'Transformer/Embeddings/PubMed-w2v.bin', binary=True)


# print(len(representacionVectorial))


#####################

