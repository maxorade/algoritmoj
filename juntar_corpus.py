import pandas as pd 
import os 
import numpy as np 
import joblib
import json

# carpeta_abs = os.listdir("Corpus")

# df = pd.DataFrame()

# for i in carpeta_abs:
#     if ".zip" not in i and i!= 'CorpusCompleto.csv' and i != "Abstracts" and  i != 'Biocreative3.csv' :
#         corpus = pd.read_csv(f"Corpus/{i}")
#         df = pd.concat([df, corpus])


# # corpus = pd.read_csv(ruta_corpus_filtrado)
# length = len(df)

# # Get the length of the dataframe using shape
# num_rows = df.shape[0]

# # Print the lengths
## print(length)
## print(num_rows)

# df.to_csv(f"Corpus/CorpusCompleto2.csv", index=False)

###############
# AImed_NB_LEXICON_Model
# AImed_KNN_MULTIPLE_TFIDF_Model
# nombre_modelo =   "AImed_KNN_MULTIPLE_TFIDF_Model"   

# info_modelo = nombre_modelo.split("_")
# ruta_resultados = f"Resultados/{info_modelo[1]}/Modelo"
# ruta_modelo = f"{ruta_resultados}/{nombre_modelo}.joblib"


# ruta_corpus_filtrado = f"Corpus/CorpusCompleto2.csv"

# corpus = pd.read_csv(ruta_corpus_filtrado)

# X = np.asarray(corpus["Abstract"][0:10000])
# X = X.ravel()
# y = np.asarray(corpus["Clasificacion"][0:10000])
# y = y.ravel()


# y_1 = [i for i in y if i == 1]
## print(len(y_1))

# ## print(len(y))
# ## print(ruta_modelo)

# modelo = joblib.load(ruta_modelo)

# y_pred = modelo.predict(X)
# listado_pred = [{"Index": index, "Abstract": X[index]}
#                         for index, val in enumerate(y) if val == 1 and y_pred[index] == 1]

# df = pd.DataFrame(data=listado_pred)
# df.to_csv(
#     f"CorpusCompleto2_SinFiltro_{nombre_modelo}.csv", index=False)

####################
# import nltk
# import re
# import requests

# # Descargar la base de datos UniProt
# url = 'https://www.uniprot.org/uniprot/?query=&columns=id&format=tab'
# response = requests.get(url)
# uniprot = response.text.split('\n')[1:-1]

# # Texto a analizar
# text = 'The herpes simplex virus type 1 genes UL5, UL8 and UL52 encode an essential heterotrimeric DNA helicase-primase that is responsible for concomitant DNA unwinding and primer synthesis at the viral DNA replication fork.'

# # Tokenizar el texto
# tokens = nltk.word_tokenize(text)

# # Buscar nombres de proteínas
# proteins = []
# for token in tokens:
#     if re.match('^[A-Z][A-Z0-9]{3}[0-9]?$', token):
#         if token in uniprot:
#             proteins.append(token)

## print(proteins)


##################

# text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut consectetur nulla. Vivamus bibendum eget tellus eu congue. Sed sed ipsum id orci ullamcorper molestie. Sed eu massa sit amet tortor venenatis sollicitudin in ac erat. Aenean gravida convallis elit, vel suscipit sapien tempor eu. Nulla sed facilisis metus, in dapibus risus. Duis auctor ipsum non odio pharetra, quis venenatis velit rutrum. Sed ut fringilla mi, in facilisis lorem. Etiam at sapien ut odio accumsan vehicula ut eu ex. Nam vitae blandit magna, euismod consectetur lorem. Proin luctus ligula in mi volutpat, vitae luctus nisi congue."

# words = text.lower().split()

## print(words)

# word_count = {}
# for i, word in enumerate(words):
#     if word in word_count:
#         word_count[word].append(i)
#     else:
#         word_count[word] = [i]

## print(word_count)
# for word, indices in word_count.items():
#     if len(indices) > 1:
#        # print(f"The word '{word}' appears {len(indices)} times at indices {indices}")
#        # print(f"The first occurrence of '{word}' is at index {indices[0]}")


####################

# import requests

# # Set the URL for the UniProt API
# url = 'https://www.ebi.ac.uk/proteins/api/proteins/'

# # Set the query parameters for the API request
# params = {
#     'offset': 0,
#     'size': 100,
#     'reviewed': 'yes',
#     'include': 'protein.names'
# }

# # Set the headers for the API request
# headers = {
#     'Accept': 'application/json'
# }

# # Send the API request and retrieve the response
# response = requests.get(url, headers=headers, params=params)

# # Extract the complex names from the response JSON
# complex_names = []
# # for result in response.json():
# #     for name in result['protein']['names']:
# #         if name['type'] == 'complex':
# #             complex_names.append(name['value'])

# # Print the list of complex names
## print(response.json())

################

# import requests, sys

# requestURL = "https://www.ebi.ac.uk/proteins/api/coordinates?offset=0&size=100&gene=FGFR2"

# r = requests.get(requestURL, headers={ "Accept" : "application/json"})

# if not r.ok:
#     r.raise_for_status()
#     sys.exit()

# responseBody = r.text
## print(responseBody)


#################
import sys
import requests
import re
import Herramientas.BusquedaProteinas as bp
import Herramientas.FuncionesLexicon as fl

from tqdm import tqdm
import spacy

ruta = "Documentos/DatosProfesora"
ruta_corpus = f"{ruta}/ParrafosArticulosComplejos.csv"

rango1 = 0
rango2 = 50000
ruta_resultados = f"{ruta}/ResultadoClasificacionTexto{rango1}_{rango2}.json"

# ruta_resultados = f"{ruta}/ResultadoClasificacionTexto.json"

ruta_uni_obl = f"{ruta}/Uniprot/ComplejosObligateUniprot.csv"
ruta_uni_tra = f"{ruta}/Uniprot/ComplejosTransientUniprot.csv"

ruta_pdb_obl = f"{ruta}/ComplejosObligate.csv"
ruta_pdb_tra = f"{ruta}/ComplejosTransient.csv"

# Complementos

jnlpba = spacy.load('en_ner_jnlpba_md')
with open('Documentos/PatronProteinasSwissProt.txt') as f:
    patron_swiss = f.readlines()

patron = patron_swiss[0]

#### Conjunto de parrafos

corpus = pd.read_csv(ruta_corpus)
corpus = corpus.values.tolist()

#### Datos Uniprot

info_uni_obl = pd.read_csv(ruta_uni_obl)
info_uni_tra = pd.read_csv(ruta_uni_tra)

info_pdb_obl = pd.read_csv(ruta_pdb_obl)
info_pdb_tra = pd.read_csv(ruta_pdb_tra)

### Resultados

with open(ruta_resultados, 'r') as f:
    # Load the JSON data from the file
    resultados = json.load(f)

resultados_index = [index for index,
                    res in enumerate(resultados) if res == 1]

extraccion_info = []

## print(resultados_index)

### Analisis de informacion

for i in tqdm(range(len(resultados_index))):
    index = resultados_index[i] + rango1
    # index = resultados_index[i]
    info_corpus = corpus[index]
    abstract = info_corpus[3]

    complejo = info_corpus[0]
    pdb_id = str(complejo.split("_")[0])
    id_pmc = info_corpus[1]

    validar_obl = False
    index_obl = -1

    validar_tra = False
    index_tra = -1

    uniprot_id = ""
    uniprot_nombres = ""
    
    pdb_nombres = ""
    
    for index, proteina in  enumerate(info_uni_obl["Proteina"]):
        if pdb_id.strip() == proteina.strip():
            validar_obl = True
            index_obl = index

    for index, proteina in  enumerate(info_uni_tra["Proteina"]):
        if pdb_id.strip() == proteina.strip():
            validar_tra = True
            index_tra = index
            


    if validar_obl == False and validar_tra == False :
        continue
    
    elif validar_obl == True:
       # print(f"UniprotId :", info_uni_obl["UniprotId"][index_obl])
       # print(f"Uniprot nombres:", info_uni_obl["Nombres"][index_obl])

        uniprot_id =  info_uni_obl["UniprotId"][index_obl]
        uniprot_nombres =  info_uni_obl["Nombres"][index_obl]

        

    elif validar_tra == True:
       # print(f"UniprotId :", info_uni_tra["UniprotId"][index_tra])   
       # print(f"Uniprot nombres :", info_uni_tra["Nombres"][index_tra])   

        uniprot_id =  info_uni_tra["UniprotId"][index_tra]
        uniprot_nombres =  info_uni_tra["Nombres"][index_tra]

    validar_obl = False
    index_obl = -1

    validar_tra = False
    index_tra = -1 

    for index, proteina in  enumerate(info_pdb_obl["Proteina"]):
        if pdb_id.strip() == proteina.strip():
            validar_obl = True
            index_obl = index

    for index, proteina in  enumerate(info_pdb_tra["Proteina"]):
        if pdb_id.strip() == proteina.strip():
            validar_tra = True
            index_tra = index

    if validar_obl == False and validar_tra == False :
        continue
    
    elif validar_obl == True:
       # print(f"PDB nombre :", info_pdb_obl["NombreProteina"][index_obl])
        pdb_nombres = info_pdb_obl["NombreProteina"][index_obl]

    elif validar_tra == True:
       # print(f"PDB nombre :", info_pdb_tra["NombreProteina"][index_tra])   
        pdb_nombres = info_pdb_tra["NombreProteina"][index_tra]
   

   # print(f"complejo :{complejo}\n")
   # print(f"complejo id :{pdb_id}\n")
   # print(f"id_pmc :{id_pmc}\n")
   # print(f"abstract : PMC{abstract}\n")
   # print("\n")

    doc = jnlpba(abstract)

    proteinas_docs = []

    proteinas_spacy = bp.BusquedaProteinasSpacy(abstract, doc)
    proteinas_patron = bp.BusquedaProteinasPatrones(
        abstract, patron)
    proteinas_corpus = bp.BuscarProteinasCorpus(abstract)

    proteinas_complex =  bp.BuscarComplejosCorpus(abstract)

    for j in proteinas_spacy:
        if j not in proteinas_docs:
            if j in proteinas_patron or j in proteinas_corpus:
                proteinas_docs.append(j)
    for j in proteinas_patron:
        if j not in proteinas_docs:
            if j in proteinas_spacy or j in proteinas_corpus:
                proteinas_docs.append(j)
    for j in proteinas_corpus:
        if j not in proteinas_docs:
            if j in proteinas_spacy or j in proteinas_patron:
                proteinas_docs.append(j)

   # print(f"proteinas_spacy :{proteinas_spacy}\n",)
   # print(f"proteinas_swissprot :{proteinas_patron}\n")
   # print(f"proteinas_corpus :{proteinas_corpus}\n")
   # print(f"proteinas_complejos :{proteinas_complex}\n")

   # print(f"proteinas validadas :{proteinas_docs}\n")

    proteinas = []
    for  j in proteinas_docs:
        indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
        proteinas_aparicion = [{"Proteina": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
        info_proteina = {
            "Proteina": j,
            "ListadoIndex": proteinas_aparicion
        }
        proteinas.append(info_proteina)
    ## print("proteina :", proteinas)

    validar = False
    complejo_id = pdb_id 
    for j in proteinas_docs:
        pdb_id = bp.GetPDBId(nombre=j)
        if pdb_id != None:
           # print(f"PDB :{pdb_id}\n")
            if complejo_id == pdb_id:
                validar == True

   # print(f"complejos encontrados :{validar}\n")

   # print("____\n")
    palabras_docs = fl.ValidarPalabrasClaves(doc)[1]
   # print(f"palabras positivas :{palabras_docs}\n")

    palabras_claves = []
    for  j in palabras_docs:
        indices = [match.start() for match in re.finditer(f"{j}", abstract, re.IGNORECASE)]
        palabra = [{"Palabra": abstract[index: index + len(j)], "Inicio": index, "Termino": index + len(j)  } for index  in indices]
        info_palabra = {
            "Palabra": j,
            "ListadoIndex": palabra
        }
        palabras_claves.append(info_palabra)

   # print("_________________\n")

    if len(proteinas) >=2:
        data = {
            "Complejo": complejo,
            "PDBId":  complejo_id,
            "PDBNombre": pdb_nombres,
            "UniprotId": uniprot_id,
            "UniprotNombres": uniprot_nombres,
            "PMCId": id_pmc,
            "Abstract": abstract,
            "ProteinasSpacy":proteinas_spacy,
            "ProteinasSwissProt":proteinas_patron,
            "ProteinasCorpus":proteinas_corpus,
            "ProteinasComplejos":proteinas_complex,
            "ProteinasValidadas":proteinas_docs,
            "ProteinasUbicacion": proteinas,
            "ComplejosEncontrados":validar,
            "PalabrasPositivas":palabras_docs,
            "PalabrasUbicacion":palabras_claves
        }
        
        extraccion_info.append(data)
#     doc = jnlpba(abstract)

#     ents = list(doc.ents)
#     for ent in ents:
#        # print(ent.label_,ent.text)

    # proteinas_complex =  bp.BuscarComplejosCorpus2(abstract)

    ## print(f"proteinas_complex :{proteinas_complex}\n")


#     complejo = info_corpus[0]
#     complejo_id = str(complejo.split("_")[0]).upper()
#     id_pmc = info_corpus[1]
   # print("_________________\n")

with open(f"ResultadosProfesora.json", 'w') as f:
            json.dump(extraccion_info, f)

#    # print(f"complejo :{complejo}\n")
#    # print(f"complejo id :{complejo_id}\n")
#    # print(f"id_pmc :{id_pmc}\n")
#    # print(f"abstract :{abstract}\n")

#     doc = jnlpba(abstract)

#     proteinas_docs = []
    ## print("_________________\n")






