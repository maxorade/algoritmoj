corpus = ["AImed","Biocreative3","CorpusCompleto","LLL","IEPA","HPRD50" ]
algoritmo_clasificacion_ml = ["NB","SVM","RF","KNN"]
algoritmo_clas_ml_combinado = ["SVM_NB_RF","SVM_KNN_RF","KNN_NB_RF","SVM_NB_KNN" ]
algoritmo_clasificacion_dl = ["CNN","LSTM","GRU","BERT" ]
representacion_documento = ["LEXICON","TFIDF","TFIDF_LEXICON","WEMB","WEMB_LEXICON","WEMB_LEXICON_TFIDF","MULTIPLE_TFIDF","MULTIPLE_WEMB" ]
representacion_documento_combi = ["LEXICON","TFIDF","WEMB","TFIDF_LEXICON","WEMB_LEXICON" ]
informacion_corpus = ["Filtrado","NoFiltrado" ]
tipo_filtro = ["ninguno","spacy","patrones"  ]
guardar_modelo = ["Guardar","NoGuardar"  ]


with open(f"ScriptsClasificadoresCombiSinFiltrar.txt", 'w') as f:

    for representacion in representacion_documento_combi:
        for filtro in informacion_corpus:
            for tipo in tipo_filtro:
                for corp in corpus:
                    for algoritmo in algoritmo_clas_ml_combinado:
                        comand = f"python ExperimentosML.py {corp} {algoritmo} {representacion} {filtro} {tipo} NoGuardar & " + "\n"
                        if filtro == "NoFiltrado" and tipo == "ninguno":
                            f.writelines(comand)
                        # if  filtro == "Filtrado" and tipo == "spacy" or tipo == "patrones":
                        #     f.writelines(comand)